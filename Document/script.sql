USE [master]
GO
/****** Object:  Database [QLTV]    Script Date: 1/14/2017 12:07:26 AM ******/
CREATE DATABASE [QLTV]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'QLTV', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\QLTV.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'QLTV_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\QLTV_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [QLTV] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QLTV].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QLTV] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QLTV] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QLTV] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QLTV] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QLTV] SET ARITHABORT OFF 
GO
ALTER DATABASE [QLTV] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [QLTV] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QLTV] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QLTV] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QLTV] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QLTV] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QLTV] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QLTV] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QLTV] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QLTV] SET  DISABLE_BROKER 
GO
ALTER DATABASE [QLTV] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QLTV] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QLTV] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QLTV] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QLTV] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QLTV] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QLTV] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QLTV] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [QLTV] SET  MULTI_USER 
GO
ALTER DATABASE [QLTV] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QLTV] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QLTV] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QLTV] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [QLTV] SET DELAYED_DURABILITY = DISABLED 
GO
USE [QLTV]
GO
/****** Object:  Table [dbo].[Account]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Account](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[FullName] [nvarchar](50) NULL,
	[Xoa] [int] NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChiTietMuon]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietMuon](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Sach] [int] NOT NULL,
	[Tra] [int] NULL,
	[ID_Muon] [int] NOT NULL,
 CONSTRAINT [PK_ChiTietMuon] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[ID_Sach] ASC,
	[ID_Muon] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChiTietTra]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChiTietTra](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Sach] [int] NOT NULL,
	[ID_Tra] [int] NOT NULL,
 CONSTRAINT [PK_ChiTietTra] PRIMARY KEY CLUSTERED 
(
	[ID] ASC,
	[ID_Sach] ASC,
	[ID_Tra] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[DocGia]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DocGia](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TenDG] [nvarchar](50) NOT NULL,
	[GTinh] [nvarchar](3) NOT NULL,
	[CMND] [nchar](12) NOT NULL,
	[NgaySinh] [date] NOT NULL,
	[ID_Loai] [int] NOT NULL,
	[DiaChi] [nvarchar](500) NULL,
	[Tel] [nchar](11) NULL,
	[Email] [nvarchar](50) NULL,
	[Xoa] [int] NULL,
	[SoDangMuon] [int] NULL,
	[NgayHetHan] [date] NOT NULL,
 CONSTRAINT [PK_DocGia_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiDocGia]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiDocGia](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TenLoai] [nvarchar](50) NOT NULL,
	[PhiThuongNien] [int] NULL,
	[SoNgayMuonToiDa] [int] NULL,
	[MuonSachDB] [int] NULL,
	[PhiQuaHan] [int] NULL,
	[SoSachMuonToiDa] [int] NULL,
 CONSTRAINT [PK_LoaiDocGia_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiSach]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiSach](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiSach] [nvarchar](50) NOT NULL,
	[MoTa] [nvarchar](500) NULL,
	[Xoa] [int] NULL,
 CONSTRAINT [PK_LoaiSach_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MuonSach]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MuonSach](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_DocGia] [int] NULL,
	[Ngay] [date] NULL,
	[Xoa] [int] NULL,
 CONSTRAINT [PK_MuonSach_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sach]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sach](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TenSach] [nvarchar](50) NOT NULL,
	[ID_LoaiSach] [int] NULL,
	[NhaSX] [nvarchar](50) NULL,
	[NamSX] [int] NULL,
	[TomTat] [nvarchar](500) NULL,
	[TongSach] [int] NULL,
	[SoSachCon] [nchar](10) NULL,
	[NgayNhap] [date] NULL,
	[Xoa] [int] NULL,
	[GiaSach] [int] NULL,
 CONSTRAINT [PK_Sach] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TraSach]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TraSach](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[ID_Muon] [int] NULL,
	[Ngay] [date] NULL,
	[TienPhat] [int] NULL,
	[Xoa] [int] NULL,
 CONSTRAINT [PK_TraSach_1] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  View [dbo].[vv_DG_HetHan]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vv_DG_HetHan] as
(SELECT        DocGia.ID, DocGia.TenDG, DocGia.GTinh, DocGia.CMND, DocGia.NgaySinh, DocGia.DiaChi, DocGia.Tel, DocGia.Email, DocGia.SoDangMuon, DocGia.NgayHetHan, LoaiDocGia.TenLoai
FROM            DocGia INNER JOIN
                         LoaiDocGia ON DocGia.ID_Loai = LoaiDocGia.ID
WHERE        (DocGia.Xoa = 0) AND (DocGia.NgayHetHan < { fn CURDATE() }))
GO
/****** Object:  View [dbo].[vv_Muon]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vv_Muon] as
(SELECT DISTINCT DocGia.ID, DocGia.TenDG, MuonSach.Ngay, Sach.ID AS IDSach, Sach.TenSach, DATEADD(day, LoaiDocGia.SoNgayMuonToiDa, MuonSach.Ngay) AS NgayHetHan
FROM            ChiTietMuon INNER JOIN
                         MuonSach ON ChiTietMuon.ID_Muon = MuonSach.ID INNER JOIN
                         DocGia ON MuonSach.ID_DocGia = DocGia.ID INNER JOIN
                         Sach ON ChiTietMuon.ID_Sach = Sach.ID INNER JOIN
                         LoaiDocGia ON DocGia.ID_Loai = LoaiDocGia.ID AND DATEDIFF(day, MuonSach.Ngay, { fn CURDATE() }) > 0
WHERE        (DocGia.Xoa = 0) AND (ChiTietMuon.Tra = 0))
GO
/****** Object:  View [dbo].[vv_SachIt]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vv_SachIt] as
SELECT        Sach.ID, Sach.TenSach, Sach.NhaSX, Sach.NamSX, Sach.TomTat, Sach.TongSach, Sach.SoSachCon, Sach.NgayNhap, Sach.GiaSach, LoaiSach.TenLoaiSach, LoaiSach.MoTa
FROM            LoaiSach INNER JOIN
                         Sach ON LoaiSach.ID = Sach.ID_LoaiSach
WHERE        (LoaiSach.Xoa = 0) AND (Sach.SoSachCon < 50)
GO
/****** Object:  View [dbo].[vv_XoaDG]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vv_XoaDG] as
(SELECT *
FROM   DocGia
WHERE        Xoa = 1)
GO
/****** Object:  View [dbo].[vv_XoaLSach]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[vv_XoaLSach] as
(SELECT *
FROM   LoaiSach 
WHERE        Xoa = 1)
GO
/****** Object:  View [dbo].[vv_XoaMuon]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vv_XoaMuon] as
(SELECT *
FROM   MuonSach
WHERE        Xoa = 1)
GO
/****** Object:  View [dbo].[vv_XoaSach]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[vv_XoaSach] as
(SELECT *
FROM   Sach 
WHERE        Xoa = 1)
GO
/****** Object:  View [dbo].[vv_XoaTra]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[vv_XoaTra] as
(SELECT *
FROM   TraSach 
WHERE        Xoa = 1)
GO
ALTER TABLE [dbo].[ChiTietMuon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietMuon_MuonSach1] FOREIGN KEY([ID_Muon])
REFERENCES [dbo].[MuonSach] ([ID])
GO
ALTER TABLE [dbo].[ChiTietMuon] CHECK CONSTRAINT [FK_ChiTietMuon_MuonSach1]
GO
ALTER TABLE [dbo].[ChiTietMuon]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietMuon_Sach] FOREIGN KEY([ID_Sach])
REFERENCES [dbo].[Sach] ([ID])
GO
ALTER TABLE [dbo].[ChiTietMuon] CHECK CONSTRAINT [FK_ChiTietMuon_Sach]
GO
ALTER TABLE [dbo].[ChiTietTra]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietTra_Sach] FOREIGN KEY([ID_Sach])
REFERENCES [dbo].[Sach] ([ID])
GO
ALTER TABLE [dbo].[ChiTietTra] CHECK CONSTRAINT [FK_ChiTietTra_Sach]
GO
ALTER TABLE [dbo].[ChiTietTra]  WITH CHECK ADD  CONSTRAINT [FK_ChiTietTra_TraSach] FOREIGN KEY([ID_Tra])
REFERENCES [dbo].[TraSach] ([ID])
GO
ALTER TABLE [dbo].[ChiTietTra] CHECK CONSTRAINT [FK_ChiTietTra_TraSach]
GO
ALTER TABLE [dbo].[DocGia]  WITH CHECK ADD  CONSTRAINT [FK_DocGia_LoaiDocGia] FOREIGN KEY([ID_Loai])
REFERENCES [dbo].[LoaiDocGia] ([ID])
GO
ALTER TABLE [dbo].[DocGia] CHECK CONSTRAINT [FK_DocGia_LoaiDocGia]
GO
ALTER TABLE [dbo].[MuonSach]  WITH CHECK ADD  CONSTRAINT [FK_MuonSach_DocGia] FOREIGN KEY([ID_DocGia])
REFERENCES [dbo].[DocGia] ([ID])
GO
ALTER TABLE [dbo].[MuonSach] CHECK CONSTRAINT [FK_MuonSach_DocGia]
GO
ALTER TABLE [dbo].[Sach]  WITH CHECK ADD  CONSTRAINT [FK_Sach_LoaiSach] FOREIGN KEY([ID_LoaiSach])
REFERENCES [dbo].[LoaiSach] ([ID])
GO
ALTER TABLE [dbo].[Sach] CHECK CONSTRAINT [FK_Sach_LoaiSach]
GO
ALTER TABLE [dbo].[TraSach]  WITH CHECK ADD  CONSTRAINT [FK_TraSach_MuonSach] FOREIGN KEY([ID_Muon])
REFERENCES [dbo].[MuonSach] ([ID])
GO
ALTER TABLE [dbo].[TraSach] CHECK CONSTRAINT [FK_TraSach_MuonSach]
GO
/****** Object:  StoredProcedure [dbo].[IS_DCMuon]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[IS_DCMuon]
@id int,
@soSach int,
@result int output
as
begin
declare @muon int 
set @muon =(select COUNT(CTM.ID) from ChiTietMuon CTM join MuonSach MS on CTM.ID_Muon=MS.ID join DocGia DG on MS.ID_DocGia=DG.ID where Tra=0 and DG.ID=@id)+@soSach
declare @dcmuon int 
set @dcmuon =(select LDG.SoSachMuonToiDa from LoaiDocGia LDG join DocGia DG on LDG.ID=DG.ID_Loai where DG.ID=@id)-@muon
if @dcmuon<0 
set @result=0
else
set @result=1
end

GO
/****** Object:  StoredProcedure [dbo].[IS_SDCMuon]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[IS_SDCMuon]
@id int,
@result int output
as
begin
declare @con int 
set @con =(select SoSachCon from Sach where ID=@id and Xoa=0)
if @con>0 
set @result=1
else
set @result=0
end
GO
/****** Object:  StoredProcedure [dbo].[st_idn_ms]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[st_idn_ms]
	@result Integer output
as
begin
set @result=(select max(ID)
from MuonSach)
end
GO
/****** Object:  StoredProcedure [dbo].[st_idn_ts]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[st_idn_ts]
	@result Integer output
as
begin
set @result=(select max(ID)
from TraSach)
end
GO
/****** Object:  StoredProcedure [dbo].[stTen_DG]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create proc [dbo].[stTen_DG]
@id int,
@result nvarchar(50) output
as
begin
set @result =(select TenDG from DocGia where ID=@id)
end
GO
/****** Object:  StoredProcedure [dbo].[stTen_Sach]    Script Date: 1/14/2017 12:07:27 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE proc [dbo].[stTen_Sach]
@id int,
@result nvarchar(50) output
as
begin
set @result =(select TenSach from Sach where ID=@id)
end
GO
USE [master]
GO
ALTER DATABASE [QLTV] SET  READ_WRITE 
GO
