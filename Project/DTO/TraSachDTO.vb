﻿Public Class TraSachDTO
    Private _iD As Integer
    Public Property ID() As Integer
        Get
            Return _iD
        End Get
        Set(ByVal value As Integer)
            _iD = value
        End Set
    End Property
    Private _iD_Muon As Integer
    Public Property ID_Muon() As Integer
        Get
            Return _iD_Muon
        End Get
        Set(ByVal value As Integer)
            _iD_Muon = value
        End Set
    End Property

    Private _ngay As DateTime
    Public Property Ngay() As DateTime
        Get
            Return _ngay
        End Get
        Set(ByVal value As DateTime)
            _ngay = value
        End Set
    End Property

    Private _tienPhat As Integer
    Public Property TienPhat() As Integer
        Get
            Return _tienPhat
        End Get
        Set(ByVal value As Integer)
            _tienPhat = value
        End Set
    End Property
    Private _xoa As Integer
    Public Property Xoa() As Integer
        Get
            Return _xoa
        End Get
        Set(ByVal value As Integer)
            _xoa = value
        End Set
    End Property
End Class
