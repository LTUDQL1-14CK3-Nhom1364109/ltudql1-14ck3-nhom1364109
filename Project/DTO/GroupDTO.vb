﻿Public Class GroupDTO
    Private _iD As Integer
    Public Property ID() As Integer
        Get
            Return _iD
        End Get
        Set(ByVal value As Integer)
            _iD = value
        End Set
    End Property
    Private _tenLoai As String
    Public Property TenLoai() As String
        Get
            Return _tenLoai
        End Get
        Set(ByVal value As String)
            _tenLoai = value
        End Set
    End Property
    Private _moTa As String
    Public Property MoTa() As String
        Get
            Return _moTa
        End Get
        Set(ByVal value As String)
            _moTa = value
        End Set
    End Property
End Class
