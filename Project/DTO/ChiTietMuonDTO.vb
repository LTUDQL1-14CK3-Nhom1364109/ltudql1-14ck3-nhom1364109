﻿Public Class ChiTietMuonDTO
    Private _iD As Integer
    Public Property ID() As Integer
        Get
            Return _iD
        End Get
        Set(ByVal value As Integer)
            _iD = value
        End Set
    End Property
    Private _iD_Sach As Integer
    Public Property ID_Sach() As Integer
        Get
            Return _iD_Sach
        End Get
        Set(ByVal value As Integer)
            _iD_Sach = value
        End Set
    End Property
    Private _tra As Integer
    Public Property Tra() As Integer
        Get
            Return _tra
        End Get
        Set(ByVal value As Integer)
            _tra = value
        End Set
    End Property
    Private _iD_Muon As Integer
    Public Property ID_Muon() As Integer
        Get
            Return _iD_Muon
        End Get
        Set(ByVal value As Integer)
            _iD_Muon = value
        End Set
    End Property
End Class
