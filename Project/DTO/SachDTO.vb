﻿Public Class SachDTO
    Private _iD As Integer
    Public Property ID() As Integer
        Get
            Return _iD
        End Get
        Set(ByVal value As Integer)
            _iD = value
        End Set
    End Property
    Private _tenSach As String
    Public Property TenSach() As String
        Get
            Return _tenSach
        End Get
        Set(ByVal value As String)
            _tenSach = value
        End Set
    End Property
    Private _iD_LoaiSach As Integer
    Public Property ID_LoaiSach() As Integer
        Get
            Return _iD_LoaiSach
        End Get
        Set(ByVal value As Integer)
            _iD_LoaiSach = value
        End Set
    End Property
    Private _nhaSX As String
    Public Property NhaSX() As String
        Get
            Return _nhaSX
        End Get
        Set(ByVal value As String)
            _nhaSX = value
        End Set
    End Property
    Private _namSX As Integer
    Public Property NamSX() As Integer
        Get
            Return _namSX
        End Get
        Set(ByVal value As Integer)
            _namSX = value
        End Set
    End Property
    Private _tomTat As String
    Public Property TomTat() As String
        Get
            Return _tomTat
        End Get
        Set(ByVal value As String)
            _tomTat = value
        End Set
    End Property
    Private _tongSach As Integer
    Public Property TongSach() As Integer
        Get
            Return _tongSach
        End Get
        Set(ByVal value As Integer)
            _tongSach = value
        End Set
    End Property
    Private _soSachCon As Integer
    Public Property SoSachCon() As Integer
        Get
            Return _soSachCon
        End Get
        Set(ByVal value As Integer)
            _soSachCon = value
        End Set
    End Property
    Private _ngayNhap As DateTime
    Public Property NgayNhap() As DateTime
        Get
            Return _ngayNhap
        End Get
        Set(ByVal value As DateTime)
            _ngayNhap = value
        End Set
    End Property
    Private _giaSach As Integer
    Public Property GiaSach() As Integer
        Get
            Return _giaSach
        End Get
        Set(ByVal value As Integer)
            _giaSach = value
        End Set
    End Property
End Class
