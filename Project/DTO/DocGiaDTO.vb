﻿Public Class DocGiaDTO
    Private _iD As String
    Public Property ID() As Integer
        Get
            Return _iD
        End Get
        Set(ByVal value As Integer)
            _iD = value
        End Set
    End Property
    Private _tenDG As String
    Public Property TenDG() As String
        Get
            Return _tenDG
        End Get
        Set(ByVal value As String)
            _tenDG = value
        End Set
    End Property
    Private _cMND As String
    Public Property CMND() As String
        Get
            Return _cMND
        End Get
        Set(ByVal value As String)
            _cMND = value
        End Set
    End Property
    Private _ngaySinh As String
    Public Property NgaySinh() As String
        Get
            Return _ngaySinh
        End Get
        Set(ByVal value As String)
            _ngaySinh = value
        End Set
    End Property
    Private _iD_Loai As Integer
    Public Property ID_Loai() As Integer
        Get
            Return _iD_Loai
        End Get
        Set(ByVal value As Integer)
            _iD_Loai = value
        End Set
    End Property
    Private _diaChi As String
    Public Property DiaChi() As String
        Get
            Return _diaChi
        End Get
        Set(ByVal value As String)
            _diaChi = value
        End Set
    End Property
    Private _tel As String
    Public Property Tel() As String
        Get
            Return _tel
        End Get
        Set(ByVal value As String)
            _tel = value
        End Set
    End Property
    Private _email As String
    Public Property Email() As String
        Get
            Return _email
        End Get
        Set(ByVal value As String)
            _email = value
        End Set
    End Property
    Private _xoa As Integer
    Public Property Xoa() As Integer
        Get
            Return _xoa
        End Get
        Set(ByVal value As Integer)
            _xoa = value
        End Set
    End Property
    Private _soDangMuon As Integer
    Public Property SoDangMuon() As Integer
        Get
            Return _soDangMuon
        End Get
        Set(ByVal value As Integer)
            _soDangMuon = value
        End Set
    End Property
    Private _ngayHetHan As String
    Public Property NgayHetHan() As String
        Get
            Return _ngayHetHan
        End Get
        Set(ByVal value As String)
            _ngayHetHan = value
        End Set
    End Property
    Private _gTinh As String
    Public Property GTinh() As String
        Get
            Return _gTinh
        End Get
        Set(ByVal value As String)
            _gTinh = value
        End Set
    End Property
End Class
