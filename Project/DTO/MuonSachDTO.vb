﻿Public Class MuonSachDTO
    Private _iD As Integer
    Public Property ID() As Integer
        Get
            Return _iD
        End Get
        Set(ByVal value As Integer)
            _iD = value
        End Set
    End Property
    Private _iD_DocGia As Integer
    Public Property ID_DocGia() As Integer
        Get
            Return _iD_DocGia
        End Get
        Set(ByVal value As Integer)
            _iD_DocGia = value
        End Set
    End Property
    Private _ngay As DateTime
    Public Property Ngay() As DateTime
        Get
            Return _ngay
        End Get
        Set(ByVal value As DateTime)
            _ngay = value
        End Set
    End Property
    Private _xoa As Integer
    Public Property Xoa() As Integer
        Get
            Return _xoa
        End Get
        Set(ByVal value As Integer)
            _xoa = value
        End Set
    End Property
End Class
