﻿Public Class ChiTietTraDTO
    Private _iD As Integer
    Public Property ID() As Integer
        Get
            Return _iD
        End Get
        Set(ByVal value As Integer)
            _iD = value
        End Set
    End Property
    Private _iD_Sach As Integer
    Public Property ID_Sach() As Integer
        Get
            Return _iD_Sach
        End Get
        Set(ByVal value As Integer)
            _iD_Sach = value
        End Set
    End Property
    Private _iD_Tra As Integer
    Public Property ID_Tra() As Integer
        Get
            Return _iD_Tra
        End Get
        Set(ByVal value As Integer)
            _iD_Tra = value
        End Set
    End Property
End Class
