﻿Imports DTO
Imports DAO

Public Class AccountBUS
    Private acDao As New AccountDAO()
    Public Function IsLogin(ByVal ac As AccountDTO) As Boolean
        Return acDao.IsLogin(ac)
    End Function
    Public Function Table() As DataTable
        Return acDao.Table()
    End Function
    Public Function Them(ByVal key As AccountDTO) As Integer
        Dim dgDAO As AccountDAO
        dgDAO = New AccountDAO()
        Return dgDAO.Them(key)
    End Function
    Public Function Xoa(ByVal key As String) As Integer
        Dim dgDAO As AccountDAO
        dgDAO = New AccountDAO()
        Return dgDAO.Xoa(key)
    End Function
    Public Function Sua(ByVal key As AccountDTO) As Integer
        Dim dgDAO As AccountDAO
        dgDAO = New AccountDAO()
        Return dgDAO.Sua(key)
    End Function
    Public Function TableTim(ByVal thamso As Integer, ByVal key As String) As DataTable
        Dim dgDAO As AccountDAO
        dgDAO = New AccountDAO()
        Return dgDAO.TableTim(thamso, key)
    End Function
End Class


