﻿Imports DTO
Imports DAO
Public Class TraSachBUS
    Public Function ListLQ(ByVal key As Integer) As List(Of String)
        Dim CT As New ChiTietMuonDAO()
        Return CT.ListLQ(key)
    End Function
    Public Function Table(ByVal star As Integer, ByVal max As Integer) As DataTable
        Dim tsDAO As TraSachDAO
        tsDAO = New TraSachDAO()
        Return tsDAO.Table(star, max)
    End Function
    Public Function Them(ByVal key As TraSachDTO) As Integer
        Dim tsDAO As TraSachDAO
        tsDAO = New TraSachDAO()
        Return tsDAO.Them(key)
    End Function
    Public Function ThemCT(ByVal key As ChiTietTraDTO) As Integer
        Dim ctmDAO As ChiTietTraDAO
        ctmDAO = New ChiTietTraDAO()
        Return ctmDAO.Them(key)
    End Function
    Public Function Xoa(ByVal key As String) As Integer
        Dim tsDAO As TraSachDAO
        tsDAO = New TraSachDAO()
        Return tsDAO.Xoa(key)
    End Function
    Public Function Sua(ByVal key As TraSachDTO) As Integer
        Dim tsDAO As TraSachDAO
        tsDAO = New TraSachDAO()
        Return tsDAO.Sua(key)
    End Function
    Public Function TableTim(ByVal thamso As Integer, ByVal key As String) As DataTable
        Dim tsDAO As TraSachDAO
        tsDAO = New TraSachDAO()
        Return tsDAO.TableTim(thamso, key)
    End Function
End Class
