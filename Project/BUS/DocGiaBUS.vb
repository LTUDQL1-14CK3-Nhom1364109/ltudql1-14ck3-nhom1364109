﻿Imports DTO
Imports DAO
Public Class DocGiaBUS
    Public Function ShowTen(ByVal key As String) As String
        Dim dgDAO As DocGiaDAO
        dgDAO = New DocGiaDAO()
        Return dgDAO.ShowTen(key)
    End Function
    Public Function ListLQ() As List(Of String)
        Dim dgDAO As DocGiaDAO
        dgDAO = New DocGiaDAO()
        Return dgDAO.ListLQ()
    End Function
    Public Function Table(ByVal star As Integer, ByVal max As Integer) As DataTable
        Dim dgDAO As DocGiaDAO
        dgDAO = New DocGiaDAO()
        Return dgDAO.Table(star, max)
    End Function
    Public Function Them(ByVal key As DocGiaDTO) As Integer
        Dim dgDAO As DocGiaDAO
        dgDAO = New DocGiaDAO()
        Return dgDAO.Them(key)
    End Function
    Public Function Xoa(ByVal key As String) As Integer
        Dim dgDAO As DocGiaDAO
        dgDAO = New DocGiaDAO()
        Return dgDAO.Xoa(key)
    End Function
    Public Function Sua(ByVal key As DocGiaDTO) As Integer
        Dim dgDAO As DocGiaDAO
        dgDAO = New DocGiaDAO()
        Return dgDAO.Sua(key)
    End Function
    Public Function TableTim(ByVal thamso As Integer, ByVal key As String) As DataTable
        Dim dgDAO As DocGiaDAO
        dgDAO = New DocGiaDAO()
        Return dgDAO.TableTim(thamso, key)
    End Function
    Public Function Ghan(ByVal key As String) As Integer
        Dim dgDAO As DocGiaDAO
        dgDAO = New DocGiaDAO()
        Return dgDAO.Ghan(key)
    End Function
    Public Function Phi(ByVal key As String) As Integer
        Dim dgDAO As DocGiaDAO
        dgDAO = New DocGiaDAO()
        Return dgDAO.Phi(key)
    End Function
End Class
