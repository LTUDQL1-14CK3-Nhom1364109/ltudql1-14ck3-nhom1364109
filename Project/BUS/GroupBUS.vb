﻿Imports DAO
Imports DTO
Public Class GroupBUS
    Public Function LstLoai() As List(Of String)
        Dim lsDAO As GroupDAO
        lsDAO = New GroupDAO()
        Return lsDAO.LstLoai()
    End Function
    Public Function Table() As DataTable
        Dim lsDAO As GroupDAO
        lsDAO = New GroupDAO()
        Return lsDAO.Table()
    End Function
    Public Function Them(ByVal key As GroupDTO) As Integer
        Dim lsDAO As GroupDAO
        lsDAO = New GroupDAO()
        Return lsDAO.Them(key)
    End Function
    Public Function Xoa(ByVal key As String) As Integer
        Dim lsDAO As GroupDAO
        lsDAO = New GroupDAO()
        Return lsDAO.Xoa(key)
    End Function
    Public Function Sua(ByVal key As GroupDTO) As Integer
        Dim lsDAO As GroupDAO
        lsDAO = New GroupDAO()
        Return lsDAO.Sua(key)
    End Function
    Public Function TableTim(ByVal thamso As Integer, ByVal key As String) As DataTable
        Dim lsDAO As GroupDAO
        lsDAO = New GroupDAO()
        Return lsDAO.TableTim(thamso, key)
    End Function
End Class
