﻿Imports DAO
Imports DTO
Public Class SachBUS
    Public Function ShowTen(ByVal key As String) As String
        Dim sDAO As SachDAO
        sDAO = New SachDAO()
        Return sDAO.ShowTen(key)
    End Function
    Public Function Table(ByVal star As Integer, ByVal max As Integer) As DataTable
        Dim sDAO As SachDAO
        sDAO = New SachDAO()
        Return sDAO.Table(star, max)
    End Function
    Public Function Them(ByVal key As SachDTO) As Integer
        Dim sDAO As SachDAO
        sDAO = New SachDAO()
        Return sDAO.Them(key)
    End Function
    Public Function Xoa(ByVal key As String) As Integer
        Dim sDAO As SachDAO
        sDAO = New SachDAO()
        Return sDAO.Xoa(key)
    End Function
    Public Function Sua(ByVal key As SachDTO) As Integer
        Dim sDAO As SachDAO
        sDAO = New SachDAO()
        Return sDAO.Sua(key)
    End Function
    Public Function TableTim(ByVal thamso As Integer, ByVal key As String) As DataTable
        Dim sDAO As SachDAO
        sDAO = New SachDAO()
        Return sDAO.TableTim(thamso, key)
    End Function
End Class
