﻿Imports DTO
Imports DAO
Public Class MuonSachBUS
    Public Function ISSachDCMuon(ByVal key As List(Of String)) As Integer
        Dim ms As New MuonSachDAO()
        Return ms.ISSachCon(key)
    End Function
    Public Function ISMuon(ByVal key1 As Integer, ByVal key2 As Integer) As Integer
        Dim ms As New MuonSachDAO()
        Return ms.ISMuon(key1, key2)
    End Function
    Public Function ListLQ(ByVal key As Integer) As List(Of String)
        Dim CT As New ChiTietMuonDAO()
        Return CT.ListLQ(key)
    End Function
    Public Function Table(ByVal star As Integer, ByVal max As Integer) As DataTable
        Dim msDAO As MuonSachDAO
        msDAO = New MuonSachDAO()
        Return msDAO.Table(star, max)
    End Function
    Public Function Them(ByVal key As MuonSachDTO) As Integer
        Dim msDAO As MuonSachDAO
        msDAO = New MuonSachDAO()
        Return msDAO.Them(key)
    End Function
    Public Function ThemCT(ByVal key As ChiTietMuonDTO) As Integer
        Dim ctmDAO As ChiTietMuonDAO
        ctmDAO = New ChiTietMuonDAO()
        Return ctmDAO.Them(key)
    End Function
    Public Function SuaCT(ByVal key As ChiTietMuonDTO) As Integer
        Dim ctmDAO As ChiTietMuonDAO
        ctmDAO = New ChiTietMuonDAO()
        Return ctmDAO.Sua(key)
    End Function
    Public Function CT(ByVal key As String, ByVal lstID As List(Of String)) As List(Of String)
        Dim ctmDAO As ChiTietMuonDAO
        ctmDAO = New ChiTietMuonDAO()
        Return ctmDAO.CT(key, lstID)
    End Function
    Public Function CTM(ByVal key As String, ByVal lstID As List(Of String)) As List(Of String)
        Dim ctmDAO As ChiTietMuonDAO
        ctmDAO = New ChiTietMuonDAO()
        Return ctmDAO.CTM(key, lstID)
    End Function
    Public Function Xoa(ByVal key As String) As Integer
        Dim msDAO As MuonSachDAO
        msDAO = New MuonSachDAO()
        Return msDAO.Xoa(key)
    End Function
    Public Function Sua(ByVal key As MuonSachDTO) As Integer
        Dim msDAO As MuonSachDAO
        msDAO = New MuonSachDAO()
        Return msDAO.Sua(key)
    End Function
    Public Function TableTim(ByVal thamso As Integer, ByVal key As String) As DataTable
        Dim msDAO As MuonSachDAO
        msDAO = New MuonSachDAO()
        Return msDAO.TableTim(thamso, key)
    End Function

End Class
