﻿Imports System.Configuration
Imports System.Data.SqlClient

Public Class DataProvider
    Shared Function DataConnection() As SqlConnection
        Dim cn As SqlConnection
        cn = New SqlConnection(ConfigurationManager.ConnectionStrings("con").ConnectionString)
        cn.Open()
        Return cn
    End Function
End Class
