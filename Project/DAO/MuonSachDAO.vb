﻿Imports DTO
Imports System.Data.SqlClient

Public Class MuonSachDAO
    Public Function ISSachCon(ByVal key As List(Of String)) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim result As Integer
        Dim strSQL As String
        strSQL = "IS_SDCMuon"
        For Each i As Integer In key
            Dim cmd As SqlCommand
            cmd = New SqlCommand(strSQL, cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@id", SqlDbType.Int)
            cmd.Parameters("@id").Value = Convert.ToInt32(i)
            cmd.Parameters.Add("@result", SqlDbType.Int)
            cmd.Parameters("@result").Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            result = cmd.Parameters("@result").Value
            If result = 0 Then
                Return result
            End If
        Next
        cn.Close()
        Return result
    End Function
    Public Function ISMuon(ByVal key1 As Integer, ByVal key2 As Integer) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim result As Integer
        Dim strSQL As String
        Dim cmd As SqlCommand
        strSQL = "IS_DCMuon"
        cmd = New SqlCommand(strSQL, cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@id", SqlDbType.Int)
        cmd.Parameters("@id").Value = key1
        cmd.Parameters.Add("@sosach", SqlDbType.Int)
        cmd.Parameters("@sosach").Value = key2
        cmd.Parameters.Add("@result", SqlDbType.Int)
        cmd.Parameters("@result").Direction = ParameterDirection.Output
        cmd.ExecuteNonQuery()
        result = cmd.Parameters("@result").Value
        cn.Close()
        Return result
    End Function
    Public Function Table(ByVal star As Integer, ByVal max As Integer) As DataTable
        Dim tabl As New DataTable()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select MS.ID, MS.ID_DocGia, MS.Ngay, DG.TenDG from MuonSach MS join DocGia DG on MS.ID_DocGia=DG.ID Where MS.Xoa=0"
        Dim da As SqlDataAdapter
        da = New SqlDataAdapter(strSQL, cn)
        da.Fill(star, max, tabl)
        cn.Close()
        Return tabl
    End Function
    Public Function Them(ByVal key As MuonSachDTO) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = String.Format("insert into MuonSach values({0},'{1}',0)", key.ID_DocGia, key.Ngay)
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        If result > -1 Then
            strSQL = "st_idn_ms"
            cmd = New SqlCommand(strSQL, cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@result", SqlDbType.Int)
            cmd.Parameters("@result").Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            result = Convert.ToInt32(cmd.Parameters("@result").Value)
        End If
        cn.Close()
        Return result
    End Function
    Public Function Xoa(ByVal key As String) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "update MuonSach set Xoa=1 where ID=" & key.ToString
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function Sua(ByVal key As MuonSachDTO) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = String.Format("update MuonSach set ID_DocGia={0}, Ngay='{1}'
                                where ID=", key.ID_DocGia, key.Ngay) & key.ID
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function TableTim(ByVal thamso As Integer, ByVal key As String) As DataTable
        Dim tabl As New DataTable()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select MS.ID, MS.ID_DocGia, MS.Ngay, DG.TenDG from MuonSach MS join DocGia DG on MS.ID_DocGia=DG.ID Where"
        Select Case thamso
            Case 0
                strSQL = strSQL + " MS.ID=" + key
            Case 1
                strSQL = strSQL + " DG.TenDG like N'%" + key + "%'"
        End Select
        Dim da As SqlDataAdapter
        da = New SqlDataAdapter(strSQL, cn)
        da.Fill(tabl)
        cn.Close()
        Return tabl
    End Function
End Class
