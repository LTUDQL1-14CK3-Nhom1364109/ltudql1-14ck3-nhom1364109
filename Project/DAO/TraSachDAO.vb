﻿Imports DTO
Imports System.Data.SqlClient

Public Class TraSachDAO
    Public Function Table(ByVal star As Integer, ByVal max As Integer) As DataTable
        Dim tabl As New DataTable()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select TS.ID as MaTra, MS.ID as MaMuon, MS.ID_DocGia, MS.Ngay as NgayMuon, TS.Ngay as NgayTra, DG.TenDG, TS.TienPhat from TraSach TS join MuonSach MS on TS.ID_Muon=MS.ID join DocGia DG on MS.ID_DocGia=DG.ID Where TS.Xoa=0"
        Dim da As SqlDataAdapter
        da = New SqlDataAdapter(strSQL, cn)
        da.Fill(star, max, tabl)
        cn.Close()
        Return tabl
    End Function
    Public Function Them(ByVal key As TraSachDTO) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = String.Format("insert into TraSach values({0},'{1}',0,0)", key.ID_Muon, key.Ngay)
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        If result > -1 Then
            strSQL = "st_idn_ts"
            cmd = New SqlCommand(strSQL, cn)
            cmd.CommandType = CommandType.StoredProcedure
            cmd.Parameters.Add("@result", SqlDbType.Int)
            cmd.Parameters("@result").Direction = ParameterDirection.Output
            cmd.ExecuteNonQuery()
            result = Convert.ToInt32(cmd.Parameters("@result").Value)
        End If
        cn.Close()
        Return result
    End Function
    Public Function Xoa(ByVal key As String) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "update MuonSach set Xoa=1 where ID=" & key.ToString
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function Sua(ByVal key As TraSachDTO) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = String.Format("update TraSach set ID_Muon={0}, Ngay='{1}'
                                where ID=", key.ID_Muon, key.Ngay) & key.ID
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function TableTim(ByVal thamso As Integer, ByVal key As String) As DataTable
        Dim tabl As New DataTable()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select TS.ID as MaTra, MS.ID as MaMuon, MS.ID_DocGia, MS.Ngay as NgayMuon, TS.Ngay as NgayTra, DG.TenDG, TS.TienPhat from TraSach TS join MuonSach MS on TS.ID_Muon=MS.ID join DocGia DG on MS.ID_DocGia=DG.ID Where TS.Xoa=0 and"
        Select Case thamso
            Case 0
                strSQL = strSQL + " TS.ID = " + key
            Case 1
                strSQL = strSQL + " DG.TenDG like N'%" + key + "%'"
            Case 2
                strSQL = strSQL + " MS.ID = " + key
        End Select
        Dim da As SqlDataAdapter
        da = New SqlDataAdapter(strSQL, cn)
        da.Fill(tabl)
        cn.Close()
        Return tabl
    End Function
End Class
