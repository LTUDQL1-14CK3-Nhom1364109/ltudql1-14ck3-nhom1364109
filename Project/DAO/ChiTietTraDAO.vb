﻿Imports DTO
Imports System.Data.SqlClient
Public Class ChiTietTraDAO
    Public Function ListLQ(ByVal key As Integer) As List(Of String)
        Dim lstTemp As New List(Of String)()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select * from ChiTietTra CT join Sach S on CT.ID_Sach=S.ID Where ID=" & key
        Dim cmd As SqlCommand
        cmd = New SqlCommand(strSQL, cn)
        Dim dr As SqlDataReader
        dr = cmd.ExecuteReader()
        Dim i As Integer
        i = 0
        While dr.Read()
            lstTemp.Add(dr("ID_Sach"))
        End While
        cn.Close()
        Return lstTemp
    End Function
    Public Function Them(ByVal key As ChiTietTraDTO) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = String.Format("insert into ChiTietTra values({0},{1})", key.ID_Sach, key.ID_Tra)
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
End Class
