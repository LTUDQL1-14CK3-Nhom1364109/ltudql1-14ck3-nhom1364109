﻿Imports DTO
Imports System.Data.SqlClient

Public Class DocGiaDAO
    Public Function ShowTen(ByVal key As String) As String
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim result As String
        Dim strSQL As String
        Dim cmd As SqlCommand
        strSQL = "stTen_DG"
        cmd = New SqlCommand(strSQL, cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@ID", SqlDbType.Int)
        cmd.Parameters("@ID").Value = Convert.ToInt32(key)
        cmd.Parameters.Add("@result", SqlDbType.NVarChar, 50)
        cmd.Parameters("@result").Direction = ParameterDirection.Output
        cmd.ExecuteNonQuery()
        result = cmd.Parameters("@result").Value.ToString
        cn.Close()
        Return result
    End Function
    Public Function ListLQ() As List(Of String)
        Dim lstTemp As New List(Of String)()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select * from LoaiDocGia"
        Dim cmd As SqlCommand
        cmd = New SqlCommand(strSQL, cn)
        Dim dr As SqlDataReader
        dr = cmd.ExecuteReader()
        Dim i As Integer
        i = 0
        While dr.Read()
            lstTemp.Add(dr("TenLoai"))
        End While
        cn.Close()
        Return lstTemp
    End Function
    Public Function Table(ByVal star As Integer, ByVal max As Integer) As DataTable
        Dim tabl As New DataTable()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select DG.ID, DG.TenDG, DG.GTinh, DG.CMND, DG.NgaySinh, DG.ID_Loai, 
                    DG.DiaChi, DG.Tel, DG.Email, DG.SoDangMuon, DG.NgayHetHan, LDG.TenLoai
                    from DocGia DG join LoaiDocGia LDG
                    on DG.ID_Loai = LDG.ID where DG.Xoa=0 "
        Dim da As SqlDataAdapter
        da = New SqlDataAdapter(strSQL, cn)
        da.Fill(star, max, tabl)
        cn.Close()
        Return tabl
    End Function
    Public Function Them(ByVal key As DocGiaDTO) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = String.Format("insert into DocGia  values(N'{0}',N'{1}','{2}','{3}',{4},N'{5}','{6}','{7}'
                                ,0,0,'{8}')", key.TenDG, key.GTinh, key.CMND, key.NgaySinh, key.ID_Loai,
                               key.DiaChi, key.Tel, key.Email, key.NgayHetHan)
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function Xoa(ByVal key As String) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "update DocGia set Xoa=1 where ID=" & key.ToString
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function Sua(ByVal key As DocGiaDTO) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = String.Format("update DocGia  set TenDG=N'{0}', GTinh=N'{1}', CMND='{2}', NgaySinh='{3}',
                                DiaChi=N'{4}',Tel='{5}',Email='{6}',Xoa={7},SoDangMuon={8},
                                NgayHetHan='{9}'
                                where ID=", key.TenDG, key.GTinh, key.CMND, key.NgaySinh,
                               key.DiaChi, key.Tel, key.Email, key.Xoa, key.SoDangMuon, key.NgayHetHan) & key.ID
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function TableTim(ByVal thamso As Integer, ByVal key As String) As DataTable
        Dim tabl As New DataTable()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select DG.ID, DG.TenDG, DG.GTinh, DG.CMND, DG.NgaySinh, DG.ID_Loai, 
                    DG.DiaChi, DG.Tel, DG.Email, DG.SoDangMuon, DG.NgayHetHan, LDG.TenLoai
                    from DocGia DG join LoaiDocGia LDG
                    on DG.ID_Loai = LDG.ID where DG.Xoa=0 "
        Select Case thamso
            Case 0
                strSQL = strSQL + "and DG.ID=" + key
            Case 1
                strSQL = strSQL + "and DG.TenDG like N'%" + key + "%'"
            Case 2
                strSQL = strSQL + "and DG.CMND=" + key
        End Select
        Dim da As SqlDataAdapter
        da = New SqlDataAdapter(strSQL, cn)
        da.Fill(tabl)
        cn.Close()
        Return tabl
    End Function
    Public Function Phi(ByVal key As String) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select LDG.PhiThuongNien from LoaiDocGia LDG join DocGia DG on LDG.ID=DG.ID_Loai where DG.ID=" & key
        Dim cmd As SqlCommand
        cmd = New SqlCommand(strSQL, cn)
        Dim dr As SqlDataReader
        dr = cmd.ExecuteReader()
        dr.Read()
        Dim kq As Integer = dr("PhiThuongNien")
        cn.Close()
        Return kq
    End Function
    Public Function Ghan(ByVal key As String) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "declare @new date
                    set @new=dateadd(year,1,(select NgayHetHan from DocGia where ID=" & key.ToString & "))" &
                    "update DocGia set NgayHetHan=@new where ID=" & key.ToString
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
End Class
