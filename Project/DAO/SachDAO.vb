﻿Imports DTO
Imports System.Data.SqlClient
Public Class SachDAO
    Public Function ShowTen(ByVal key As String) As String
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim result As String
        Dim strSQL As String
        Dim cmd As SqlCommand
        strSQL = "stTen_Sach"
        cmd = New SqlCommand(strSQL, cn)
        cmd.CommandType = CommandType.StoredProcedure
        cmd.Parameters.Add("@ID", SqlDbType.Int)
        cmd.Parameters("@ID").Value = Convert.ToInt32(key)
        cmd.Parameters.Add("@result", SqlDbType.NVarChar, 50)
        cmd.Parameters("@result").Direction = ParameterDirection.Output
        cmd.ExecuteNonQuery()
        result = cmd.Parameters("@result").Value.ToString
        cn.Close()
        Return result
    End Function
    Public Function Table(ByVal star As Integer, ByVal max As Integer) As DataTable
        Dim tabl As New DataTable()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select S.ID, S.ID_LoaiSach, S.TenSach, S.NhaSX, S.NamSX, S.TomTat,  
                    S.TongSach, S.SoSachCon, S.NgayNhap, S.GiaSach, LS.TenLoaiSach
                    from Sach S join LoaiSach LS
                    on S.ID_LoaiSach = LS.ID where S.Xoa=0 "
        Dim da As SqlDataAdapter
        da = New SqlDataAdapter(strSQL, cn)
        da.Fill(star, max, tabl)
        cn.Close()
        Return tabl
    End Function
    Public Function Them(ByVal key As SachDTO) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = String.Format("insert into Sach  values(N'{0}',{1},N'{2}',{3},N'{4}',{5},{6},'{7}'
                                ,0,{8})", key.TenSach, key.ID_LoaiSach, key.NhaSX, key.NamSX, key.TomTat,
                                   key.TongSach, key.SoSachCon, key.NgayNhap, key.GiaSach)
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function Xoa(ByVal key As String) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "update Sach set Xoa=1 where ID=" & key.ToString
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function Sua(ByVal key As SachDTO) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = String.Format("update Sach  set TenSach=N'{0}', ID_LoaiSach={1}, 
        NhaSX=N'{2}', NamSX={3}, TomTat=N'{4}',TongSach={5},
        SoSachCon={6},NgayNhap='{7}',Xoa=0,GiaSach={8}  
        where ID=", key.TenSach, key.ID_LoaiSach, key.NhaSX, key.NamSX, key.TomTat,
        key.TongSach, key.SoSachCon, key.NgayNhap, key.GiaSach) & key.ID
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function TableTim(ByVal thamso As Integer, ByVal key As String) As DataTable
        Dim tabl As New DataTable()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select S.ID, S.ID_LoaiSach, S.TenSach, S.NhaSX, S.NamSX, S.TomTat,  
                    S.TongSach, S.SoSachCon, S.NgayNhap, S.GiaSach, LS.TenLoaiSach
                    from Sach S join LoaiSach LS
                    on S.ID_LoaiSach = LS.ID where S.Xoa=0 "
        Select Case thamso
            Case 0
                strSQL = strSQL + "And S.ID=" + key
            Case 1
                strSQL = strSQL + "And S.TenSach Like N'%" + key + "%'"
            Case 2
                strSQL = strSQL + "and S.NhaSX Like N'%" + key + "%'"
            Case 3
                strSQL = strSQL + "and LS.TenLoaiSach Like N'%" + key + "%'"
            Case 4
                strSQL = strSQL + "and S.TomTat Like N'%" + key + "%'"
        End Select
        Dim da As SqlDataAdapter
        da = New SqlDataAdapter(strSQL, cn)
        da.Fill(tabl)
        cn.Close()
        Return tabl
    End Function
End Class
