﻿Imports DTO
Imports System.Data.SqlClient
Public Class ChiTietMuonDAO
    Public Function ListLQ(ByVal key As Integer) As List(Of String)
        Dim lstTemp As New List(Of String)()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select * from ChiTietMuon CT join Sach S on CT.ID_Sach=S.ID Where ID=" & key
        Dim cmd As SqlCommand
        cmd = New SqlCommand(strSQL, cn)
        Dim dr As SqlDataReader
        dr = cmd.ExecuteReader()
        Dim i As Integer
        i = 0
        While dr.Read()
            lstTemp.Add(dr("ID_Sach"))
        End While
        cn.Close()
        Return lstTemp
    End Function
    Public Function Them(ByVal key As ChiTietMuonDTO) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = String.Format("insert into ChiTietMuon values({0},0,{1})", key.ID_Sach, key.ID_Muon)
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function Sua(ByVal key As ChiTietMuonDTO) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = String.Format("update ChiTietMuon set ID_Sach={0}
                                where ID=", key.ID_Sach) & key.ID
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function CT(ByVal key As String, ByVal lstID As List(Of String)) As List(Of String)
        Dim lst As New List(Of String)()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select * from ChiTietMuon Where ID_Muon=" + key
        Dim dr As SqlDataReader
        dr = New SqlCommand(strSQL, cn).ExecuteReader()
        While dr.Read()
            lst.Add(dr("ID_Sach"))
            lstID.Add(dr("ID"))
        End While
        cn.Close()
        Return lst
    End Function
    Public Function TableTim(ByVal key As String) As DataTable
        Dim tabl As New DataTable()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select * from ChiTietMuon CTM join MuonSach MS on CTM.ID_Muon=MS.ID Where CTM.ID=" + key
        Dim da As SqlDataAdapter
        da = New SqlDataAdapter(strSQL, cn)
        da.Fill(tabl)
        cn.Close()
        Return tabl
    End Function
    Public Function CTM(ByVal key As String, ByVal lstID As List(Of String)) As List(Of String)
        Dim lst As New List(Of String)()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select * from ChiTietMuon Where Tra=0 and ID_Muon=" + key
        Dim dr As SqlDataReader
        dr = New SqlCommand(strSQL, cn).ExecuteReader()
        While dr.Read()
            lst.Add(dr("ID_Sach"))
            lstID.Add(dr("ID"))
        End While
        cn.Close()
        Return lst
    End Function
End Class
