﻿Imports DTO
Imports System.Data.SqlClient

Public Class GroupDAO
    Public Function LstLoai() As List(Of String)
        Dim lst As New List(Of String)()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select * from LoaiSach"
        Dim cmd As SqlCommand
        cmd = New SqlCommand(strSQL, cn)
        Dim dr As SqlDataReader
        dr = cmd.ExecuteReader()
        While dr.Read()
            lst.Add(dr("TenLoaiSach"))
        End While
        Return lst
    End Function
    Public Function Table() As DataTable
        Dim tabl As New DataTable()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select * from LoaiSach Where Xoa=0"
        Dim da As SqlDataAdapter
        da = New SqlDataAdapter(strSQL, cn)
        da.Fill(tabl)
        cn.Close()
        Return tabl
    End Function
    Public Function Them(ByVal key As GroupDTO) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = String.Format("insert into LoaiSach values(N'{0}',N'{1}',0)", key.TenLoai, key.MoTa)
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function Xoa(ByVal key As String) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "update LoaiSach set Xoa=1 where ID=" & key.ToString
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function Sua(ByVal key As GroupDTO) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = String.Format("update LoaiSach set TenLoaiSach=N'{0}', MoTa=N'{1}' where ID=", key.TenLoai, key.MoTa) & key.ID
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function TableTim(ByVal thamso As Integer, ByVal key As String) As DataTable
        Dim tabl As New DataTable()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select LS.ID, LS.TenLoaiSach, LS.MoTa
                    from LoaiSach LS
                    where LS.Xoa=0 "
        Select Case thamso
            Case 0
                strSQL = strSQL + "and LS.ID=" + key
            Case 1
                strSQL = strSQL + "and LS.TenLoaiSach. like N'%" + key + "%'"
            Case 2
                strSQL = strSQL + "and LS.MoTa like N'%" + key + "%'"
        End Select
        Dim da As SqlDataAdapter
        da = New SqlDataAdapter(strSQL, cn)
        da.Fill(tabl)
        cn.Close()
        Return tabl
    End Function
End Class
