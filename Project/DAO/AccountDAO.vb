﻿Imports DTO
Imports System.Data.SqlClient


Public Class AccountDAO
    Public Function IsLogin(ByVal ac As AccountDTO) As Boolean
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select * from Account"
        Dim cmd As SqlCommand
        cmd = New SqlCommand(strSQL, cn)
        Dim dr As SqlDataReader
        dr = cmd.ExecuteReader()
        While dr.Read()
            If String.Compare(ac.User, dr("UserName")) = 0 Then
                If String.Compare(ac.Pass, dr("Password")) = 0 Then
                    ac.Name = dr("FullName")
                    Return True
                End If
            End If
        End While
        Return False
    End Function
    Public Function Table() As DataTable
        Dim tabl As New DataTable()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select * from Account Where Xoa=0"
        Dim da As SqlDataAdapter
        da = New SqlDataAdapter(strSQL, cn)
        da.Fill(tabl)
        cn.Close()
        Return tabl
    End Function
    Public Function Them(ByVal key As AccountDTO) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = String.Format("insert into Account  values(N'{0}',N'{1}',N'{2}',0)", key.User, key.Pass, key.Name)
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function Xoa(ByVal key As String) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "update Account set Xoa=1 where ID=" & key.ToString
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function Sua(ByVal key As AccountDTO) As Integer
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = String.Format("update Account set UserName=N'{0}', Password=N'{1}', FullName=N'{2}'
                                where ID=", key.User, key.Pass, key.Name) & key.ID
        Dim result = -1
        Dim cmd As New SqlCommand(strSQL, cn)
        result = cmd.ExecuteNonQuery()
        cn.Close()
        Return result
    End Function
    Public Function TableTim(ByVal thamso As Integer, ByVal key As String) As DataTable
        Dim tabl As New DataTable()
        Dim cn As SqlConnection
        cn = DataProvider.DataConnection()
        Dim strSQL As String
        strSQL = "select ACC.UserName, ACC.Password, ACC.FullName
                    from Account ACC
                    where ACC.Xoa=0 "
        Select Case thamso
            Case 0
                strSQL = strSQL + "and ACC.ID=" + key
            Case 1
                strSQL = strSQL + "and ACC.UserName like N'%" + key + "%'"
            Case 2
                strSQL = strSQL + "and ACC.FullName like N'%" + key + "%'"
        End Select
        Dim da As SqlDataAdapter
        da = New SqlDataAdapter(strSQL, cn)
        da.Fill(tabl)
        cn.Close()
        Return tabl
    End Function
End Class
