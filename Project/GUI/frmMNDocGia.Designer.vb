﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMNDocGia
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.dgvTable = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column11 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column12 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxTim = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnTim = New System.Windows.Forms.Button()
        Me.txtKey = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtDiaChi = New System.Windows.Forms.TextBox()
        Me.dtpNSinh = New System.Windows.Forms.DateTimePicker()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtGTinh = New System.Windows.Forms.TextBox()
        Me.txtTen = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cbxChucVu = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtEmail = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.dtpNHH = New System.Windows.Forms.DateTimePicker()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.txtCMND = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtFone = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnFirst = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnPre = New System.Windows.Forms.Button()
        Me.btnPrice = New System.Windows.Forms.Button()
        Me.btnBor = New System.Windows.Forms.Button()
        Me.btnPay = New System.Windows.Forms.Button()
        Me.lblTrang = New System.Windows.Forms.Label()
        Me.btnXemCT = New System.Windows.Forms.Button()
        CType(Me.dgvTable, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 20.0!)
        Me.lblTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblTitle.Location = New System.Drawing.Point(444, 39)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(212, 33)
        Me.lblTitle.TabIndex = 0
        Me.lblTitle.Text = "Quản Lý Độc Giả"
        '
        'dgvTable
        '
        Me.dgvTable.AllowUserToOrderColumns = True
        Me.dgvTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvTable.BackgroundColor = System.Drawing.SystemColors.ActiveCaption
        Me.dgvTable.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvTable.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SunkenVertical
        Me.dgvTable.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken
        Me.dgvTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTable.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column8, Me.Column10, Me.Column9, Me.Column5, Me.Column6, Me.Column11, Me.Column12, Me.Column7})
        Me.dgvTable.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.dgvTable.GridColor = System.Drawing.SystemColors.ActiveCaption
        Me.dgvTable.Location = New System.Drawing.Point(0, 458)
        Me.dgvTable.Name = "dgvTable"
        Me.dgvTable.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvTable.Size = New System.Drawing.Size(1100, 260)
        Me.dgvTable.StandardTab = True
        Me.dgvTable.TabIndex = 1
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "ID"
        Me.Column1.HeaderText = "Mã"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "TenDG"
        Me.Column2.HeaderText = "Họ Tên"
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "GTinh"
        Me.Column3.HeaderText = "Giới Tính"
        Me.Column3.Name = "Column3"
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "CMND"
        Me.Column4.HeaderText = "CMND"
        Me.Column4.Name = "Column4"
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "Email"
        Me.Column8.HeaderText = "Email"
        Me.Column8.Name = "Column8"
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "Tel"
        Me.Column10.HeaderText = "Điện Thoại"
        Me.Column10.Name = "Column10"
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "NgaySinh"
        Me.Column9.HeaderText = "Ngày Sinh"
        Me.Column9.Name = "Column9"
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "DiaChi"
        Me.Column5.HeaderText = "Địa Chỉ"
        Me.Column5.Name = "Column5"
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "TenLoai"
        Me.Column6.HeaderText = "Chức Vụ"
        Me.Column6.Name = "Column6"
        '
        'Column11
        '
        Me.Column11.DataPropertyName = "SoDangMuon"
        Me.Column11.HeaderText = "Đang Mượn"
        Me.Column11.Name = "Column11"
        '
        'Column12
        '
        Me.Column12.DataPropertyName = "NgayHetHan"
        Me.Column12.HeaderText = "Hết Hạn"
        Me.Column12.Name = "Column12"
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "ID_Loai"
        Me.Column7.HeaderText = "ID_CV"
        Me.Column7.Name = "Column7"
        Me.Column7.Visible = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cbxTim)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.btnTim)
        Me.GroupBox1.Controls.Add(Me.txtKey)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 86)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(295, 135)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(4, 91)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 16)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Tìm Theo"
        '
        'cbxTim
        '
        Me.cbxTim.FormattingEnabled = True
        Me.cbxTim.Items.AddRange(New Object() {"Mã Độc Giả", "Họ Tên", "CMND"})
        Me.cbxTim.Location = New System.Drawing.Point(73, 87)
        Me.cbxTim.Name = "cbxTim"
        Me.cbxTim.Size = New System.Drawing.Size(121, 24)
        Me.cbxTim.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(4, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Từ Khoá"
        '
        'btnTim
        '
        Me.btnTim.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnTim.Location = New System.Drawing.Point(200, 88)
        Me.btnTim.Name = "btnTim"
        Me.btnTim.Size = New System.Drawing.Size(89, 23)
        Me.btnTim.TabIndex = 1
        Me.btnTim.Text = "Tìm Kiếm"
        Me.btnTim.UseVisualStyleBackColor = True
        '
        'txtKey
        '
        Me.txtKey.Location = New System.Drawing.Point(66, 35)
        Me.txtKey.Name = "txtKey"
        Me.txtKey.Size = New System.Drawing.Size(223, 23)
        Me.txtKey.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupBox2.Location = New System.Drawing.Point(333, 86)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(736, 326)
        Me.GroupBox2.TabIndex = 3
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Thông Tin"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 7
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 9.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 41.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.txtDiaChi, 2, 8)
        Me.TableLayoutPanel1.Controls.Add(Me.dtpNSinh, 2, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtID, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtGTinh, 2, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.txtTen, 2, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label5, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.Label11, 0, 8)
        Me.TableLayoutPanel1.Controls.Add(Me.Label9, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.cbxChucVu, 6, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label6, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtEmail, 6, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label12, 4, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.dtpNHH, 6, 8)
        Me.TableLayoutPanel1.Controls.Add(Me.Label10, 4, 8)
        Me.TableLayoutPanel1.Controls.Add(Me.Label7, 4, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.txtCMND, 6, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.Label8, 4, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.txtFone, 6, 4)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(6, 38)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 9
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(698, 224)
        Me.TableLayoutPanel1.TabIndex = 14
        '
        'txtDiaChi
        '
        Me.txtDiaChi.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtDiaChi.Location = New System.Drawing.Point(91, 200)
        Me.txtDiaChi.Name = "txtDiaChi"
        Me.txtDiaChi.Size = New System.Drawing.Size(245, 23)
        Me.txtDiaChi.TabIndex = 12
        '
        'dtpNSinh
        '
        Me.dtpNSinh.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpNSinh.Location = New System.Drawing.Point(91, 151)
        Me.dtpNSinh.Name = "dtpNSinh"
        Me.dtpNSinh.Size = New System.Drawing.Size(245, 23)
        Me.dtpNSinh.TabIndex = 9
        Me.dtpNSinh.Value = New Date(1990, 12, 25, 0, 0, 0, 0)
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(73, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Mã Độc Giả"
        '
        'txtID
        '
        Me.txtID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtID.Location = New System.Drawing.Point(91, 3)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(245, 23)
        Me.txtID.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(27, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(49, 16)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Họ Tên"
        '
        'txtGTinh
        '
        Me.txtGTinh.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtGTinh.Location = New System.Drawing.Point(91, 102)
        Me.txtGTinh.Name = "txtGTinh"
        Me.txtGTinh.Size = New System.Drawing.Size(245, 23)
        Me.txtGTinh.TabIndex = 11
        '
        'txtTen
        '
        Me.txtTen.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTen.Location = New System.Drawing.Point(91, 53)
        Me.txtTen.Name = "txtTen"
        Me.txtTen.Size = New System.Drawing.Size(245, 23)
        Me.txtTen.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(18, 105)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(58, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Giới Tính"
        '
        'Label11
        '
        Me.Label11.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(27, 203)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(49, 16)
        Me.Label11.TabIndex = 13
        Me.Label11.Text = "Địa Chỉ"
        '
        'Label9
        '
        Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(11, 154)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(65, 16)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Ngày Sinh"
        '
        'cbxChucVu
        '
        Me.cbxChucVu.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxChucVu.FormattingEnabled = True
        Me.cbxChucVu.Location = New System.Drawing.Point(484, 3)
        Me.cbxChucVu.Name = "cbxChucVu"
        Me.cbxChucVu.Size = New System.Drawing.Size(211, 24)
        Me.cbxChucVu.TabIndex = 12
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(412, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(56, 16)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Chức Vụ"
        '
        'txtEmail
        '
        Me.txtEmail.Location = New System.Drawing.Point(484, 53)
        Me.txtEmail.Name = "txtEmail"
        Me.txtEmail.Size = New System.Drawing.Size(188, 23)
        Me.txtEmail.TabIndex = 16
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(429, 50)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(39, 16)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = "Email"
        '
        'dtpNHH
        '
        Me.dtpNHH.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpNHH.Location = New System.Drawing.Point(484, 200)
        Me.dtpNHH.Name = "dtpNHH"
        Me.dtpNHH.Size = New System.Drawing.Size(211, 23)
        Me.dtpNHH.TabIndex = 8
        '
        'Label10
        '
        Me.Label10.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(383, 203)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(85, 16)
        Me.Label10.TabIndex = 7
        Me.Label10.Text = "Ngày Hết Hạn"
        '
        'Label7
        '
        Me.Label7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(426, 148)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(42, 16)
        Me.Label7.TabIndex = 4
        Me.Label7.Text = "CMND"
        '
        'txtCMND
        '
        Me.txtCMND.Location = New System.Drawing.Point(484, 151)
        Me.txtCMND.Name = "txtCMND"
        Me.txtCMND.Size = New System.Drawing.Size(185, 23)
        Me.txtCMND.TabIndex = 13
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(413, 99)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(55, 16)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "Số Fone"
        '
        'txtFone
        '
        Me.txtFone.Location = New System.Drawing.Point(484, 102)
        Me.txtFone.Name = "txtFone"
        Me.txtFone.Size = New System.Drawing.Size(100, 23)
        Me.txtFone.TabIndex = 14
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnFirst)
        Me.GroupBox3.Controls.Add(Me.btnNext)
        Me.GroupBox3.Controls.Add(Me.btnPre)
        Me.GroupBox3.Location = New System.Drawing.Point(20, 396)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(287, 56)
        Me.GroupBox3.TabIndex = 4
        Me.GroupBox3.TabStop = False
        '
        'btnFirst
        '
        Me.btnFirst.Location = New System.Drawing.Point(6, 15)
        Me.btnFirst.Name = "btnFirst"
        Me.btnFirst.Size = New System.Drawing.Size(93, 29)
        Me.btnFirst.TabIndex = 2
        Me.btnFirst.Text = "Trang Đầu"
        Me.btnFirst.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(105, 15)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 29)
        Me.btnNext.TabIndex = 1
        Me.btnNext.Text = "Trang Kế"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnPre
        '
        Me.btnPre.Location = New System.Drawing.Point(186, 15)
        Me.btnPre.Name = "btnPre"
        Me.btnPre.Size = New System.Drawing.Size(95, 29)
        Me.btnPre.TabIndex = 0
        Me.btnPre.Text = "Trang Trước"
        Me.btnPre.UseVisualStyleBackColor = True
        '
        'btnPrice
        '
        Me.btnPrice.Location = New System.Drawing.Point(417, 423)
        Me.btnPrice.Name = "btnPrice"
        Me.btnPrice.Size = New System.Drawing.Size(85, 23)
        Me.btnPrice.TabIndex = 5
        Me.btnPrice.Text = "Gia Hạn Phí"
        Me.btnPrice.UseVisualStyleBackColor = True
        '
        'btnBor
        '
        Me.btnBor.Location = New System.Drawing.Point(529, 423)
        Me.btnBor.Name = "btnBor"
        Me.btnBor.Size = New System.Drawing.Size(91, 23)
        Me.btnBor.TabIndex = 6
        Me.btnBor.Text = "Mượn Sách"
        Me.btnBor.UseVisualStyleBackColor = True
        '
        'btnPay
        '
        Me.btnPay.Location = New System.Drawing.Point(642, 423)
        Me.btnPay.Name = "btnPay"
        Me.btnPay.Size = New System.Drawing.Size(86, 23)
        Me.btnPay.TabIndex = 7
        Me.btnPay.Text = "Trả Sách"
        Me.btnPay.UseVisualStyleBackColor = True
        '
        'lblTrang
        '
        Me.lblTrang.AutoSize = True
        Me.lblTrang.Location = New System.Drawing.Point(313, 430)
        Me.lblTrang.Name = "lblTrang"
        Me.lblTrang.Size = New System.Drawing.Size(0, 16)
        Me.lblTrang.TabIndex = 8
        '
        'btnXemCT
        '
        Me.btnXemCT.Location = New System.Drawing.Point(754, 423)
        Me.btnXemCT.Name = "btnXemCT"
        Me.btnXemCT.Size = New System.Drawing.Size(98, 23)
        Me.btnXemCT.TabIndex = 51
        Me.btnXemCT.Text = "Xem Chi Tiết"
        Me.btnXemCT.UseVisualStyleBackColor = True
        '
        'frmMNDocGia
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(1100, 718)
        Me.Controls.Add(Me.btnXemCT)
        Me.Controls.Add(Me.lblTrang)
        Me.Controls.Add(Me.btnPay)
        Me.Controls.Add(Me.btnBor)
        Me.Controls.Add(Me.btnPrice)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvTable)
        Me.Controls.Add(Me.lblTitle)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "frmMNDocGia"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmMNDocGia"
        CType(Me.dgvTable, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblTitle As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label2 As Label
    Friend WithEvents btnTim As Button
    Friend WithEvents txtKey As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents btnFirst As Button
    Friend WithEvents btnNext As Button
    Friend WithEvents btnPre As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents cbxTim As ComboBox
    Friend WithEvents dgvTable As DataGridView
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents txtDiaChi As TextBox
    Friend WithEvents dtpNSinh As DateTimePicker
    Friend WithEvents Label3 As Label
    Friend WithEvents txtID As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtGTinh As TextBox
    Friend WithEvents txtTen As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents dtpNHH As DateTimePicker
    Friend WithEvents txtCMND As TextBox
    Friend WithEvents txtFone As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents cbxChucVu As ComboBox
    Friend WithEvents txtEmail As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents btnPrice As Button
    Friend WithEvents btnBor As Button
    Friend WithEvents btnPay As Button
    Friend WithEvents lblTrang As Label
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column8 As DataGridViewTextBoxColumn
    Friend WithEvents Column10 As DataGridViewTextBoxColumn
    Friend WithEvents Column9 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents Column11 As DataGridViewTextBoxColumn
    Friend WithEvents Column12 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents btnXemCT As Button
End Class
