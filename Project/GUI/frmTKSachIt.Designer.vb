﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmTKSachIt
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim ReportDataSource1 As Microsoft.Reporting.WinForms.ReportDataSource = New Microsoft.Reporting.WinForms.ReportDataSource()
        Me.ReportViewer1 = New Microsoft.Reporting.WinForms.ReportViewer()
        Me.DataSetCT = New GUI.DataSetCT()
        Me.SachItBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SachItTableAdapter = New GUI.DataSetCTTableAdapters.SachItTableAdapter()
        CType(Me.DataSetCT, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SachItBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ReportViewer1
        '
        Me.ReportViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        ReportDataSource1.Name = "SachIt"
        ReportDataSource1.Value = Me.SachItBindingSource
        Me.ReportViewer1.LocalReport.DataSources.Add(ReportDataSource1)
        Me.ReportViewer1.LocalReport.ReportEmbeddedResource = "GUI.ReportSachIt.rdlc"
        Me.ReportViewer1.Location = New System.Drawing.Point(0, 0)
        Me.ReportViewer1.Name = "ReportViewer1"
        Me.ReportViewer1.Size = New System.Drawing.Size(284, 261)
        Me.ReportViewer1.TabIndex = 0
        '
        'DataSetCT
        '
        Me.DataSetCT.DataSetName = "DataSetCT"
        Me.DataSetCT.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SachItBindingSource
        '
        Me.SachItBindingSource.DataMember = "SachIt"
        Me.SachItBindingSource.DataSource = Me.DataSetCT
        '
        'SachItTableAdapter
        '
        Me.SachItTableAdapter.ClearBeforeFill = True
        '
        'frmTKSachIt
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 261)
        Me.Controls.Add(Me.ReportViewer1)
        Me.Name = "frmTKSachIt"
        Me.Text = "frmTKSachIt"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        CType(Me.DataSetCT, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SachItBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents ReportViewer1 As Microsoft.Reporting.WinForms.ReportViewer
    Friend WithEvents SachItBindingSource As BindingSource
    Friend WithEvents DataSetCT As DataSetCT
    Friend WithEvents SachItTableAdapter As DataSetCTTableAdapters.SachItTableAdapter
End Class
