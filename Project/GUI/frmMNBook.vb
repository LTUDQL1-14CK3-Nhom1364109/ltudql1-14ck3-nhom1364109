﻿Imports DTO
Imports BUS
Public Class frmMNBook

#Region "This"
    Private star As Integer
    Private max As Integer
    Private SBus As New SachBUS()
    Private LSBus As New GroupBUS()

    Private Sub frmMNBook_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cbxTim.SelectedIndex = 0
        cbxLoaiSach.DataSource = LSBus.LstLoai()
        star = 0
        max = 10
    End Sub
    Private Function SetDTO() As SachDTO
        Dim sDTO As New SachDTO()
        If txtID.Text = "" Then
            sDTO.ID = 0
        Else
            sDTO.ID = Convert.ToInt32(txtID.Text)
        End If

        sDTO.ID_LoaiSach = cbxLoaiSach.SelectedIndex + 1
        sDTO.NgayNhap = dtpNgNhap.Value.ToShortDateString()
        sDTO.NamSX = txtNam.Text
        sDTO.NhaSX = txtNSX.Text
        sDTO.SoSachCon = txtSoSachCon.Text
        sDTO.TomTat = txtTomTat.Text
        sDTO.TongSach = txtTongSL.Text
        sDTO.GiaSach = txtGia.Text
        sDTO.TenSach = txtTen.Text
        Return sDTO
    End Function
    Private Sub dgvTable_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTable.CellClick
        Dim row As Integer
        row = dgvTable.CurrentRow.Index
        txtID.Text = dgvTable.Rows(row).Cells(0).Value().ToString
        txtTen.Text = dgvTable.Rows(row).Cells(2).Value().ToString
        txtNSX.Text = dgvTable.Rows(row).Cells(3).Value().ToString
        txtNam.Text = dgvTable.Rows(row).Cells(4).Value().ToString
        txtTomTat.Text = dgvTable.Rows(row).Cells(5).Value().ToString
        txtTongSL.Text = dgvTable.Rows(row).Cells(6).Value().ToString
        txtSoSachCon.Text = dgvTable.Rows(row).Cells(7).Value().ToString
        dtpNgNhap.Value = dgvTable.Rows(row).Cells(8).Value().ToString
        txtGia.Text = dgvTable.Rows(row).Cells(9).Value().ToString
        cbxLoaiSach.Text = dgvTable.Rows(row).Cells(10).Value().ToString
    End Sub
#End Region
#Region "Tool"
    Public Sub HienDS(ByVal i As Integer, ByVal s As Integer)
        max = s
        star = i
        If star = 0 Then
            btnFirst.Visible = False
            btnPre.Visible = False
        Else
            btnFirst.Visible = True
            btnPre.Visible = True
        End If
        If SBus.Table(star + 10, max).Rows.Count = 0 Then
            btnNext.Visible = False
        Else
            btnNext.Visible = True
        End If
        dgvTable.DataSource = SBus.Table(star, max)
    End Sub
    Public Sub Them()
        If txtTen.Text <> "" Then
            If MessageBox.Show("Ban Co Chac Chan Muon Them Thong Tin", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                If SBus.Them(SetDTO()) > -1 Then
                    MessageBox.Show("Thành Công")
                    HienDS(star, max)
                Else
                    MessageBox.Show("Vui long nhap Day Du Thong Tin")
                End If
            End If
        End If
    End Sub
    Public Sub Sua()
        If Me.Validate = False Then
            Me.Show()
        ElseIf txtTen.Text <> "" Then
            If MessageBox.Show("Ban Co Chac Chan Muon Sua Thong Tin", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                If SBus.Sua(SetDTO()) > -1 Then
                    MessageBox.Show("Thành Công")
                    HienDS(star, max)
                Else
                    MessageBox.Show("Vui long nhap Day Du Thong Tin")
                End If
            End If
        End If
    End Sub
    Public Sub Xoa()
        If txtTen.Text <> "" Then
            If MessageBox.Show("Ban Co Chac Chan Muon Xoa Thong Tin ", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                If SBus.Xoa(txtID.Text) > -1 Then
                    MessageBox.Show("Xoa Thanh Cong")
                    HienDS(star, max)
                Else
                    MessageBox.Show("Vui long nhap Ma Doc Gia")
                End If
            End If
        End If
    End Sub

#End Region
#Region "Phan Trang"
    Private Sub btnFirst_Click(sender As Object, e As EventArgs) Handles btnFirst.Click
        star = 0
        HienDS(star, max)
    End Sub

    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        star += max
        HienDS(star, max)
    End Sub

    Private Sub btnPre_Click(sender As Object, e As EventArgs) Handles btnPre.Click
        star -= max
        If star < 0 Then star = 0
        HienDS(star, max)
    End Sub

    Private Sub btnTim_Click(sender As Object, e As EventArgs) Handles btnTim.Click
        Dim tab As DataTable
        tab = SBus.TableTim(cbxTim.SelectedIndex, txtKey.Text)
        If tab.Rows.Count() = 0 Then
            MessageBox.Show("Khong tim thay du lieu")
        Else
            dgvTable.DataSource = tab
        End If
    End Sub

    Private Sub btnBor_Click(sender As Object, e As EventArgs) Handles btnBor.Click
        frmParent.ShowFrm(frmMNMuon)
        frmMNMuon.load(txtID.Text, 2)
    End Sub

    Private Sub btnPay_Click(sender As Object, e As EventArgs) Handles btnPay.Click
        frmParent.ShowFrm(frmMNPay)
        frmMNPay.load(txtID.Text, 2)
    End Sub

    Private Sub btnXemCT_Click(sender As Object, e As EventArgs) Handles btnXemCT.Click
        frmCTSach.Value = Convert.ToInt32(txtID.Text)
        frmCTSach.Show()
    End Sub
#End Region
End Class