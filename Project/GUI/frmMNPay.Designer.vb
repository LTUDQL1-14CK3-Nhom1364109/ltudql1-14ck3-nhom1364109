﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMNPay
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtTen3 = New System.Windows.Forms.TextBox()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxTim = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnTim = New System.Windows.Forms.Button()
        Me.txtKey = New System.Windows.Forms.TextBox()
        Me.dgvTable = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.txtTen2 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.lblTrang = New System.Windows.Forms.Label()
        Me.txtMa1 = New System.Windows.Forms.TextBox()
        Me.txtMaPhieu = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtMa2 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnFirst = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnPre = New System.Windows.Forms.Button()
        Me.txtMa5 = New System.Windows.Forms.TextBox()
        Me.txtTen5 = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtMaDG = New System.Windows.Forms.TextBox()
        Me.txtTenDG = New System.Windows.Forms.TextBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.txtTen4 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtTen1 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtMa3 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtMa4 = New System.Windows.Forms.TextBox()
        Me.dtpNgay = New System.Windows.Forms.DateTimePicker()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.dtpTra = New System.Windows.Forms.DateTimePicker()
        Me.cb1 = New System.Windows.Forms.CheckBox()
        Me.cb2 = New System.Windows.Forms.CheckBox()
        Me.cb3 = New System.Windows.Forms.CheckBox()
        Me.cb4 = New System.Windows.Forms.CheckBox()
        Me.cb5 = New System.Windows.Forms.CheckBox()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.txtMaTra = New System.Windows.Forms.TextBox()
        Me.lblPhat = New System.Windows.Forms.Label()
        Me.btnXemCT = New System.Windows.Forms.Button()
        Me.btnInCT = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvTable, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(3, 265)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(72, 16)
        Me.Label15.TabIndex = 27
        Me.Label15.Text = "Ngày Mượn"
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(7, 26)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Mã Sách 1"
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(356, 118)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(73, 16)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "Tên Sách 3"
        '
        'txtTen3
        '
        Me.txtTen3.Location = New System.Drawing.Point(449, 121)
        Me.txtTen3.Name = "txtTen3"
        Me.txtTen3.Size = New System.Drawing.Size(245, 23)
        Me.txtTen3.TabIndex = 14
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(7, 195)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(96, 16)
        Me.Label16.TabIndex = 44
        Me.Label16.Text = "Mã Phiếu Mượn"
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 20.0!)
        Me.lblTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblTitle.Location = New System.Drawing.Point(438, 3)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(224, 33)
        Me.lblTitle.TabIndex = 37
        Me.lblTitle.Text = "Quản Lý Trả Sách"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cbxTim)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.btnTim)
        Me.GroupBox1.Controls.Add(Me.txtKey)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 50)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(295, 135)
        Me.GroupBox1.TabIndex = 39
        Me.GroupBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(4, 91)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 16)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Tìm Theo"
        '
        'cbxTim
        '
        Me.cbxTim.FormattingEnabled = True
        Me.cbxTim.Items.AddRange(New Object() {"Mã Phiếu Trả", "Tên Độc Giả"})
        Me.cbxTim.Location = New System.Drawing.Point(73, 87)
        Me.cbxTim.Name = "cbxTim"
        Me.cbxTim.Size = New System.Drawing.Size(121, 24)
        Me.cbxTim.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(4, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Từ Khoá"
        '
        'btnTim
        '
        Me.btnTim.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnTim.Location = New System.Drawing.Point(200, 88)
        Me.btnTim.Name = "btnTim"
        Me.btnTim.Size = New System.Drawing.Size(89, 23)
        Me.btnTim.TabIndex = 1
        Me.btnTim.Text = "Tìm Kiếm"
        Me.btnTim.UseVisualStyleBackColor = True
        '
        'txtKey
        '
        Me.txtKey.Location = New System.Drawing.Point(66, 35)
        Me.txtKey.Name = "txtKey"
        Me.txtKey.Size = New System.Drawing.Size(223, 23)
        Me.txtKey.TabIndex = 0
        '
        'dgvTable
        '
        Me.dgvTable.AllowUserToOrderColumns = True
        Me.dgvTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvTable.BackgroundColor = System.Drawing.SystemColors.ActiveCaption
        Me.dgvTable.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvTable.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SunkenVertical
        Me.dgvTable.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken
        Me.dgvTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTable.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column7, Me.Column5, Me.Column2, Me.Column3, Me.Column6, Me.Column4, Me.Column8})
        Me.dgvTable.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.dgvTable.GridColor = System.Drawing.SystemColors.ActiveCaption
        Me.dgvTable.Location = New System.Drawing.Point(0, 470)
        Me.dgvTable.Name = "dgvTable"
        Me.dgvTable.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvTable.Size = New System.Drawing.Size(1100, 248)
        Me.dgvTable.StandardTab = True
        Me.dgvTable.TabIndex = 38
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "MaTra"
        Me.Column1.HeaderText = "Mã Phiếu trả"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "MaMuon"
        Me.Column7.HeaderText = "ID_Muon"
        Me.Column7.Name = "Column7"
        Me.Column7.Visible = False
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "MaMuon"
        Me.Column5.HeaderText = "Mã Mượn"
        Me.Column5.Name = "Column5"
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "TenDG"
        Me.Column2.HeaderText = "Tên Độc Giả"
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "NgayMuon"
        Me.Column3.HeaderText = "Ngày Mượn"
        Me.Column3.Name = "Column3"
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "NgayTra"
        Me.Column6.HeaderText = "Ngày Trả"
        Me.Column6.Name = "Column6"
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "ID_DocGia"
        Me.Column4.HeaderText = "ID_DG"
        Me.Column4.Name = "Column4"
        Me.Column4.Visible = False
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "TienPhat"
        Me.Column8.HeaderText = "Tiền Phạt"
        Me.Column8.Name = "Column8"
        '
        'txtTen2
        '
        Me.txtTen2.Location = New System.Drawing.Point(449, 72)
        Me.txtTen2.Name = "txtTen2"
        Me.txtTen2.Size = New System.Drawing.Size(245, 23)
        Me.txtTen2.TabIndex = 16
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(356, 69)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(73, 16)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = "Tên Sách 2"
        '
        'lblTrang
        '
        Me.lblTrang.AutoSize = True
        Me.lblTrang.Location = New System.Drawing.Point(313, 394)
        Me.lblTrang.Name = "lblTrang"
        Me.lblTrang.Size = New System.Drawing.Size(0, 16)
        Me.lblTrang.TabIndex = 42
        '
        'txtMa1
        '
        Me.txtMa1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMa1.Location = New System.Drawing.Point(90, 23)
        Me.txtMa1.Name = "txtMa1"
        Me.txtMa1.Size = New System.Drawing.Size(245, 23)
        Me.txtMa1.TabIndex = 9
        '
        'txtMaPhieu
        '
        Me.txtMaPhieu.Location = New System.Drawing.Point(106, 192)
        Me.txtMaPhieu.Name = "txtMaPhieu"
        Me.txtMaPhieu.Size = New System.Drawing.Size(100, 23)
        Me.txtMaPhieu.TabIndex = 45
        '
        'Label10
        '
        Me.Label10.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(356, 222)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(73, 16)
        Me.Label10.TabIndex = 7
        Me.Label10.Text = "Tên Sách 5"
        '
        'txtMa2
        '
        Me.txtMa2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMa2.Location = New System.Drawing.Point(90, 72)
        Me.txtMa2.Name = "txtMa2"
        Me.txtMa2.Size = New System.Drawing.Size(245, 23)
        Me.txtMa2.TabIndex = 11
        '
        'Label9
        '
        Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(7, 124)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(68, 16)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Mã Sách 3"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(3, 216)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(68, 16)
        Me.Label6.TabIndex = 20
        Me.Label6.Text = "Mã Sách 5"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnFirst)
        Me.GroupBox3.Controls.Add(Me.btnNext)
        Me.GroupBox3.Controls.Add(Me.btnPre)
        Me.GroupBox3.Location = New System.Drawing.Point(20, 372)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(287, 56)
        Me.GroupBox3.TabIndex = 41
        Me.GroupBox3.TabStop = False
        '
        'btnFirst
        '
        Me.btnFirst.Location = New System.Drawing.Point(6, 15)
        Me.btnFirst.Name = "btnFirst"
        Me.btnFirst.Size = New System.Drawing.Size(93, 29)
        Me.btnFirst.TabIndex = 2
        Me.btnFirst.Text = "Trang Đầu"
        Me.btnFirst.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(105, 15)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 29)
        Me.btnNext.TabIndex = 1
        Me.btnNext.Text = "Trang Kế"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnPre
        '
        Me.btnPre.Location = New System.Drawing.Point(186, 15)
        Me.btnPre.Name = "btnPre"
        Me.btnPre.Size = New System.Drawing.Size(95, 29)
        Me.btnPre.TabIndex = 0
        Me.btnPre.Text = "Trang Trước"
        Me.btnPre.UseVisualStyleBackColor = True
        '
        'txtMa5
        '
        Me.txtMa5.Location = New System.Drawing.Point(90, 219)
        Me.txtMa5.Name = "txtMa5"
        Me.txtMa5.Size = New System.Drawing.Size(245, 23)
        Me.txtMa5.TabIndex = 21
        '
        'txtTen5
        '
        Me.txtTen5.Location = New System.Drawing.Point(449, 219)
        Me.txtTen5.Name = "txtTen5"
        Me.txtTen5.Size = New System.Drawing.Size(245, 23)
        Me.txtTen5.TabIndex = 22
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label14)
        Me.GroupBox4.Controls.Add(Me.Label13)
        Me.GroupBox4.Controls.Add(Me.txtMaDG)
        Me.GroupBox4.Controls.Add(Me.txtTenDG)
        Me.GroupBox4.Location = New System.Drawing.Point(12, 249)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(295, 100)
        Me.GroupBox4.TabIndex = 43
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Thông Tin Độc Giả"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(55, 61)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(78, 16)
        Me.Label14.TabIndex = 3
        Me.Label14.Text = "Tên Độc Giả"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(55, 29)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(73, 16)
        Me.Label13.TabIndex = 2
        Me.Label13.Text = "Mã Độc Giả"
        '
        'txtMaDG
        '
        Me.txtMaDG.Location = New System.Drawing.Point(155, 26)
        Me.txtMaDG.Name = "txtMaDG"
        Me.txtMaDG.Size = New System.Drawing.Size(100, 23)
        Me.txtMaDG.TabIndex = 1
        '
        'txtTenDG
        '
        Me.txtTenDG.Location = New System.Drawing.Point(155, 61)
        Me.txtTenDG.Name = "txtTenDG"
        Me.txtTenDG.Size = New System.Drawing.Size(100, 23)
        Me.txtTenDG.TabIndex = 0
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 8
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 9.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 15.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 14.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 61.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.Label15, 0, 11)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtMa1, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtTen2, 6, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label12, 4, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label8, 4, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.txtTen3, 6, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.Label10, 4, 9)
        Me.TableLayoutPanel1.Controls.Add(Me.txtTen4, 6, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.Label7, 4, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 4, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.txtTen1, 6, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Label5, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.txtMa2, 2, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.Label9, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.txtMa3, 2, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.Label11, 0, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.txtMa4, 2, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.Label6, 0, 9)
        Me.TableLayoutPanel1.Controls.Add(Me.txtMa5, 2, 9)
        Me.TableLayoutPanel1.Controls.Add(Me.txtTen5, 6, 9)
        Me.TableLayoutPanel1.Controls.Add(Me.dtpNgay, 2, 11)
        Me.TableLayoutPanel1.Controls.Add(Me.Label18, 4, 11)
        Me.TableLayoutPanel1.Controls.Add(Me.dtpTra, 6, 11)
        Me.TableLayoutPanel1.Controls.Add(Me.cb1, 7, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.cb2, 7, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.cb3, 7, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.cb4, 7, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.cb5, 7, 9)
        Me.TableLayoutPanel1.Controls.Add(Me.Label19, 7, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(6, 38)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 12
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(738, 294)
        Me.TableLayoutPanel1.TabIndex = 14
        '
        'txtTen4
        '
        Me.txtTen4.Location = New System.Drawing.Point(449, 170)
        Me.txtTen4.Name = "txtTen4"
        Me.txtTen4.Size = New System.Drawing.Size(245, 23)
        Me.txtTen4.TabIndex = 18
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(356, 167)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(73, 16)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Tên Sách 4"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(356, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 16)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Tên Sách 1"
        '
        'txtTen1
        '
        Me.txtTen1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTen1.Location = New System.Drawing.Point(449, 23)
        Me.txtTen1.Name = "txtTen1"
        Me.txtTen1.Size = New System.Drawing.Size(245, 23)
        Me.txtTen1.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 75)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(68, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Mã Sách 2"
        '
        'txtMa3
        '
        Me.txtMa3.Location = New System.Drawing.Point(90, 121)
        Me.txtMa3.Name = "txtMa3"
        Me.txtMa3.Size = New System.Drawing.Size(245, 23)
        Me.txtMa3.TabIndex = 17
        '
        'Label11
        '
        Me.Label11.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(7, 173)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(68, 16)
        Me.Label11.TabIndex = 13
        Me.Label11.Text = "Mã Sách 4"
        '
        'txtMa4
        '
        Me.txtMa4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMa4.Location = New System.Drawing.Point(90, 170)
        Me.txtMa4.Name = "txtMa4"
        Me.txtMa4.Size = New System.Drawing.Size(245, 23)
        Me.txtMa4.TabIndex = 12
        '
        'dtpNgay
        '
        Me.dtpNgay.Location = New System.Drawing.Point(90, 268)
        Me.dtpNgay.Name = "dtpNgay"
        Me.dtpNgay.Size = New System.Drawing.Size(245, 23)
        Me.dtpNgay.TabIndex = 28
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(356, 265)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(60, 16)
        Me.Label18.TabIndex = 29
        Me.Label18.Text = "Ngày Trả"
        '
        'dtpTra
        '
        Me.dtpTra.Location = New System.Drawing.Point(449, 268)
        Me.dtpTra.Name = "dtpTra"
        Me.dtpTra.Size = New System.Drawing.Size(245, 23)
        Me.dtpTra.TabIndex = 30
        '
        'cb1
        '
        Me.cb1.AutoSize = True
        Me.cb1.Location = New System.Drawing.Point(700, 23)
        Me.cb1.Name = "cb1"
        Me.cb1.Size = New System.Drawing.Size(15, 14)
        Me.cb1.TabIndex = 31
        Me.cb1.UseVisualStyleBackColor = True
        '
        'cb2
        '
        Me.cb2.AutoSize = True
        Me.cb2.Location = New System.Drawing.Point(700, 72)
        Me.cb2.Name = "cb2"
        Me.cb2.Size = New System.Drawing.Size(15, 14)
        Me.cb2.TabIndex = 32
        Me.cb2.UseVisualStyleBackColor = True
        '
        'cb3
        '
        Me.cb3.AutoSize = True
        Me.cb3.Location = New System.Drawing.Point(700, 121)
        Me.cb3.Name = "cb3"
        Me.cb3.Size = New System.Drawing.Size(15, 14)
        Me.cb3.TabIndex = 33
        Me.cb3.UseVisualStyleBackColor = True
        '
        'cb4
        '
        Me.cb4.AutoSize = True
        Me.cb4.Location = New System.Drawing.Point(700, 170)
        Me.cb4.Name = "cb4"
        Me.cb4.Size = New System.Drawing.Size(15, 14)
        Me.cb4.TabIndex = 34
        Me.cb4.UseVisualStyleBackColor = True
        '
        'cb5
        '
        Me.cb5.AutoSize = True
        Me.cb5.Location = New System.Drawing.Point(700, 219)
        Me.cb5.Name = "cb5"
        Me.cb5.Size = New System.Drawing.Size(15, 14)
        Me.cb5.TabIndex = 35
        Me.cb5.UseVisualStyleBackColor = True
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(700, 0)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(28, 16)
        Me.Label19.TabIndex = 36
        Me.Label19.Text = "Trả"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupBox2.Location = New System.Drawing.Point(316, 50)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(763, 338)
        Me.GroupBox2.TabIndex = 40
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Thông Tin Sách Chưa Trả"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(9, 230)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(84, 16)
        Me.Label17.TabIndex = 46
        Me.Label17.Text = "Mã Phiếu Trả"
        '
        'txtMaTra
        '
        Me.txtMaTra.Location = New System.Drawing.Point(106, 222)
        Me.txtMaTra.Name = "txtMaTra"
        Me.txtMaTra.Size = New System.Drawing.Size(100, 23)
        Me.txtMaTra.TabIndex = 47
        '
        'lblPhat
        '
        Me.lblPhat.AutoSize = True
        Me.lblPhat.Location = New System.Drawing.Point(371, 400)
        Me.lblPhat.Name = "lblPhat"
        Me.lblPhat.Size = New System.Drawing.Size(62, 16)
        Me.lblPhat.TabIndex = 48
        Me.lblPhat.Text = "Tiền Phạt"
        '
        'btnXemCT
        '
        Me.btnXemCT.Location = New System.Drawing.Point(518, 400)
        Me.btnXemCT.Name = "btnXemCT"
        Me.btnXemCT.Size = New System.Drawing.Size(98, 23)
        Me.btnXemCT.TabIndex = 49
        Me.btnXemCT.Text = "Xem Chi Tiết"
        Me.btnXemCT.UseVisualStyleBackColor = True
        '
        'btnInCT
        '
        Me.btnInCT.Location = New System.Drawing.Point(663, 400)
        Me.btnInCT.Name = "btnInCT"
        Me.btnInCT.Size = New System.Drawing.Size(88, 23)
        Me.btnInCT.TabIndex = 50
        Me.btnInCT.Text = "In Chi Tiết"
        Me.btnInCT.UseVisualStyleBackColor = True
        '
        'frmMNPay
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(1100, 718)
        Me.Controls.Add(Me.btnInCT)
        Me.Controls.Add(Me.btnXemCT)
        Me.Controls.Add(Me.lblPhat)
        Me.Controls.Add(Me.txtMaTra)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvTable)
        Me.Controls.Add(Me.lblTrang)
        Me.Controls.Add(Me.txtMaPhieu)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox2)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "frmMNPay"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmMNPay"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvTable, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label15 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents txtTen3 As TextBox
    Friend WithEvents Label16 As Label
    Friend WithEvents lblTitle As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cbxTim As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents btnTim As Button
    Friend WithEvents txtKey As TextBox
    Friend WithEvents dgvTable As DataGridView
    Friend WithEvents txtTen2 As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents lblTrang As Label
    Friend WithEvents txtMa1 As TextBox
    Friend WithEvents txtMaPhieu As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtMa2 As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents btnFirst As Button
    Friend WithEvents btnNext As Button
    Friend WithEvents btnPre As Button
    Friend WithEvents txtMa5 As TextBox
    Friend WithEvents txtTen5 As TextBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents txtMaDG As TextBox
    Friend WithEvents txtTenDG As TextBox
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents txtTen4 As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtTen1 As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtMa3 As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtMa4 As TextBox
    Friend WithEvents dtpNgay As DateTimePicker
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label17 As Label
    Friend WithEvents txtMaTra As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents dtpTra As DateTimePicker
    Friend WithEvents cb1 As CheckBox
    Friend WithEvents cb2 As CheckBox
    Friend WithEvents cb3 As CheckBox
    Friend WithEvents cb4 As CheckBox
    Friend WithEvents cb5 As CheckBox
    Friend WithEvents Label19 As Label
    Friend WithEvents lblPhat As Label
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column8 As DataGridViewTextBoxColumn
    Friend WithEvents btnXemCT As Button
    Friend WithEvents btnInCT As Button
End Class
