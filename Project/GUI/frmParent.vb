﻿Imports BUS
Public Class frmParent

#Region "Me"
    Private DGBus As New DocGiaBUS()
    Private SBus As New SachBUS()
    Private AccBus As New AccountBUS()
    Private MSBus As New MuonSachBUS()
    Private TSBus As New TraSachBUS()
    Public Sub ReceiveName(ByVal name As String)
        lblUser.Text = "Xin Chào " + name
    End Sub
    Private Sub frmParent_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SetFrm()
    End Sub
    Private Sub sniHome_Click(sender As Object, e As EventArgs) 

    End Sub
    Private Sub sniThoat_Click(sender As Object, e As EventArgs) Handles sniThoat.Click
        Application.Exit()
    End Sub
    Private Sub SetFrm()
        frmMNBook.MdiParent = Me
        frmMNDocGia.MdiParent = Me
        frmMNGroup.MdiParent = Me
        frmMNMuon.MdiParent = Me
        frmMNPay.MdiParent = Me
        frmMNAccount.MdiParent = Me
    End Sub
    Public Sub ShowFrm(ByVal frmShow As Form)
        For Each frm As Form In Me.MdiChildren
            frm.Hide()
        Next
        frmShow.Show()
    End Sub
#End Region
#Region "DocGia"
    Private Sub sniDG_Click(sender As Object, e As EventArgs) Handles sniDG.Click
        frmMNDocGia.HienDS(0, 10)
        ShowFrm(frmMNDocGia)
    End Sub
    Private Sub Panel6_Click(sender As Object, e As EventArgs) Handles PictureBox6.Click, pnl41.Click, LinkLabel6.Click
        frmMNDocGia.HienDS(0, 10)
        ShowFrm(frmMNDocGia)
    End Sub
    Private Sub PictureBox8_Click(sender As Object, e As EventArgs) Handles pnl42.Click, PictureBox8.Click, LinkLabel8.Click
        frmMNDocGia.Them()
        frmMNDocGia.HienDS(0, 10)
    End Sub
    Private Sub pnl44_Click(sender As Object, e As EventArgs) Handles pnl44.Click, PictureBox5.Click, LinkLabel5.Click
        frmMNDocGia.Xoa()
        frmMNDocGia.HienDS(0, 10)
    End Sub
    Private Sub pnl43_Click(sender As Object, e As EventArgs) Handles pnl43.Click, PictureBox7.Click, LinkLabel7.Click
        frmMNDocGia.Sua()
        frmMNDocGia.HienDS(0, 10)
    End Sub
#End Region
#Region "Account"
    Private Sub Panel10_Click(sender As Object, e As EventArgs) Handles PictureBox10.Click, Panel10.Click, LinkLabel10.Click
        frmMNAccount.HienDS()
        ShowFrm(frmMNAccount)
    End Sub
    Private Sub Panel12_Click(sender As Object, e As EventArgs) Handles PictureBox12.Click, Panel12.Click, LinkLabel12.Click
        frmMNAccount.Them()
        ShowFrm(frmMNAccount)
    End Sub
    Private Sub Panel11_Click(sender As Object, e As EventArgs) Handles PictureBox11.Click, Panel11.Click, LinkLabel11.Click
        frmMNAccount.Sua()
        ShowFrm(frmMNAccount)
    End Sub
    Private Sub Panel9_Click(sender As Object, e As EventArgs) Handles PictureBox9.Click, Panel9.Click, LinkLabel9.Click
        frmMNAccount.Xoa()
        ShowFrm(frmMNAccount)
    End Sub
    Private Sub sniUser_Click(sender As Object, e As EventArgs) Handles sniUser.Click
        frmMNAccount.HienDS()
        ShowFrm(frmMNAccount)
    End Sub
#End Region
#Region "Group"
    Private Sub SideNavItem2_Click(sender As Object, e As EventArgs) Handles SideNavItem2.Click
        frmMNGroup.HienDS()
        ShowFrm(frmMNGroup)
    End Sub
    Private Sub Panel2_Click(sender As Object, e As EventArgs) Handles PictureBox2.Click, Panel2.Click, LinkLabel2.Click
        frmMNGroup.HienDS()
        ShowFrm(frmMNGroup)
    End Sub
    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click, Panel4.Click, LinkLabel4.Click
        frmMNGroup.Them()
        ShowFrm(frmMNGroup)
    End Sub
    Private Sub Panel3_Click(sender As Object, e As EventArgs) Handles PictureBox3.Click, Panel3.Click, LinkLabel3.Click
        frmMNGroup.Sua()
        ShowFrm(frmMNGroup)
    End Sub
    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click, Panel1.Click, LinkLabel1.Click
        frmMNGroup.Xoa()
        ShowFrm(frmMNGroup)
    End Sub
#End Region
#Region "Book"
    Private Sub SideNavItem8_Click(sender As Object, e As EventArgs) Handles SideNavItem8.Click
        frmMNBook.HienDS(0, 10)
        ShowFrm(frmMNBook)
    End Sub
    Private Sub pnl21_Click(sender As Object, e As EventArgs) Handles pnl21.Click, pbx11.Click, llbl21.Click
        frmMNBook.HienDS(0, 10)
        ShowFrm(frmMNBook)
    End Sub
    Private Sub pnl22_Click(sender As Object, e As EventArgs) Handles pnl22.Click, pb22.Click, llbl22.Click
        frmMNBook.HienDS(0, 10)
        frmMNBook.Them()
    End Sub
    Private Sub pnl23_Click(sender As Object, e As EventArgs) Handles pnl23.Click, pb23.Click, lbl23.Click
        frmMNBook.HienDS(0, 10)
        frmMNBook.Sua()
    End Sub
    Private Sub pnl24_Click(sender As Object, e As EventArgs) Handles pnl24.Click, pb24.Click, llbl24.Click
        frmMNBook.HienDS(0, 10)
        frmMNBook.Xoa()
    End Sub
#End Region
#Region "Muon"
    Private Sub PictureBox18_Click(sender As Object, e As EventArgs) Handles PictureBox18.Click, Panel18.Click, LinkLabel18.Click
        frmMNMuon.HienDS(0, 10)
        ShowFrm(frmMNMuon)
    End Sub
    Private Sub Panel19_Click(sender As Object, e As EventArgs) Handles PictureBox19.Click, Panel19.Click, LinkLabel19.Click
        frmMNMuon.Them()
        frmMNMuon.HienDS(0, 10)
    End Sub
    Private Sub PictureBox20_Click(sender As Object, e As EventArgs) Handles PictureBox20.Click, Panel20.Click, LinkLabel20.Click
        frmMNMuon.Sua()
        frmMNMuon.HienDS(0, 10)
    End Sub
    Private Sub Panel17_Click(sender As Object, e As EventArgs) Handles PictureBox17.Click, Panel17.Click, LinkLabel17.Click
        frmMNMuon.Xoa()
        frmMNMuon.HienDS(0, 10)
    End Sub
    Private Sub SideNavItem10_Click(sender As Object, e As EventArgs) Handles SideNavItem10.Click
        frmMNMuon.HienDS(0, 10)
        ShowFrm(frmMNMuon)
    End Sub
#End Region
#Region "Tra"
    Private Sub Panel14_Click(sender As Object, e As EventArgs) Handles PictureBox14.Click, Panel14.Click, LinkLabel14.Click
        frmMNPay.HienDS(0, 10)
        ShowFrm(frmMNPay)
    End Sub
    Private Sub Panel15_Click(sender As Object, e As EventArgs) Handles PictureBox15.Click, Panel15.Click, LinkLabel15.Click
        frmMNPay.Them()
        frmMNPay.HienDS(0, 10)
    End Sub
    Private Sub Panel16_Click(sender As Object, e As EventArgs) Handles PictureBox16.Click, Panel16.Click, LinkLabel16.Click
        frmMNPay.Sua()
        frmMNPay.HienDS(0, 10)
    End Sub
    Private Sub Panel13_Click(sender As Object, e As EventArgs) Handles PictureBox13.Click, Panel13.Click, LinkLabel13.Click
        frmMNPay.Xoa()
        frmMNPay.HienDS(0, 10)
    End Sub
    Private Sub SideNavItem4_Click(sender As Object, e As EventArgs) Handles SideNavItem4.Click
        frmTKSachIt.Show()
    End Sub
    Private Sub SideNavItem9_Click(sender As Object, e As EventArgs) Handles SideNavItem9.Click
        frmMNPay.HienDS(0, 10)
        ShowFrm(frmMNPay)
    End Sub
#End Region
End Class
