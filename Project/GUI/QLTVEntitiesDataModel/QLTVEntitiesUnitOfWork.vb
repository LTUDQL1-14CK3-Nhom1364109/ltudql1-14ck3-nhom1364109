﻿Imports DevExpress.Mvvm.DataModel
Imports DevExpress.Mvvm.DataModel.EF6
Imports GUI
Imports System
Imports System.Collections.Generic
Imports System.Linq
Namespace Global.GUI.QLTVEntitiesDataModel
  ''' <summary>
  ''' A QLTVEntitiesUnitOfWork instance that represents the run-time implementation of the IQLTVEntitiesUnitOfWork interface.
  ''' </summary>
  Public Class QLTVEntitiesUnitOfWork
    Inherits DbUnitOfWork(Of QLTVEntities)
    Implements IQLTVEntitiesUnitOfWork
    Public Sub New(ByVal contextFactory As Func(Of QLTVEntities))
      MyBase.New(contextFactory)
    End Sub
    Private ReadOnly Property vv_DG_HetHan As IReadOnlyRepository(Of vv_DG_HetHan) Implements IQLTVEntitiesUnitOfWork.vv_DG_HetHan
      Get
        Return GetReadOnlyRepository(Function(ByVal x) x.[Set](Of vv_DG_HetHan)())
      End Get
    End Property
    Private ReadOnly Property vv_Muon As IReadOnlyRepository(Of vv_Muon) Implements IQLTVEntitiesUnitOfWork.vv_Muon
      Get
        Return GetReadOnlyRepository(Function(ByVal x) x.[Set](Of vv_Muon)())
      End Get
    End Property
    Private ReadOnly Property vv_SachIt As IReadOnlyRepository(Of vv_SachIt) Implements IQLTVEntitiesUnitOfWork.vv_SachIt
      Get
        Return GetReadOnlyRepository(Function(ByVal x) x.[Set](Of vv_SachIt)())
      End Get
    End Property
    Private ReadOnly Property vv_XoaDG As IReadOnlyRepository(Of vv_XoaDG) Implements IQLTVEntitiesUnitOfWork.vv_XoaDG
      Get
        Return GetReadOnlyRepository(Function(ByVal x) x.[Set](Of vv_XoaDG)())
      End Get
    End Property
    Private ReadOnly Property vv_XoaLSach As IReadOnlyRepository(Of vv_XoaLSach) Implements IQLTVEntitiesUnitOfWork.vv_XoaLSach
      Get
        Return GetReadOnlyRepository(Function(ByVal x) x.[Set](Of vv_XoaLSach)())
      End Get
    End Property
    Private ReadOnly Property vv_XoaMuon As IReadOnlyRepository(Of vv_XoaMuon) Implements IQLTVEntitiesUnitOfWork.vv_XoaMuon
      Get
        Return GetReadOnlyRepository(Function(ByVal x) x.[Set](Of vv_XoaMuon)())
      End Get
    End Property
    Private ReadOnly Property vv_XoaSach As IReadOnlyRepository(Of vv_XoaSach) Implements IQLTVEntitiesUnitOfWork.vv_XoaSach
      Get
        Return GetReadOnlyRepository(Function(ByVal x) x.[Set](Of vv_XoaSach)())
      End Get
    End Property
    Private ReadOnly Property vv_XoaTra As IReadOnlyRepository(Of vv_XoaTra) Implements IQLTVEntitiesUnitOfWork.vv_XoaTra
      Get
        Return GetReadOnlyRepository(Function(ByVal x) x.[Set](Of vv_XoaTra)())
      End Get
    End Property
  End Class
End Namespace
