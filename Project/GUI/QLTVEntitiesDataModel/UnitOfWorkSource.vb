﻿Imports DevExpress.Mvvm
Imports DevExpress.Mvvm.DataModel
Imports DevExpress.Mvvm.DataModel.DesignTime
Imports DevExpress.Mvvm.DataModel.EF6
Imports GUI
Imports System
Imports System.Collections
Imports System.Linq
Namespace Global.GUI.QLTVEntitiesDataModel
  ''' <summary>
  ''' Provides methods to obtain the relevant IUnitOfWorkFactory.
  ''' </summary>
  Public Module UnitOfWorkSource  
    ''' <summary>
    ''' Returns the IUnitOfWorkFactory implementation.
    ''' </summary>
    Public Function GetUnitOfWorkFactory() As IUnitOfWorkFactory(Of IQLTVEntitiesUnitOfWork)
      Return New DbUnitOfWorkFactory(Of IQLTVEntitiesUnitOfWork)(Function() New QLTVEntitiesUnitOfWork(Function() New QLTVEntities()))
    End Function
  End Module
End Namespace
