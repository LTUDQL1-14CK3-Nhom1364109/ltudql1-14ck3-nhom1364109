﻿Imports DevExpress.Mvvm.DataModel
Imports GUI
Imports System
Imports System.Collections.Generic
Imports System.Linq
Namespace Global.GUI.QLTVEntitiesDataModel
  ''' <summary>
  ''' IQLTVEntitiesUnitOfWork extends the IUnitOfWork interface with repositories representing specific entities.
  ''' </summary>
  Public Interface IQLTVEntitiesUnitOfWork
    Inherits IUnitOfWork    
    ''' <summary>
    ''' The vv_DG_HetHan entities repository.
    ''' </summary>
    ReadOnly Property vv_DG_HetHan As IReadOnlyRepository(Of vv_DG_HetHan)    
    ''' <summary>
    ''' The vv_Muon entities repository.
    ''' </summary>
    ReadOnly Property vv_Muon As IReadOnlyRepository(Of vv_Muon)    
    ''' <summary>
    ''' The vv_SachIt entities repository.
    ''' </summary>
    ReadOnly Property vv_SachIt As IReadOnlyRepository(Of vv_SachIt)    
    ''' <summary>
    ''' The vv_XoaDG entities repository.
    ''' </summary>
    ReadOnly Property vv_XoaDG As IReadOnlyRepository(Of vv_XoaDG)    
    ''' <summary>
    ''' The vv_XoaLSach entities repository.
    ''' </summary>
    ReadOnly Property vv_XoaLSach As IReadOnlyRepository(Of vv_XoaLSach)    
    ''' <summary>
    ''' The vv_XoaMuon entities repository.
    ''' </summary>
    ReadOnly Property vv_XoaMuon As IReadOnlyRepository(Of vv_XoaMuon)    
    ''' <summary>
    ''' The vv_XoaSach entities repository.
    ''' </summary>
    ReadOnly Property vv_XoaSach As IReadOnlyRepository(Of vv_XoaSach)    
    ''' <summary>
    ''' The vv_XoaTra entities repository.
    ''' </summary>
    ReadOnly Property vv_XoaTra As IReadOnlyRepository(Of vv_XoaTra)
  End Interface
End Namespace
