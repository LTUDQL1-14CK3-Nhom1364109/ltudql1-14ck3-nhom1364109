﻿Imports BUS
Imports DTO

Public Class frmMNGroup
    Private GrBUS As New GroupBUS()
    Private Function SetDTO() As GroupDTO
        Dim GrDTO As New GroupDTO()
        If txtID.Text = "" Then
            GrDTO.ID = 0
        Else
            GrDTO.ID = Convert.ToInt32(txtID.Text)
        End If
        GrDTO.TenLoai = txtTen.Text
        GrDTO.MoTa = txtMoTa.Text
        Return GrDTO
    End Function
    Private Sub dgvTable_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTable.CellClick
        Dim row As Integer
        row = dgvTable.CurrentRow.Index
        txtID.Text = dgvTable.Rows(row).Cells(0).Value().ToString
        txtTen.Text = dgvTable.Rows(row).Cells(1).Value().ToString
        txtMoTa.Text = dgvTable.Rows(row).Cells(2).Value().ToString
    End Sub
    Private Sub frmMNGroup_Load(sender As Object, e As EventArgs) Handles MyBase.Load

    End Sub
#Region "Tool"
    Public Sub HienDS()
        dgvTable.DataSource = GrBUS.Table()
    End Sub
    Public Sub Them()
        If txtTen.Text <> "" Then
            If MessageBox.Show("Ban Co Chac Chan Muon Them Thong Tin", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                If GrBUS.Them(SetDTO()) > -1 Then
                    MessageBox.Show("Thành Công")
                    HienDS()
                Else
                    MessageBox.Show("Vui long nhap Day Du Thong Tin")
                End If
            End If
        End If
    End Sub
    Public Sub Sua()
        If Me.Validate = False Then
            Me.Show()
        ElseIf txtTen.Text <> "" Then
            If MessageBox.Show("Ban Co Chac Chan Muon Sua Thong Tin", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                If GrBUS.Sua(SetDTO()) > -1 Then
                    MessageBox.Show("Thành Công")
                    HienDS()
                Else
                    MessageBox.Show("Vui long nhap Day Du Thong Tin")
                End If
            End If
        End If
    End Sub
    Public Sub Xoa()
        If txtTen.Text <> "" Then
            If MessageBox.Show("Ban Co Chac Chan Muon Xoa Thong Tin ", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                If GrBUS.Xoa(txtID.Text) > -1 Then
                    MessageBox.Show("Xoa Thanh Cong")
                    HienDS()
                Else
                    MessageBox.Show("Vui long nhap Ma Doc Gia")
                End If
            End If
        End If
    End Sub
    Private Sub btnTim_Click(sender As Object, e As EventArgs) Handles btnTim.Click
        Dim tab As DataTable
        tab = GrBUS.TableTim(cbxTim.SelectedIndex, txtKey.Text)
        If tab.Rows.Count() = 0 Then
            MessageBox.Show("Khong tim thay du lieu")
        Else
            dgvTable.DataSource = tab
        End If
    End Sub


#End Region
End Class