﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMNBook
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.lblTrang = New System.Windows.Forms.Label()
        Me.btnBor = New System.Windows.Forms.Button()
        Me.btnFirst = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnPre = New System.Windows.Forms.Button()
        Me.btnPay = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtTomTat = New System.Windows.Forms.TextBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtID = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtNSX = New System.Windows.Forms.TextBox()
        Me.txtTen = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.cbxLoaiSach = New System.Windows.Forms.ComboBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.txtTongSL = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtSoSachCon = New System.Windows.Forms.TextBox()
        Me.txtNam = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.dtpNgNhap = New System.Windows.Forms.DateTimePicker()
        Me.txtGia = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.cbxTim = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnTim = New System.Windows.Forms.Button()
        Me.txtKey = New System.Windows.Forms.TextBox()
        Me.dgvTable = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column10 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column13 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnXemCT = New System.Windows.Forms.Button()
        Me.GroupBox3.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.dgvTable, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblTrang
        '
        Me.lblTrang.AutoSize = True
        Me.lblTrang.Location = New System.Drawing.Point(313, 411)
        Me.lblTrang.Name = "lblTrang"
        Me.lblTrang.Size = New System.Drawing.Size(0, 16)
        Me.lblTrang.TabIndex = 17
        '
        'btnBor
        '
        Me.btnBor.Location = New System.Drawing.Point(529, 404)
        Me.btnBor.Name = "btnBor"
        Me.btnBor.Size = New System.Drawing.Size(91, 23)
        Me.btnBor.TabIndex = 15
        Me.btnBor.Text = "Mượn Sách"
        Me.btnBor.UseVisualStyleBackColor = True
        '
        'btnFirst
        '
        Me.btnFirst.Location = New System.Drawing.Point(6, 15)
        Me.btnFirst.Name = "btnFirst"
        Me.btnFirst.Size = New System.Drawing.Size(93, 29)
        Me.btnFirst.TabIndex = 2
        Me.btnFirst.Text = "Trang Đầu"
        Me.btnFirst.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(105, 15)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 29)
        Me.btnNext.TabIndex = 1
        Me.btnNext.Text = "Trang Kế"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnPre
        '
        Me.btnPre.Location = New System.Drawing.Point(186, 15)
        Me.btnPre.Name = "btnPre"
        Me.btnPre.Size = New System.Drawing.Size(95, 29)
        Me.btnPre.TabIndex = 0
        Me.btnPre.Text = "Trang Trước"
        Me.btnPre.UseVisualStyleBackColor = True
        '
        'btnPay
        '
        Me.btnPay.Location = New System.Drawing.Point(710, 404)
        Me.btnPay.Name = "btnPay"
        Me.btnPay.Size = New System.Drawing.Size(86, 23)
        Me.btnPay.TabIndex = 16
        Me.btnPay.Text = "Trả Sách"
        Me.btnPay.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnFirst)
        Me.GroupBox3.Controls.Add(Me.btnNext)
        Me.GroupBox3.Controls.Add(Me.btnPre)
        Me.GroupBox3.Location = New System.Drawing.Point(20, 389)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(287, 56)
        Me.GroupBox3.TabIndex = 13
        Me.GroupBox3.TabStop = False
        '
        'txtTomTat
        '
        Me.txtTomTat.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTomTat.Location = New System.Drawing.Point(108, 200)
        Me.txtTomTat.Name = "txtTomTat"
        Me.txtTomTat.Size = New System.Drawing.Size(245, 23)
        Me.txtTomTat.TabIndex = 12
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 7
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 9.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 41.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.txtTomTat, 2, 8)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtID, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtNSX, 2, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.txtTen, 2, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label5, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.Label11, 0, 8)
        Me.TableLayoutPanel1.Controls.Add(Me.Label9, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.cbxLoaiSach, 6, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label6, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtTongSL, 6, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label12, 4, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label8, 4, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.txtSoSachCon, 6, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.txtNam, 2, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.Label10, 4, 8)
        Me.TableLayoutPanel1.Controls.Add(Me.dtpNgNhap, 6, 8)
        Me.TableLayoutPanel1.Controls.Add(Me.txtGia, 6, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.Label7, 4, 6)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(6, 38)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 9
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(698, 224)
        Me.TableLayoutPanel1.TabIndex = 14
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(36, 7)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(57, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Mã Sách"
        '
        'txtID
        '
        Me.txtID.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtID.Location = New System.Drawing.Point(108, 3)
        Me.txtID.Name = "txtID"
        Me.txtID.Size = New System.Drawing.Size(245, 23)
        Me.txtID.TabIndex = 9
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(31, 56)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(62, 16)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Tên Sách"
        '
        'txtNSX
        '
        Me.txtNSX.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtNSX.Location = New System.Drawing.Point(108, 102)
        Me.txtNSX.Name = "txtNSX"
        Me.txtNSX.Size = New System.Drawing.Size(245, 23)
        Me.txtNSX.TabIndex = 11
        '
        'txtTen
        '
        Me.txtTen.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTen.Location = New System.Drawing.Point(108, 53)
        Me.txtTen.Name = "txtTen"
        Me.txtTen.Size = New System.Drawing.Size(245, 23)
        Me.txtTen.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 105)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Nhà Sản Xuất"
        '
        'Label11
        '
        Me.Label11.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(36, 203)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(57, 16)
        Me.Label11.TabIndex = 13
        Me.Label11.Text = "Tóm Tắt"
        '
        'Label9
        '
        Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(3, 154)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(90, 16)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Năm Sản Xuất"
        '
        'cbxLoaiSach
        '
        Me.cbxLoaiSach.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cbxLoaiSach.FormattingEnabled = True
        Me.cbxLoaiSach.Location = New System.Drawing.Point(504, 3)
        Me.cbxLoaiSach.Name = "cbxLoaiSach"
        Me.cbxLoaiSach.Size = New System.Drawing.Size(191, 24)
        Me.cbxLoaiSach.TabIndex = 12
        '
        'Label6
        '
        Me.Label6.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(425, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(63, 16)
        Me.Label6.TabIndex = 3
        Me.Label6.Text = "Loại Sách"
        '
        'txtTongSL
        '
        Me.txtTongSL.Location = New System.Drawing.Point(504, 53)
        Me.txtTongSL.Name = "txtTongSL"
        Me.txtTongSL.Size = New System.Drawing.Size(188, 23)
        Me.txtTongSL.TabIndex = 16
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(400, 50)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(88, 16)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = "Tổng Số Sách"
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(407, 99)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(81, 16)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "Số Sách Còn"
        '
        'txtSoSachCon
        '
        Me.txtSoSachCon.Location = New System.Drawing.Point(504, 102)
        Me.txtSoSachCon.Name = "txtSoSachCon"
        Me.txtSoSachCon.Size = New System.Drawing.Size(100, 23)
        Me.txtSoSachCon.TabIndex = 14
        '
        'txtNam
        '
        Me.txtNam.Location = New System.Drawing.Point(108, 151)
        Me.txtNam.Name = "txtNam"
        Me.txtNam.Size = New System.Drawing.Size(100, 23)
        Me.txtNam.TabIndex = 17
        '
        'Label10
        '
        Me.Label10.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(419, 203)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(69, 16)
        Me.Label10.TabIndex = 7
        Me.Label10.Text = "Ngày Nhập"
        '
        'dtpNgNhap
        '
        Me.dtpNgNhap.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dtpNgNhap.Location = New System.Drawing.Point(504, 200)
        Me.dtpNgNhap.Name = "dtpNgNhap"
        Me.dtpNgNhap.Size = New System.Drawing.Size(191, 23)
        Me.dtpNgNhap.TabIndex = 8
        '
        'txtGia
        '
        Me.txtGia.Location = New System.Drawing.Point(504, 151)
        Me.txtGia.Name = "txtGia"
        Me.txtGia.Size = New System.Drawing.Size(100, 23)
        Me.txtGia.TabIndex = 18
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(400, 148)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(58, 16)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Giá Sách"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupBox2.Location = New System.Drawing.Point(333, 67)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(736, 326)
        Me.GroupBox2.TabIndex = 12
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Thông Tin"
        '
        'cbxTim
        '
        Me.cbxTim.FormattingEnabled = True
        Me.cbxTim.Items.AddRange(New Object() {"Mã Sách", "Tên Sách", "Nhà Sản Xuất", "Loại Sách", "Tóm Tắt"})
        Me.cbxTim.Location = New System.Drawing.Point(73, 87)
        Me.cbxTim.Name = "cbxTim"
        Me.cbxTim.Size = New System.Drawing.Size(121, 24)
        Me.cbxTim.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(4, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Từ Khoá"
        '
        'btnTim
        '
        Me.btnTim.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnTim.Location = New System.Drawing.Point(200, 88)
        Me.btnTim.Name = "btnTim"
        Me.btnTim.Size = New System.Drawing.Size(89, 23)
        Me.btnTim.TabIndex = 1
        Me.btnTim.Text = "Tìm Kiếm"
        Me.btnTim.UseVisualStyleBackColor = True
        '
        'txtKey
        '
        Me.txtKey.Location = New System.Drawing.Point(66, 35)
        Me.txtKey.Name = "txtKey"
        Me.txtKey.Size = New System.Drawing.Size(223, 23)
        Me.txtKey.TabIndex = 0
        '
        'dgvTable
        '
        Me.dgvTable.AllowUserToOrderColumns = True
        Me.dgvTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvTable.BackgroundColor = System.Drawing.SystemColors.ActiveCaption
        Me.dgvTable.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvTable.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SunkenVertical
        Me.dgvTable.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken
        Me.dgvTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTable.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6, Me.Column7, Me.Column8, Me.Column9, Me.Column10, Me.Column13})
        Me.dgvTable.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.dgvTable.GridColor = System.Drawing.SystemColors.ActiveCaption
        Me.dgvTable.Location = New System.Drawing.Point(0, 451)
        Me.dgvTable.Name = "dgvTable"
        Me.dgvTable.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvTable.Size = New System.Drawing.Size(1100, 267)
        Me.dgvTable.StandardTab = True
        Me.dgvTable.TabIndex = 10
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "ID"
        Me.Column1.HeaderText = "Mã"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "TenSach"
        Me.Column2.HeaderText = "Tên Sách"
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "NhaSX"
        Me.Column3.HeaderText = "Nhà Sản Xuất"
        Me.Column3.Name = "Column3"
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "NamSX"
        Me.Column4.HeaderText = "Năm Sản Xuất"
        Me.Column4.Name = "Column4"
        '
        'Column5
        '
        Me.Column5.DataPropertyName = "TomTat"
        Me.Column5.HeaderText = "Tóm Tắt"
        Me.Column5.Name = "Column5"
        '
        'Column6
        '
        Me.Column6.DataPropertyName = "TongSach"
        Me.Column6.HeaderText = "Tổng Nhập"
        Me.Column6.Name = "Column6"
        '
        'Column7
        '
        Me.Column7.DataPropertyName = "SoSachCon"
        Me.Column7.HeaderText = "Số Lượng Còn"
        Me.Column7.Name = "Column7"
        '
        'Column8
        '
        Me.Column8.DataPropertyName = "NgayNhap"
        Me.Column8.HeaderText = "Ngày Nhập"
        Me.Column8.Name = "Column8"
        '
        'Column9
        '
        Me.Column9.DataPropertyName = "GiaSach"
        Me.Column9.HeaderText = "Giá Sách"
        Me.Column9.Name = "Column9"
        '
        'Column10
        '
        Me.Column10.DataPropertyName = "TenLoaiSach"
        Me.Column10.HeaderText = "Loại Sách"
        Me.Column10.Name = "Column10"
        '
        'Column13
        '
        Me.Column13.DataPropertyName = "ID_LoaiSach"
        Me.Column13.HeaderText = "ID_Loai"
        Me.Column13.Name = "Column13"
        Me.Column13.Visible = False
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 20.0!)
        Me.lblTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblTitle.Location = New System.Drawing.Point(444, 20)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(176, 33)
        Me.lblTitle.TabIndex = 9
        Me.lblTitle.Text = "Quản Lý Sách"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(4, 91)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 16)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Tìm Theo"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cbxTim)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.btnTim)
        Me.GroupBox1.Controls.Add(Me.txtKey)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 67)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(295, 135)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        '
        'btnXemCT
        '
        Me.btnXemCT.Location = New System.Drawing.Point(845, 404)
        Me.btnXemCT.Name = "btnXemCT"
        Me.btnXemCT.Size = New System.Drawing.Size(98, 23)
        Me.btnXemCT.TabIndex = 50
        Me.btnXemCT.Text = "Xem Chi Tiết"
        Me.btnXemCT.UseVisualStyleBackColor = True
        '
        'frmMNBook
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(1100, 718)
        Me.Controls.Add(Me.btnXemCT)
        Me.Controls.Add(Me.lblTrang)
        Me.Controls.Add(Me.btnBor)
        Me.Controls.Add(Me.btnPay)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.dgvTable)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.GroupBox1)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "frmMNBook"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmMNBook"
        Me.GroupBox3.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.dgvTable, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lblTrang As Label
    Friend WithEvents btnBor As Button
    Friend WithEvents btnFirst As Button
    Friend WithEvents btnNext As Button
    Friend WithEvents btnPre As Button
    Friend WithEvents btnPay As Button
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents txtTomTat As TextBox
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents Label3 As Label
    Friend WithEvents txtID As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtNSX As TextBox
    Friend WithEvents txtTen As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents cbxLoaiSach As ComboBox
    Friend WithEvents Label6 As Label
    Friend WithEvents txtTongSL As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents txtSoSachCon As TextBox
    Friend WithEvents txtNam As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents dtpNgNhap As DateTimePicker
    Friend WithEvents txtGia As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents cbxTim As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents btnTim As Button
    Friend WithEvents txtKey As TextBox
    Friend WithEvents dgvTable As DataGridView
    Friend WithEvents lblTitle As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents Column5 As DataGridViewTextBoxColumn
    Friend WithEvents Column6 As DataGridViewTextBoxColumn
    Friend WithEvents Column7 As DataGridViewTextBoxColumn
    Friend WithEvents Column8 As DataGridViewTextBoxColumn
    Friend WithEvents Column9 As DataGridViewTextBoxColumn
    Friend WithEvents Column10 As DataGridViewTextBoxColumn
    Friend WithEvents Column13 As DataGridViewTextBoxColumn
    Friend WithEvents btnXemCT As Button
End Class
