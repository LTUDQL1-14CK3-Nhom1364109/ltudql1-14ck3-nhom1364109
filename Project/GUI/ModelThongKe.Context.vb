﻿'------------------------------------------------------------------------------
' <auto-generated>
'     This code was generated from a template.
'
'     Manual changes to this file may cause unexpected behavior in your application.
'     Manual changes to this file will be overwritten if the code is regenerated.
' </auto-generated>
'------------------------------------------------------------------------------

Imports System
Imports System.Data.Entity
Imports System.Data.Entity.Infrastructure

Partial Public Class QLTVEntities
    Inherits DbContext

    Public Sub New()
        MyBase.New("name=QLTVEntities")
    End Sub

    Protected Overrides Sub OnModelCreating(modelBuilder As DbModelBuilder)
        Throw New UnintentionalCodeFirstException()
    End Sub

    Public Overridable Property vv_DG_HetHan() As DbSet(Of vv_DG_HetHan)
    Public Overridable Property vv_Muon() As DbSet(Of vv_Muon)
    Public Overridable Property vv_SachIt() As DbSet(Of vv_SachIt)
    Public Overridable Property vv_XoaDG() As DbSet(Of vv_XoaDG)
    Public Overridable Property vv_XoaLSach() As DbSet(Of vv_XoaLSach)
    Public Overridable Property vv_XoaMuon() As DbSet(Of vv_XoaMuon)
    Public Overridable Property vv_XoaSach() As DbSet(Of vv_XoaSach)
    Public Overridable Property vv_XoaTra() As DbSet(Of vv_XoaTra)

End Class
