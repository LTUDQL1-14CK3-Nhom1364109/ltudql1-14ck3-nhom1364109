﻿Imports DTO
Imports BUS

Public Class frmLogin
    Private acBus As New AccountBUS()

    Private Sub OK_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles OK.Click
        Dim user As New AccountDTO()
        user.User = txtUser.Text
        user.Pass = txtPass.Text
        If (acBus.IsLogin(user)) Then
            MessageBox.Show("Xin chào " + user.Name)
            frmParent.ReceiveName(user.Name)
            frmParent.Show()
            Me.Hide()
        Else
            MessageBox.Show("Vui Lòng Kiểm Tra Lại Thông Tin Đăng Nhập")
        End If




    End Sub

    Private Sub Cancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancel.Click
        Me.Close()
    End Sub

    Private Sub frmLogin_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ForeColor = frmParent.ForeColor
        Me.FormBorderStyle = frmParent.FormBorderStyle
        Me.Font = frmParent.Font
    End Sub
End Class
