﻿Imports System
Imports System.Linq
Imports System.Collections.Generic
Imports DevExpress.XtraEditors
Imports DevExpress.XtraBars
Imports DevExpress.Utils.MVVM.Services

Namespace Global.GUI.Views.QLTVEntitiesView
	Partial Public Class QLTVEntitiesView
		Inherits XtraUserControl

		Public Sub New()
			InitializeComponent()
			If Not mvvmContext.IsDesignMode Then
				InitializeNavigation()
			End If
			AddHandler ribbonControl.Merge, AddressOf ribbonControl_Merge
			AddHandler ribbonControl.UnMerge, AddressOf ribbonControl_UnMerge
		End Sub
		Private Sub ribbonControl_UnMerge(ByVal sender As Object, ByVal e As DevExpress.XtraBars.Ribbon.RibbonMergeEventArgs)
			ribbonControl.SelectedPage = e.MergeOwner.SelectedPage
			ribbonControl.StatusBar.UnMergeStatusBar()
		End Sub

		Private Sub ribbonControl_Merge(ByVal sender As Object, ByVal e As DevExpress.XtraBars.Ribbon.RibbonMergeEventArgs)
			ribbonControl.SelectedPage = e.MergedChild.SelectedPage
			ribbonControl.StatusBar.MergeStatusBar(e.MergedChild.StatusBar)
		End Sub
		Private Sub InitializeNavigation()
			' We want the DocmentManager's TabbedView to be a navigation provider
			mvvmContext.RegisterService(DocumentManagerService.Create(tabbedView))
			' We want to use buttons in Ribbon to show the specific modules
			Dim fluentAPI = mvvmContext.OfType(Of Global.GUI.ViewModels.QLTVEntitiesViewModel)()
									 fluentAPI.BindCommand(accordionItemvv_DG_HetHanCollectionView, Sub(x, m) x.Show(m), Function(x) x.Modules(0))
						 fluentAPI.BindCommand(accordionItemvv_MuonCollectionView, Sub(x, m) x.Show(m), Function(x) x.Modules(1))
						 fluentAPI.BindCommand(accordionItemvv_SachItCollectionView, Sub(x, m) x.Show(m), Function(x) x.Modules(2))
						' We want show the default module when our UserControl is loaded
			fluentAPI.WithEvent (Of EventArgs)(Me, "Load").EventToCommand(Sub(x) x.OnLoaded(Nothing), Function(x) x.DefaultModule)
		End Sub
	End Class
End Namespace

