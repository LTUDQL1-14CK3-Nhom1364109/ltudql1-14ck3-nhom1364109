﻿Namespace Global.GUI.Views.vv_MuonCollectionView
	Partial Public Class vv_MuonCollectionView
		''' <summary> 
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary> 
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub
		#Region "Component Designer generated code"
		
		''' <summary> 
		''' Required method for Designer support - do not modify 
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Me.components = New System.ComponentModel.Container()
			Me.gridControl = New DevExpress.XtraGrid.GridControl()
			Me.gridView = New DevExpress.XtraGrid.Views.Grid.GridView()
			Me.mvvmContext = New DevExpress.Utils.MVVM.MVVMContext(Me.components)
			Me.ribbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
			Me.ribbonPage1 = New DevExpress.XtraBars.Ribbon.RibbonPage()
			Me.ribbonPageGroup1 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
			Me.ribbonPageGroup2 = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
			Me.ribbonStatusBar = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
			Me.bsiRecordsCount = New DevExpress.XtraBars.BarStaticItem()
			Me.bbiPrintPreview = New DevExpress.XtraBars.BarButtonItem()
			Me.popupMenu = New DevExpress.XtraBars.PopupMenu(Me.components)
			CType(Me.gridControl, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.gridView, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.ribbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.mvvmContext, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.popupMenu, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.SuspendLayout()
			' 
			' ribbonControl
			' 
			Me.ribbonControl.ExpandCollapseItem.Id = 0
			Me.ribbonControl.MaxItemId = 14
			Me.ribbonControl.Name = "ribbonControl"
			Me.ribbonControl.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() { Me.ribbonPage1})
			Me.ribbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013
			Me.ribbonControl.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False
			Me.ribbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden
			Me.ribbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() { Me.bbiPrintPreview, Me.bsiRecordsCount })
																Dim bbiRefresh As New DevExpress.XtraBars.BarButtonItem()
			bbiRefresh.Caption = "Refresh"
			bbiRefresh.Name = "bbiRefresh"
			bbiRefresh.ImageUri.Uri = "Refresh"
						Me.ribbonControl.Items.Add(bbiRefresh)
	
			' 
			' ribbonPage1
			' 
			Me.ribbonPage1.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() { Me.ribbonPageGroup1, Me.ribbonPageGroup2})
			Me.ribbonPage1.MergeOrder = 0
			Me.ribbonPage1.Name = "ribbonPage1"
			Me.ribbonPage1.Text = "Home"
			' 
			' ribbonPageGroup1
			' 
			Me.ribbonPageGroup1.AllowTextClipping = False
								Me.ribbonPageGroup1.ItemLinks.Add(bbiRefresh)
	
			Me.ribbonPageGroup1.Name = "ribbonPageGroup1"
			Me.ribbonPageGroup1.ShowCaptionButton = False
			Me.ribbonPageGroup1.Text = "vv_Muon Tasks"
			' 
			' ribbonPageGroup2
			' 
			Me.ribbonPageGroup2.ItemLinks.Add(Me.bbiPrintPreview)
			Me.ribbonPageGroup2.Name = "ribbonPageGroup2"
			Me.ribbonPageGroup2.Text = "Print and Export"
			Me.ribbonPageGroup2.AllowTextClipping = False
			Me.ribbonPageGroup2.ShowCaptionButton = False
			' 
			' ribbonStatusBar
			' 
			Me.ribbonStatusBar.ItemLinks.Add(Me.bsiRecordsCount)
			Me.ribbonStatusBar.Name = "ribbonStatusBar"
			Me.ribbonStatusBar.Ribbon = Me.ribbonControl
			' 
			' bbiPrintPreview
			' 
			Me.bbiPrintPreview.Caption = "Print Preview"
			Me.bbiPrintPreview.ImageUri.Uri = "Preview"
			Me.bbiPrintPreview.Name = "bbiPrintPreview"
			' 
			' barStaticItem1
			' 
			Me.bsiRecordsCount.Caption = "RECORDS : 2"
			Me.bsiRecordsCount.Name = "bsiRecordsCount"
			Me.bsiRecordsCount.TextAlignment = System.Drawing.StringAlignment.Near
			' 
			' gridControl
			' 
			Me.gridControl.Dock = System.Windows.Forms.DockStyle.Fill
			Me.gridControl.Location = New System.Drawing.Point(5, 116)
			Me.gridControl.MainView = Me.gridView
			Me.gridControl.MenuManager = Me.ribbonControl
			Me.gridControl.Name = "gridControl"
			Me.gridControl.Size = New System.Drawing.Size(779, 311)
			Me.gridControl.TabIndex = 2
			Me.gridControl.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() { Me.gridView})
			' 
			' gridView
			' 
			Me.gridView.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
			Me.gridView.GridControl = Me.gridControl
			Me.gridView.Name = "gridView"
			Me.gridView.OptionsBehavior.Editable = False
			Me.gridView.OptionsBehavior.ReadOnly = True
			Me.vv_MuonCollectionViewBindingSource = New System.Windows.Forms.BindingSource(Me.components)
			Me.vv_MuonCollectionViewBindingSource.DataSource = GetType(Global.GUI.vv_Muon)
			Me.gridControl.DataSource = vv_MuonCollectionViewBindingSource

			Dim parameters As New DevExpress.XtraGrid.Extensions.PopulateColumnsParameters()
			 
			 
			Me.gridView.PopulateColumns(GetType(Global.GUI.vv_Muon), parameters)
			' 
            ' popupMenu1
            ' 
								Me.popupMenu.ItemLinks.Add(bbiRefresh)
	
			Me.popupMenu.Name = "popupMenu"
			Me.popupMenu.Ribbon = Me.ribbonControl
			' 
			' mvvmContext
			' 
			Me.mvvmContext.ContainerControl = Me
            Me.mvvmContext.ViewModelType = GetType(Global.GUI.ViewModels.vv_MuonCollectionViewModel)
								Me.mvvmContext.BindingExpressions.Add(DevExpress.Utils.MVVM.BindingExpression.CreateCommandBinding(GetType(Global.GUI.ViewModels.vv_MuonCollectionViewModel), "Refresh", bbiRefresh))
	
            Me.mvvmContext.RegistrationExpressions.AddRange(New DevExpress.Utils.MVVM.RegistrationExpression() {
            DevExpress.Utils.MVVM.RegistrationExpression.RegisterLayoutSerializationService(Nothing, False, DevExpress.Utils.DefaultBoolean.Default, Me.gridControl)})
			'
            'vv_MuonCollectionView
            '
			Me.AutoScaleDimensions = New System.Drawing.SizeF(6F, 13F)
			Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
			Me.Controls.Add(Me.ribbonStatusBar)
			Me.Controls.Add(Me.gridControl)
			Me.Controls.Add(Me.ribbonControl)
			Me.Size = New System.Drawing.Size(1024, 768)
			Me.Name = "vv_MuonCollectionView"
			CType(Me.gridControl, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.gridView, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.ribbonControl, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.mvvmContext, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.popupMenu, System.ComponentModel.ISupportInitialize).EndInit()
			Me.ResumeLayout(False)
			Me.PerformLayout()
		End Sub

		#End Region

		Private gridControl As DevExpress.XtraGrid.GridControl
		Private gridView As DevExpress.XtraGrid.Views.Grid.GridView
		Private mvvmContext As DevExpress.Utils.MVVM.MVVMContext
		Private ribbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
		Private ribbonPage1 As DevExpress.XtraBars.Ribbon.RibbonPage
		Private ribbonPageGroup1 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
		Private vv_MuonCollectionViewBindingSource As System.Windows.Forms.BindingSource
		Private bbiPrintPreview As DevExpress.XtraBars.BarButtonItem
		Private ribbonPageGroup2 As DevExpress.XtraBars.Ribbon.RibbonPageGroup
		Private ribbonStatusBar As DevExpress.XtraBars.Ribbon.RibbonStatusBar
		Private bsiRecordsCount As DevExpress.XtraBars.BarStaticItem
		Private popupMenu As DevExpress.XtraBars.PopupMenu
	End Class
End Namespace
