﻿Namespace Global.GUI.Views.QLTVEntitiesView
	Partial Public Class QLTVEntitiesView
		''' <summary> 
		''' Required designer variable.
		''' </summary>
		Private components As System.ComponentModel.IContainer = Nothing

		''' <summary> 
		''' Clean up any resources being used.
		''' </summary>
		''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		Protected Overrides Sub Dispose(ByVal disposing As Boolean)
			If disposing AndAlso (components IsNot Nothing) Then
				components.Dispose()
			End If
			MyBase.Dispose(disposing)
		End Sub
		#Region "Component Designer generated code"

		''' <summary> 
		''' Required method for Designer support - do not modify 
		''' the contents of this method with the code editor.
		''' </summary>
		Private Sub InitializeComponent()
			Me.components = New System.ComponentModel.Container()
			Me.documentManager = New DevExpress.XtraBars.Docking2010.DocumentManager()
            Me.tabbedView = New DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView()
			Me.mvvmContext = New DevExpress.Utils.MVVM.MVVMContext(Me.components)
			Me.ribbonControl = New DevExpress.XtraBars.Ribbon.RibbonControl()
			Me.ribbonPage = New DevExpress.XtraBars.Ribbon.RibbonPage()
            Me.ribbonPageGroup = New DevExpress.XtraBars.Ribbon.RibbonPageGroup()
			Me.ribbonStatusBar = New DevExpress.XtraBars.Ribbon.RibbonStatusBar()
			Me.skinRibbonGalleryBarItem = New DevExpress.XtraBars.SkinRibbonGalleryBarItem()
			Me.dockManager = New DevExpress.XtraBars.Docking.DockManager(Me.components)
            Me.dockPanel = New DevExpress.XtraBars.Docking.DockPanel()
            Me.dockPanel_Container = New DevExpress.XtraBars.Docking.ControlContainer()
            Me.accordionControl = New DevExpress.XtraBars.Navigation.AccordionControl()
			Me.accordionItemTables = New DevExpress.XtraBars.Navigation.AccordionControlElement()
			Me.accordionItemViews = New DevExpress.XtraBars.Navigation.AccordionControlElement()
									Me.accordionItemvv_DG_HetHanCollectionView = New DevExpress.XtraBars.Navigation.AccordionControlElement() 
						Me.accordionItemvv_MuonCollectionView = New DevExpress.XtraBars.Navigation.AccordionControlElement() 
						Me.accordionItemvv_SachItCollectionView = New DevExpress.XtraBars.Navigation.AccordionControlElement() 
			 
			CType(Me.documentManager, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.tabbedView, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.mvvmContext, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.ribbonControl, System.ComponentModel.ISupportInitialize).BeginInit()
			CType(Me.dockManager, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.dockPanel.SuspendLayout()
            Me.dockPanel_Container.SuspendLayout()
			CType(Me.accordionControl, System.ComponentModel.ISupportInitialize).BeginInit()
			Me.SuspendLayout()
			' 
            ' ribbonControl
            ' 
            Me.ribbonControl.ExpandCollapseItem.Id = 0
            Me.ribbonControl.MaxItemId = 14
            Me.ribbonControl.Name = "ribbonControl"
			Me.ribbonControl.Items.AddRange(New DevExpress.XtraBars.BarItem() {Me.skinRibbonGalleryBarItem})
			Me.ribbonControl.Pages.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPage() {
            Me.ribbonPage})
            Me.ribbonControl.RibbonStyle = DevExpress.XtraBars.Ribbon.RibbonControlStyle.Office2013
            Me.ribbonControl.ShowApplicationButton = DevExpress.Utils.DefaultBoolean.False
			Me.ribbonControl.MdiMergeStyle = DevExpress.XtraBars.Ribbon.RibbonMdiMergeStyle.Always
            Me.ribbonControl.ToolbarLocation = DevExpress.XtraBars.Ribbon.RibbonQuickAccessToolbarLocation.Hidden
			Me.ribbonControl.StatusBar = Me.ribbonStatusBar
            ' 
            ' ribbonPage
            ' 
            Me.ribbonPage.Groups.AddRange(New DevExpress.XtraBars.Ribbon.RibbonPageGroup() {
            Me.ribbonPageGroup})
			Me.ribbonPage.MergeOrder = -1
            Me.ribbonPage.Name = "ribbonPage"
            Me.ribbonPage.Text = "View"
            ' 
            ' ribbonPageGroup
            ' 
            Me.ribbonPageGroup.AllowTextClipping = false
			Me.ribbonPageGroup.ItemLinks.Add(Me.skinRibbonGalleryBarItem)
            Me.ribbonPageGroup.Name = "ribbonPageGroup"
            Me.ribbonPageGroup.ShowCaptionButton = false
            Me.ribbonPageGroup.Text = "Appearance"
			' 
            ' ribbonStatusBar
            ' 
			Me.ribbonStatusBar.Name = "ribbonStatusBar"
            Me.ribbonStatusBar.Ribbon = Me.ribbonControl
			' 
            ' documentManager
            ' 
            Me.documentManager.ContainerControl = Me
            Me.documentManager.RibbonAndBarsMergeStyle = DevExpress.XtraBars.Docking2010.Views.RibbonAndBarsMergeStyle.Always
            Me.documentManager.View = Me.tabbedView
            Me.documentManager.ViewCollection.AddRange(New DevExpress.XtraBars.Docking2010.Views.BaseView() {
            Me.tabbedView})
			' 
            ' dockManager
            ' 
            Me.dockManager.Form = Me
            Me.dockManager.RootPanels.AddRange(New DevExpress.XtraBars.Docking.DockPanel() {
            Me.dockPanel})
			Me.dockManager.TopZIndexControls.AddRange(New String() {
            "DevExpress.XtraBars.BarDockControl",
            "DevExpress.XtraBars.StandaloneBarDockControl",
            "System.Windows.Forms.StatusBar",
            "System.Windows.Forms.MenuStrip",
            "System.Windows.Forms.StatusStrip",
            "DevExpress.XtraBars.Ribbon.RibbonStatusBar",
            "DevExpress.XtraBars.Ribbon.RibbonControl",
            "DevExpress.XtraBars.Navigation.OfficeNavigationBar",
            "DevExpress.XtraBars.Navigation.TileNavPane"})
			' 
            ' dockPanel
            ' 
            Me.dockPanel.Controls.Add(Me.dockPanel_Container)
            Me.dockPanel.Dock = DevExpress.XtraBars.Docking.DockingStyle.Left
            Me.dockPanel.Name = "dockPanel"
            Me.dockPanel.OriginalSize = New System.Drawing.Size(200, 200)
            Me.dockPanel.Text = "Navigation"
			' 
            ' dockPanel_Container
            ' 
            Me.dockPanel_Container.Controls.Add(Me.accordionControl)
            Me.dockPanel_Container.Name = "dockPanel_Container"
			' 
            ' accordionControl
            ' 
            Me.accordionControl.Dock = System.Windows.Forms.DockStyle.Fill
            Me.accordionControl.Elements.AddRange(New DevExpress.XtraBars.Navigation.AccordionControlElement() {
            Me.accordionItemTables,
            Me.accordionItemViews})
            Me.accordionControl.Name = "accordionControl"
            Me.accordionControl.TabIndex = 0
            Me.accordionControl.Text = "accordionControl"
			' 
            ' accordionItemTables
            ' 
            			
            Me.accordionItemTables.Expanded = True
            Me.accordionItemTables.Text = "Tables"
				
			' 
            ' accordionItemViews
            ' 
            			Me.accordionItemViews.Elements.Add(Me.accordionItemvv_DG_HetHanCollectionView)
						Me.accordionItemViews.Elements.Add(Me.accordionItemvv_MuonCollectionView)
						Me.accordionItemViews.Elements.Add(Me.accordionItemvv_SachItCollectionView)
						
            Me.accordionItemViews.Expanded = True
            Me.accordionItemViews.Text = "Views"
						' 
            ' accordionItemvv_DG_HetHanCollectionView
            ' 
			Me.accordionItemvv_DG_HetHanCollectionView.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item
			Me.accordionItemvv_DG_HetHanCollectionView.Text = "vv DG Het Han"
						' 
            ' accordionItemvv_MuonCollectionView
            ' 
			Me.accordionItemvv_MuonCollectionView.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item
			Me.accordionItemvv_MuonCollectionView.Text = "vv Muon"
						' 
            ' accordionItemvv_SachItCollectionView
            ' 
			Me.accordionItemvv_SachItCollectionView.Style = DevExpress.XtraBars.Navigation.ElementStyle.Item
			Me.accordionItemvv_SachItCollectionView.Text = "vv Sach It"
				
			' 
            ' mvvmContext
            ' 
            Me.mvvmContext.ContainerControl = Me
            Me.mvvmContext.ViewModelType = GetType(Global.GUI.ViewModels.QLTVEntitiesViewModel)
			' 
            ' QLTVEntitiesView
            ' 
			Me.AutoScaleDimensions = New System.Drawing.SizeF(6F, 13F)
            Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
			Me.Controls.Add(Me.dockPanel)
            Me.Controls.Add(Me.ribbonStatusBar)
            Me.Controls.Add(Me.ribbonControl)
			Me.Size = New System.Drawing.Size(1024, 768)
            Me.Name = "QLTVEntitiesView"
			CType(Me.documentManager, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.tabbedView, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.mvvmContext, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.ribbonControl, System.ComponentModel.ISupportInitialize).EndInit()
			CType(Me.dockManager, System.ComponentModel.ISupportInitialize).EndInit()
            Me.dockPanel.ResumeLayout(false)
            Me.dockPanel_Container.ResumeLayout(false)
			CType(Me.accordionControl, System.ComponentModel.ISupportInitialize).EndInit()
			Me.ResumeLayout(false)
            Me.PerformLayout()
		End Sub

		#End Region
		Private documentManager As DevExpress.XtraBars.Docking2010.DocumentManager
        Private tabbedView As DevExpress.XtraBars.Docking2010.Views.Tabbed.TabbedView
		Private mvvmContext As DevExpress.Utils.MVVM.MVVMContext 
		Private ribbonControl As DevExpress.XtraBars.Ribbon.RibbonControl
		Private ribbonPage As DevExpress.XtraBars.Ribbon.RibbonPage
        Private ribbonPageGroup As DevExpress.XtraBars.Ribbon.RibbonPageGroup
		Private ribbonStatusBar As DevExpress.XtraBars.Ribbon.RibbonStatusBar
		Private skinRibbonGalleryBarItem As DevExpress.XtraBars.SkinRibbonGalleryBarItem
		Private dockManager As DevExpress.XtraBars.Docking.DockManager
		Private dockPanel As DevExpress.XtraBars.Docking.DockPanel
        Private dockPanel_Container As DevExpress.XtraBars.Docking.ControlContainer
        Private accordionControl As DevExpress.XtraBars.Navigation.AccordionControl
		Private accordionItemTables As DevExpress.XtraBars.Navigation.AccordionControlElement
		Private accordionItemViews As DevExpress.XtraBars.Navigation.AccordionControlElement

						Private accordionItemvv_DG_HetHanCollectionView As DevExpress.XtraBars.Navigation.AccordionControlElement
				Private accordionItemvv_MuonCollectionView As DevExpress.XtraBars.Navigation.AccordionControlElement
				Private accordionItemvv_SachItCollectionView As DevExpress.XtraBars.Navigation.AccordionControlElement
			End Class
End Namespace
