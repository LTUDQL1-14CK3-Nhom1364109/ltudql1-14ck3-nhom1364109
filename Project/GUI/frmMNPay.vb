﻿Imports DTO
Imports BUS
Public Class frmMNPay
#Region "This"
    Private ts As New TraSachDTO()
    Private ctm As New List(Of ChiTietTraDTO)()
    Private star As Integer
    Private max As Integer
    Private MSBus As New MuonSachBUS()
    Private TSBus As New TraSachBUS()
    Private dgBus As New DocGiaBUS()
    Private sBus As New SachBUS()
    Private lstCT As List(Of String)
    Private lstSCT As List(Of String)
    Private msDTO As New MuonSachDTO()
    Public Sub load(ByVal id As String, ByVal thamso As Integer)
        txtTen1.ReadOnly = True
        txtTen2.ReadOnly = True
        txtTen3.ReadOnly = True
        txtTen4.ReadOnly = True
        txtTen5.ReadOnly = True
        txtTenDG.ReadOnly = True
        txtMa1.ReadOnly = True
        txtMa2.ReadOnly = True
        txtMa3.ReadOnly = True
        txtMa4.ReadOnly = True
        txtMa5.ReadOnly = True
        setTXT()
        If thamso = 1 Then
            txtMaDG.Text = id
        End If
        If thamso = 2 Then
            txtMa1.Text = id
        End If
        Me.Show()
    End Sub
    Private Sub frmMNMuon_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        load("", 3)
        cbxTim.SelectedIndex = 0
        star = 0
        max = 10
    End Sub
    Private Sub setTXT()
        txtMaPhieu.Text = ""
        txtKey.Text = ""
        txtMa1.Text = ""
        txtMa2.Text = ""
        txtMa3.Text = ""
        txtMa4.Text = ""
        txtMa5.Text = ""
        txtTen1.Text = ""
        txtTen2.Text = ""
        txtTen3.Text = ""
        txtTen4.Text = ""
        txtTen5.Text = ""
        txtMaDG.Text = ""
        txtTenDG.Text = ""
    End Sub
    Private Function SetDTO1(ByVal idtra As Integer) As List(Of ChiTietTraDTO)
        Dim tm = New List(Of ChiTietTraDTO)()
        If txtMa1.Text <> "" And cb1.Checked Then
            Dim tem1 As New ChiTietTraDTO()
            tem1.ID_Sach = Convert.ToInt32(txtMa1.Text)
            tem1.ID_Tra = idtra
            tem1.ID = txtMa1.Text
            tm.Add(tem1)
            If txtMa2.Text <> "" And cb2.Checked Then
                Dim tem2 As New ChiTietTraDTO()
                tem2.ID_Sach = Convert.ToInt32(txtMa2.Text)
                tem2.ID_Sach = idtra
                tem2.ID = txtMa2.Text
                tm.Add(tem2)
                If txtMa3.Text <> "" And cb3.Checked Then
                    Dim tem3 As New ChiTietTraDTO()
                    tem3.ID_Sach = Convert.ToInt32(txtMa3.Text)
                    tem3.ID_Tra = idtra
                    tem3.ID = txtMa3.Text
                    tm.Add(tem3)
                    If txtMa4.Text <> "" And cb4.Checked Then
                        Dim tem4 As New ChiTietTraDTO()
                        tem4.ID_Sach = Convert.ToInt32(txtMa4.Text)
                        tem4.ID_Tra = idtra
                        tem4.ID = txtMa4.Text
                        tm.Add(tem4)
                        If txtMa5.Text <> "" And cb5.Checked Then
                            Dim tem5 As New ChiTietTraDTO()
                            tem5.ID_Sach = Convert.ToInt32(txtMa5.Text)
                            tem5.ID_Tra = idtra
                            tem5.ID = txtMa5.Text
                            tm.Add(tem5)
                        End If
                    End If
                End If
            End If
        End If
        Return tm
    End Function
    Private Function SetDTO2() As TraSachDTO
        Dim ms As New TraSachDTO
        If txtMaTra.Text = "" Then
            ms.ID = 0
        Else
            ms.ID = Convert.ToInt32(txtMaTra.Text)
        End If
        ms.ID_Muon = Convert.ToInt32(txtMaPhieu.Text)
        ms.Ngay = dtpTra.Value
        Return ms
    End Function
#End Region
#Region "load_thongtin"
    Private Sub txtMaDG_TextChanged(sender As Object, e As EventArgs) Handles txtMaDG.TextChanged
        Try
            txtTenDG.Text = dgBus.ShowTen(txtMaDG.Text)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub txtMa1_TextChanged(sender As Object, e As EventArgs) Handles txtMa1.TextChanged
        Try
            txtTen1.Text = sBus.ShowTen(txtMa1.Text)
        Catch ex As Exception

        End Try

    End Sub
    Private Sub txtMa2_TextChanged(sender As Object, e As EventArgs) Handles txtMa2.TextChanged
        Try
            txtTen2.Text = sBus.ShowTen(txtMa2.Text)
        Catch ex As Exception

        End Try

    End Sub
    Private Sub txtMa3_TextChanged(sender As Object, e As EventArgs) Handles txtMa3.TextChanged
        Try
            txtTen3.Text = sBus.ShowTen(txtMa3.Text)
        Catch ex As Exception

        End Try

    End Sub
    Private Sub txtMa4_TextChanged(sender As Object, e As EventArgs) Handles txtMa4.TextChanged
        Try
            txtTen4.Text = sBus.ShowTen(txtMa4.Text)
        Catch ex As Exception

        End Try

    End Sub
    Private Sub txtMa5_TextChanged(sender As Object, e As EventArgs) Handles txtMa5.TextChanged
        Try
            txtTen5.Text = sBus.ShowTen(txtMa5.Text)
        Catch ex As Exception

        End Try

    End Sub
    Private Sub txtMaPhieu_TextChanged(sender As Object, e As EventArgs) Handles txtMaPhieu.TextChanged
        Try
            Dim dt As New DataTable()
            dt = MSBus.TableTim(0, txtMaPhieu.Text)
            txtMaDG.Text = MSBus.TableTim(0, txtMaPhieu.Text)(0)(1)
            txtTenDG.Text = dgBus.TableTim(0, txtMaDG.Text)(0)(1)
            dtpNgay.Text = dt(0)(2)
            lstCT = New List(Of String)()
            lstSCT = New List(Of String)()
            lstSCT = MSBus.CTM(txtMaPhieu.Text, lstCT)
            If lstSCT.Count > 0 Then
                txtMa1.Text = lstSCT(0)
                If lstCT.Count > 1 Then
                    txtMa2.Text = lstSCT(1)
                    If lstCT.Count > 2 Then
                        txtMa3.Text = lstSCT(2)
                        If lstCT.Count > 3 Then
                            txtMa4.Text = lstSCT(3)
                            If lstCT.Count > 4 Then
                                txtMa5.Text = lstSCT(5)
                            End If
                        End If
                    End If
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub txtMaTra_TextChanged(sender As Object, e As EventArgs) Handles txtMaTra.TextChanged
        Try
            Dim dt As New DataTable()
            dt = TSBus.TableTim(0, txtMaTra.Text)
            txtMaPhieu.Text = dt(0)(1)
            dtpTra.Value = dt(0)(4)
            lblPhat.Text = dt(0)(6)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub dgvTable_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTable.CellClick
        setTXT()
        Dim row As Integer
        row = dgvTable.CurrentRow.Index
        txtMaTra.Text = dgvTable.Rows(row).Cells(0).Value.ToString
    End Sub
#End Region
#Region "Phan Trang"
    Private Sub btnFirst_Click(sender As Object, e As EventArgs) Handles btnFirst.Click
        star = 0
        HienDS(star, max)
    End Sub
    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        star += max
        HienDS(star, max)
    End Sub
    Private Sub btnPre_Click(sender As Object, e As EventArgs) Handles btnPre.Click
        star -= max
        If star < 0 Then star = 0
        HienDS(star, max)
    End Sub
#End Region
#Region "Tool"
    Public Sub HienDS(ByVal i As Integer, ByVal s As Integer)
        max = s
        star = i
        If star = 0 Then
            btnFirst.Visible = False
            btnPre.Visible = False
        Else
            btnFirst.Visible = True
            btnPre.Visible = True
        End If
        If TSBus.Table(star + 10, max).Rows.Count = 0 Then
            btnNext.Visible = False
        Else
            btnNext.Visible = True
        End If
        dgvTable.DataSource = TSBus.Table(star, max)
    End Sub
    Public Sub Them()
        Dim idtra As Integer
        idtra = -1
        ctm = SetDTO1(idtra)
        lstSCT = New List(Of String)()
        For Each i As ChiTietTraDTO In ctm
            lstSCT.Add(i.ID_Sach.ToString)
        Next
        ts = SetDTO2()
        If txtMaDG.Text <> "" Then
            If MessageBox.Show("Ban Co Chac Chan Muon Them Thong Tin", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                idtra = TSBus.Them(ts)
                ctm = SetDTO1(idtra)
                For Each i As ChiTietTraDTO In ctm
                    TSBus.ThemCT(i)
                Next
                MessageBox.Show("Thành Công")
                setTXT()
            Else
                MessageBox.Show("Ban da muon qua so sach")
            End If
        End If
    End Sub
    Public Sub Sua()
        Dim tsDTO As TraSachDTO
        tsDTO = New TraSachDTO()
        If Me.Validate = False Then
            Me.Show()
        ElseIf txtMaPhieu.Text <> "" Then
            If MessageBox.Show("Ban Co Chac Chan Muon Sua Thong Tin", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                tsDTO = SetDTO2()
                TSBus.Sua(tsDTO)
            End If
        End If
    End Sub
    Public Sub Xoa()
        If txtMaTra.Text <> "" Then
            If MessageBox.Show("Ban Co Chac Chan Muon Xoa Thong Tin ", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                If TSBus.Xoa(txtMaTra.Text) > -1 Then
                    MessageBox.Show("Xoa Thanh Cong")
                Else
                    MessageBox.Show("Vui long nhap Ma Doc Gia")
                End If
            End If
        End If
    End Sub
    Private Sub btnTim_Click(sender As Object, e As EventArgs) Handles btnTim.Click
        Dim tab As DataTable
        tab = TSBus.TableTim(cbxTim.SelectedIndex, txtKey.Text)
        If tab.Rows.Count() = 0 Then
            MessageBox.Show("Khong tim thay du lieu")
        Else
            dgvTable.DataSource = tab
        End If
    End Sub
    Private Sub btnXemCT_Click(sender As Object, e As EventArgs) Handles btnXemCT.Click
        frmCTTra.Value = Convert.ToInt32(txtMaTra.Text)
        frmCTTra.Show()
    End Sub
    Private Sub btnInCT_Click(sender As Object, e As EventArgs) Handles btnInCT.Click
        frmCTTra.Value = Convert.ToInt32(txtMaTra.Text)
        frmCTTra.Show()
    End Sub
#End Region
End Class