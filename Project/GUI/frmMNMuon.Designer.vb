﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMNMuon
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.txtMa2 = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.txtTen2 = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.btnFirst = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnPre = New System.Windows.Forms.Button()
        Me.txtMa5 = New System.Windows.Forms.TextBox()
        Me.txtTen5 = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtMaDG = New System.Windows.Forms.TextBox()
        Me.txtTenDG = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtMa1 = New System.Windows.Forms.TextBox()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtTen3 = New System.Windows.Forms.TextBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.txtTen4 = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtTen1 = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtMa3 = New System.Windows.Forms.TextBox()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txtMa4 = New System.Windows.Forms.TextBox()
        Me.dtpNgay = New System.Windows.Forms.DateTimePicker()
        Me.lblTitle = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.cbxTim = New System.Windows.Forms.ComboBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnTim = New System.Windows.Forms.Button()
        Me.txtKey = New System.Windows.Forms.TextBox()
        Me.dgvTable = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.lblTrang = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.txtMaPhieu = New System.Windows.Forms.TextBox()
        Me.btnInCT = New System.Windows.Forms.Button()
        Me.btnXemCT = New System.Windows.Forms.Button()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvTable, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtMa2
        '
        Me.txtMa2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMa2.Location = New System.Drawing.Point(86, 52)
        Me.txtMa2.Name = "txtMa2"
        Me.txtMa2.Size = New System.Drawing.Size(245, 23)
        Me.txtMa2.TabIndex = 11
        '
        'Label9
        '
        Me.Label9.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(3, 104)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(68, 16)
        Me.Label9.TabIndex = 6
        Me.Label9.Text = "Mã Sách 3"
        '
        'txtTen2
        '
        Me.txtTen2.Location = New System.Drawing.Point(467, 52)
        Me.txtTen2.Name = "txtTen2"
        Me.txtTen2.Size = New System.Drawing.Size(231, 23)
        Me.txtTen2.TabIndex = 16
        '
        'Label12
        '
        Me.Label12.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(378, 49)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(73, 16)
        Me.Label12.TabIndex = 15
        Me.Label12.Text = "Tên Sách 2"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(3, 196)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(68, 16)
        Me.Label6.TabIndex = 20
        Me.Label6.Text = "Mã Sách 5"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.btnFirst)
        Me.GroupBox3.Controls.Add(Me.btnNext)
        Me.GroupBox3.Controls.Add(Me.btnPre)
        Me.GroupBox3.Location = New System.Drawing.Point(20, 374)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(287, 56)
        Me.GroupBox3.TabIndex = 31
        Me.GroupBox3.TabStop = False
        '
        'btnFirst
        '
        Me.btnFirst.Location = New System.Drawing.Point(6, 15)
        Me.btnFirst.Name = "btnFirst"
        Me.btnFirst.Size = New System.Drawing.Size(93, 29)
        Me.btnFirst.TabIndex = 2
        Me.btnFirst.Text = "Trang Đầu"
        Me.btnFirst.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(105, 15)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 29)
        Me.btnNext.TabIndex = 1
        Me.btnNext.Text = "Trang Kế"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnPre
        '
        Me.btnPre.Location = New System.Drawing.Point(186, 15)
        Me.btnPre.Name = "btnPre"
        Me.btnPre.Size = New System.Drawing.Size(95, 29)
        Me.btnPre.TabIndex = 0
        Me.btnPre.Text = "Trang Trước"
        Me.btnPre.UseVisualStyleBackColor = True
        '
        'txtMa5
        '
        Me.txtMa5.Location = New System.Drawing.Point(86, 199)
        Me.txtMa5.Name = "txtMa5"
        Me.txtMa5.Size = New System.Drawing.Size(245, 23)
        Me.txtMa5.TabIndex = 21
        '
        'txtTen5
        '
        Me.txtTen5.Location = New System.Drawing.Point(467, 199)
        Me.txtTen5.Name = "txtTen5"
        Me.txtTen5.Size = New System.Drawing.Size(231, 23)
        Me.txtTen5.TabIndex = 22
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.Label14)
        Me.GroupBox4.Controls.Add(Me.Label13)
        Me.GroupBox4.Controls.Add(Me.txtMaDG)
        Me.GroupBox4.Controls.Add(Me.txtTenDG)
        Me.GroupBox4.Location = New System.Drawing.Point(12, 251)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(295, 100)
        Me.GroupBox4.TabIndex = 34
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Thông Tin Độc Giả"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(55, 61)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(78, 16)
        Me.Label14.TabIndex = 3
        Me.Label14.Text = "Tên Độc Giả"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(55, 29)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(73, 16)
        Me.Label13.TabIndex = 2
        Me.Label13.Text = "Mã Độc Giả"
        '
        'txtMaDG
        '
        Me.txtMaDG.Location = New System.Drawing.Point(155, 26)
        Me.txtMaDG.Name = "txtMaDG"
        Me.txtMaDG.Size = New System.Drawing.Size(100, 23)
        Me.txtMaDG.TabIndex = 1
        '
        'txtTenDG
        '
        Me.txtTenDG.Location = New System.Drawing.Point(155, 61)
        Me.txtTenDG.Name = "txtTenDG"
        Me.txtTenDG.Size = New System.Drawing.Size(100, 23)
        Me.txtTenDG.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.TableLayoutPanel1)
        Me.GroupBox2.Location = New System.Drawing.Point(333, 52)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(736, 338)
        Me.GroupBox2.TabIndex = 30
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Thông Tin Sách Mượn"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TableLayoutPanel1.ColumnCount = 7
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 9.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 41.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10.0!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.Label15, 0, 10)
        Me.TableLayoutPanel1.Controls.Add(Me.Label3, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtMa1, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtTen2, 6, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label12, 4, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label8, 4, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.txtTen3, 6, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.Label10, 4, 8)
        Me.TableLayoutPanel1.Controls.Add(Me.txtTen4, 6, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.Label7, 4, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.Label4, 4, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.txtTen1, 6, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Label5, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.txtMa2, 2, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.Label9, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.txtMa3, 2, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.Label11, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.txtMa4, 2, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.Label6, 0, 8)
        Me.TableLayoutPanel1.Controls.Add(Me.txtMa5, 2, 8)
        Me.TableLayoutPanel1.Controls.Add(Me.txtTen5, 6, 8)
        Me.TableLayoutPanel1.Controls.Add(Me.dtpNgay, 2, 10)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(6, 38)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 11
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle())
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(698, 270)
        Me.TableLayoutPanel1.TabIndex = 14
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(3, 245)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(36, 16)
        Me.Label15.TabIndex = 27
        Me.Label15.Text = "Ngày"
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 6)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(68, 16)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "Mã Sách 1"
        '
        'txtMa1
        '
        Me.txtMa1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMa1.Location = New System.Drawing.Point(86, 3)
        Me.txtMa1.Name = "txtMa1"
        Me.txtMa1.Size = New System.Drawing.Size(245, 23)
        Me.txtMa1.TabIndex = 9
        '
        'Label8
        '
        Me.Label8.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(378, 98)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(73, 16)
        Me.Label8.TabIndex = 5
        Me.Label8.Text = "Tên Sách 3"
        '
        'txtTen3
        '
        Me.txtTen3.Location = New System.Drawing.Point(467, 101)
        Me.txtTen3.Name = "txtTen3"
        Me.txtTen3.Size = New System.Drawing.Size(231, 23)
        Me.txtTen3.TabIndex = 14
        '
        'Label10
        '
        Me.Label10.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(378, 202)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(73, 16)
        Me.Label10.TabIndex = 7
        Me.Label10.Text = "Tên Sách 5"
        '
        'txtTen4
        '
        Me.txtTen4.Location = New System.Drawing.Point(467, 150)
        Me.txtTen4.Name = "txtTen4"
        Me.txtTen4.Size = New System.Drawing.Size(231, 23)
        Me.txtTen4.TabIndex = 18
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(378, 147)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(73, 16)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Tên Sách 4"
        Me.Label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(378, 6)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(73, 16)
        Me.Label4.TabIndex = 1
        Me.Label4.Text = "Tên Sách 1"
        '
        'txtTen1
        '
        Me.txtTen1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtTen1.Location = New System.Drawing.Point(467, 3)
        Me.txtTen1.Name = "txtTen1"
        Me.txtTen1.Size = New System.Drawing.Size(245, 23)
        Me.txtTen1.TabIndex = 10
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 55)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(68, 16)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Mã Sách 2"
        '
        'txtMa3
        '
        Me.txtMa3.Location = New System.Drawing.Point(86, 101)
        Me.txtMa3.Name = "txtMa3"
        Me.txtMa3.Size = New System.Drawing.Size(245, 23)
        Me.txtMa3.TabIndex = 17
        '
        'Label11
        '
        Me.Label11.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(3, 153)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(68, 16)
        Me.Label11.TabIndex = 13
        Me.Label11.Text = "Mã Sách 4"
        '
        'txtMa4
        '
        Me.txtMa4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txtMa4.Location = New System.Drawing.Point(86, 150)
        Me.txtMa4.Name = "txtMa4"
        Me.txtMa4.Size = New System.Drawing.Size(245, 23)
        Me.txtMa4.TabIndex = 12
        '
        'dtpNgay
        '
        Me.dtpNgay.Location = New System.Drawing.Point(86, 248)
        Me.dtpNgay.Name = "dtpNgay"
        Me.dtpNgay.Size = New System.Drawing.Size(245, 23)
        Me.dtpNgay.TabIndex = 28
        '
        'lblTitle
        '
        Me.lblTitle.AutoSize = True
        Me.lblTitle.Font = New System.Drawing.Font("Tahoma", 20.0!)
        Me.lblTitle.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblTitle.Location = New System.Drawing.Point(438, 5)
        Me.lblTitle.Name = "lblTitle"
        Me.lblTitle.Size = New System.Drawing.Size(251, 33)
        Me.lblTitle.TabIndex = 27
        Me.lblTitle.Text = "Quản Lý Mượn Sách"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.cbxTim)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.btnTim)
        Me.GroupBox1.Controls.Add(Me.txtKey)
        Me.GroupBox1.Location = New System.Drawing.Point(12, 52)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(295, 135)
        Me.GroupBox1.TabIndex = 29
        Me.GroupBox1.TabStop = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(4, 91)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(63, 16)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Tìm Theo"
        '
        'cbxTim
        '
        Me.cbxTim.FormattingEnabled = True
        Me.cbxTim.Items.AddRange(New Object() {"Mã Phiếu Mượn", "Tên Độc Giả"})
        Me.cbxTim.Location = New System.Drawing.Point(73, 87)
        Me.cbxTim.Name = "cbxTim"
        Me.cbxTim.Size = New System.Drawing.Size(121, 24)
        Me.cbxTim.TabIndex = 3
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(4, 38)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(56, 16)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Từ Khoá"
        '
        'btnTim
        '
        Me.btnTim.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnTim.Location = New System.Drawing.Point(200, 88)
        Me.btnTim.Name = "btnTim"
        Me.btnTim.Size = New System.Drawing.Size(89, 23)
        Me.btnTim.TabIndex = 1
        Me.btnTim.Text = "Tìm Kiếm"
        Me.btnTim.UseVisualStyleBackColor = True
        '
        'txtKey
        '
        Me.txtKey.Location = New System.Drawing.Point(66, 35)
        Me.txtKey.Name = "txtKey"
        Me.txtKey.Size = New System.Drawing.Size(223, 23)
        Me.txtKey.TabIndex = 0
        '
        'dgvTable
        '
        Me.dgvTable.AllowUserToOrderColumns = True
        Me.dgvTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill
        Me.dgvTable.BackgroundColor = System.Drawing.SystemColors.ActiveCaption
        Me.dgvTable.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.dgvTable.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SunkenVertical
        Me.dgvTable.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken
        Me.dgvTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvTable.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4})
        Me.dgvTable.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.dgvTable.GridColor = System.Drawing.SystemColors.ActiveCaption
        Me.dgvTable.Location = New System.Drawing.Point(0, 470)
        Me.dgvTable.Name = "dgvTable"
        Me.dgvTable.ScrollBars = System.Windows.Forms.ScrollBars.None
        Me.dgvTable.Size = New System.Drawing.Size(1100, 248)
        Me.dgvTable.StandardTab = True
        Me.dgvTable.TabIndex = 28
        '
        'Column1
        '
        Me.Column1.DataPropertyName = "ID"
        Me.Column1.HeaderText = "Mã Phiếu trả"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        '
        'Column2
        '
        Me.Column2.DataPropertyName = "TenDG"
        Me.Column2.HeaderText = "Tên Độc Giả"
        Me.Column2.Name = "Column2"
        '
        'Column3
        '
        Me.Column3.DataPropertyName = "Ngay"
        Me.Column3.HeaderText = "Ngày Mượn"
        Me.Column3.Name = "Column3"
        '
        'Column4
        '
        Me.Column4.DataPropertyName = "ID_DocGia"
        Me.Column4.HeaderText = "ID_DG"
        Me.Column4.Name = "Column4"
        Me.Column4.Visible = False
        '
        'lblTrang
        '
        Me.lblTrang.AutoSize = True
        Me.lblTrang.Location = New System.Drawing.Point(313, 396)
        Me.lblTrang.Name = "lblTrang"
        Me.lblTrang.Size = New System.Drawing.Size(0, 16)
        Me.lblTrang.TabIndex = 33
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(7, 197)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(96, 16)
        Me.Label16.TabIndex = 35
        Me.Label16.Text = "Mã Phiếu Mượn"
        '
        'txtMaPhieu
        '
        Me.txtMaPhieu.Location = New System.Drawing.Point(106, 194)
        Me.txtMaPhieu.Name = "txtMaPhieu"
        Me.txtMaPhieu.Size = New System.Drawing.Size(100, 23)
        Me.txtMaPhieu.TabIndex = 36
        '
        'btnInCT
        '
        Me.btnInCT.Location = New System.Drawing.Point(589, 407)
        Me.btnInCT.Name = "btnInCT"
        Me.btnInCT.Size = New System.Drawing.Size(88, 23)
        Me.btnInCT.TabIndex = 52
        Me.btnInCT.Text = "In Chi Tiết"
        Me.btnInCT.UseVisualStyleBackColor = True
        '
        'btnXemCT
        '
        Me.btnXemCT.Location = New System.Drawing.Point(444, 407)
        Me.btnXemCT.Name = "btnXemCT"
        Me.btnXemCT.Size = New System.Drawing.Size(98, 23)
        Me.btnXemCT.TabIndex = 51
        Me.btnXemCT.Text = "Xem Chi Tiết"
        Me.btnXemCT.UseVisualStyleBackColor = True
        '
        'frmMNMuon
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(1100, 718)
        Me.Controls.Add(Me.btnInCT)
        Me.Controls.Add(Me.btnXemCT)
        Me.Controls.Add(Me.txtMaPhieu)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox4)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.lblTitle)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.dgvTable)
        Me.Controls.Add(Me.lblTrang)
        Me.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "frmMNMuon"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "frmMNMuon"
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox4.ResumeLayout(False)
        Me.GroupBox4.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvTable, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtMa2 As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents txtTen2 As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents btnFirst As Button
    Friend WithEvents btnNext As Button
    Friend WithEvents btnPre As Button
    Friend WithEvents txtMa5 As TextBox
    Friend WithEvents txtTen5 As TextBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents txtMaDG As TextBox
    Friend WithEvents txtTenDG As TextBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents Label15 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents txtMa1 As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents txtTen3 As TextBox
    Friend WithEvents Label10 As Label
    Friend WithEvents txtTen4 As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents txtTen1 As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents txtMa3 As TextBox
    Friend WithEvents Label11 As Label
    Friend WithEvents txtMa4 As TextBox
    Friend WithEvents dtpNgay As DateTimePicker
    Friend WithEvents lblTitle As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label1 As Label
    Friend WithEvents cbxTim As ComboBox
    Friend WithEvents Label2 As Label
    Friend WithEvents btnTim As Button
    Friend WithEvents txtKey As TextBox
    Friend WithEvents dgvTable As DataGridView
    Friend WithEvents lblTrang As Label
    Friend WithEvents Label16 As Label
    Friend WithEvents txtMaPhieu As TextBox
    Friend WithEvents Column1 As DataGridViewTextBoxColumn
    Friend WithEvents Column2 As DataGridViewTextBoxColumn
    Friend WithEvents Column3 As DataGridViewTextBoxColumn
    Friend WithEvents Column4 As DataGridViewTextBoxColumn
    Friend WithEvents btnInCT As Button
    Friend WithEvents btnXemCT As Button
End Class
