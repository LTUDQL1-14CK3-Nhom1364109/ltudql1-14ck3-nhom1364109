﻿Imports DTO
Imports BUS

Public Class frmMNMuon
#Region "This"
    Private ms As New MuonSachDTO()
    Private ctm As New List(Of ChiTietMuonDTO)()
    Private star As Integer
    Private max As Integer
    Private MSBus As New MuonSachBUS()
    Private dgBus As New DocGiaBUS()
    Private sBus As New SachBUS()
    Private lstCT As List(Of String)
    Private lstSCT As List(Of String)
    Public Sub load(ByVal id As String, ByVal thamso As Integer)
        txtTen1.ReadOnly = True
        txtTen2.ReadOnly = True
        txtTen3.ReadOnly = True
        txtTen4.ReadOnly = True
        txtTen5.ReadOnly = True
        txtTenDG.ReadOnly = True
        setTXT()
        If thamso = 1 Then
            txtMaDG.Text = id
        End If
        If thamso = 2 Then
            txtMa1.Text = id
        End If
        Me.Show()
    End Sub
    Private Sub frmMNMuon_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        load("", 3)
        cbxTim.SelectedIndex = 0
        star = 0
        max = 10
    End Sub
    Private Function SetDTO1(ByVal idmuon As Integer) As List(Of ChiTietMuonDTO)
        Dim tm = New List(Of ChiTietMuonDTO)()
        If txtMa1.Text <> "" Then
            Dim tem1 As New ChiTietMuonDTO()
            tem1.ID_Sach = Convert.ToInt32(txtMa1.Text)
            tem1.ID_Muon = idmuon
            tem1.ID = txtMa1.Text
            tm.Add(tem1)
            If txtMa2.Text <> "" Then
                Dim tem2 As New ChiTietMuonDTO()
                tem2.ID_Sach = Convert.ToInt32(txtMa2.Text)
                tem2.ID_Muon = idmuon
                tem2.ID = txtMa2.Text
                tm.Add(tem2)
                If txtMa3.Text <> "" Then
                    Dim tem3 As New ChiTietMuonDTO()
                    tem3.ID_Sach = Convert.ToInt32(txtMa3.Text)
                    tem3.ID_Muon = idmuon
                    tem3.ID = txtMa3.Text
                    tm.Add(tem3)
                    If txtMa4.Text <> "" Then
                        Dim tem4 As New ChiTietMuonDTO()
                        tem4.ID_Sach = Convert.ToInt32(txtMa4.Text)
                        tem4.ID_Muon = idmuon
                        tem4.ID = txtMa4.Text
                        tm.Add(tem4)
                        If txtMa5.Text <> "" Then
                            Dim tem5 As New ChiTietMuonDTO()
                            tem5.ID_Sach = Convert.ToInt32(txtMa5.Text)
                            tem5.ID_Muon = idmuon
                            tem5.ID = txtMa5.Text
                            tm.Add(tem5)
                        End If
                    End If
                End If
            End If
        End If
        Return tm
    End Function
    Private Function SetDTO2() As MuonSachDTO
        Dim ms As New MuonSachDTO
        If txtMaPhieu.Text = "" Then
            ms.ID = 0
        Else
            ms.ID = Convert.ToInt32(txtMaPhieu.Text)
        End If
        ms.ID_DocGia = Convert.ToInt32(txtMaDG.Text)
        ms.Ngay = dtpNgay.Value
        Return ms
    End Function
#End Region
#Region "loadinfo"
    Private Sub txtMaDG_TextChanged(sender As Object, e As EventArgs) Handles txtMaDG.TextChanged
        Try
            txtTenDG.Text = dgBus.ShowTen(txtMaDG.Text)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub txtMa1_TextChanged(sender As Object, e As EventArgs) Handles txtMa1.TextChanged
        Try
            txtTen1.Text = sBus.ShowTen(txtMa1.Text)
        Catch ex As Exception

        End Try

    End Sub
    Private Sub txtMa2_TextChanged(sender As Object, e As EventArgs) Handles txtMa2.TextChanged
        Try
            txtTen2.Text = sBus.ShowTen(txtMa2.Text)
        Catch ex As Exception

        End Try

    End Sub
    Private Sub txtMa3_TextChanged(sender As Object, e As EventArgs) Handles txtMa3.TextChanged
        Try
            txtTen3.Text = sBus.ShowTen(txtMa3.Text)
        Catch ex As Exception

        End Try

    End Sub
    Private Sub txtMa4_TextChanged(sender As Object, e As EventArgs) Handles txtMa4.TextChanged
        Try
            txtTen4.Text = sBus.ShowTen(txtMa4.Text)
        Catch ex As Exception

        End Try

    End Sub
    Private Sub txtMa5_TextChanged(sender As Object, e As EventArgs) Handles txtMa5.TextChanged
        Try
            txtTen5.Text = sBus.ShowTen(txtMa5.Text)
        Catch ex As Exception

        End Try

    End Sub
    Private Sub dgvTable_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTable.CellClick
        setTXT()
        Dim row As Integer
        row = dgvTable.CurrentRow.Index
        txtMaPhieu.Text = dgvTable.Rows(row).Cells(0).Value().ToString
        txtTenDG.Text = dgvTable.Rows(row).Cells(3).Value().ToString
        txtMaDG.Text = dgvTable.Rows(row).Cells(1).Value().ToString
        dtpNgay.Value = dgvTable.Rows(row).Cells(2).Value().ToString
        lstCT = New List(Of String)()
        lstSCT = New List(Of String)()
        lstSCT = MSBus.CT(txtMaPhieu.Text, lstCT)
        If lstSCT.Count > 0 Then
            txtMa1.Text = lstSCT(0)
            If lstCT.Count > 1 Then
                txtMa2.Text = lstSCT(1)
                If lstCT.Count > 2 Then
                    txtMa3.Text = lstSCT(2)
                    If lstCT.Count > 3 Then
                        txtMa4.Text = lstSCT(3)
                        If lstCT.Count > 4 Then
                            txtMa5.Text = lstSCT(5)
                        End If
                    End If
                End If
            End If
        End If
    End Sub
    Private Sub setTXT()
        txtMaPhieu.Text = ""
        txtKey.Text = ""
        txtMa1.Text = ""
        txtMa2.Text = ""
        txtMa3.Text = ""
        txtMa4.Text = ""
        txtMa5.Text = ""
        txtTen1.Text = ""
        txtTen2.Text = ""
        txtTen3.Text = ""
        txtTen4.Text = ""
        txtTen5.Text = ""
        txtMaDG.Text = ""
        txtTenDG.Text = ""
    End Sub
#End Region
#Region "Phan Trang"
    Private Sub btnFirst_Click(sender As Object, e As EventArgs) Handles btnFirst.Click
        star = 0
        HienDS(star, max)
    End Sub
    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        star += max
        HienDS(star, max)
    End Sub
    Private Sub btnPre_Click(sender As Object, e As EventArgs) Handles btnPre.Click
        star -= max
        If star < 0 Then star = 0
        HienDS(star, max)
    End Sub
#End Region
#Region "Tool"
    Public Sub HienDS(ByVal i As Integer, ByVal s As Integer)
        max = s
        star = i
        If star = 0 Then
            btnFirst.Visible = False
            btnPre.Visible = False
        Else
            btnFirst.Visible = True
            btnPre.Visible = True
        End If
        If MSBus.Table(star + 10, max).Rows.Count = 0 Then
            btnNext.Visible = False
        Else
            btnNext.Visible = True
        End If
        dgvTable.DataSource = MSBus.Table(star, max)
    End Sub
    Public Sub Them()
        Dim idmuon As Integer
        idmuon = -1

        ctm = SetDTO1(idmuon)
        lstSCT = New List(Of String)()
        For Each i As ChiTietMuonDTO In ctm
            lstSCT.Add(i.ID_Sach.ToString)
        Next
        If MSBus.ISSachDCMuon(lstSCT) Then
            ms = SetDTO2()
            If txtMaDG.Text <> "" Then
                If MessageBox.Show("Ban Co Chac Chan Muon Them Thong Tin", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                    If MSBus.ISMuon(Convert.ToInt32(txtMaDG.Text), ctm.Count()) Then
                        idmuon = MSBus.Them(ms)
                        ctm = SetDTO1(idmuon)
                        For Each i As ChiTietMuonDTO In ctm
                            MSBus.ThemCT(i)
                        Next
                        MessageBox.Show("Thành Công")
                    Else
                        MessageBox.Show("Ban da muon qua so sach")
                    End If
                End If
            End If
        Else
            MessageBox.Show("Sach da het hoac bi xoa")
        End If
    End Sub
    Public Sub Sua()
        Dim ms As MuonSachDTO
        ms = New MuonSachDTO()
        If Me.Validate = False Then
            Me.Show()
        ElseIf txtMaPhieu.Text <> "" Then
            If MessageBox.Show("Ban Co Chac Chan Muon Sua Thong Tin", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                ms = SetDTO2()
                MSBus.Sua(ms)
            End If
        End If
    End Sub
    Public Sub Xoa()
        If txtMaPhieu.Text <> "" Then
            If MessageBox.Show("Ban Co Chac Chan Muon Xoa Thong Tin ", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                If MSBus.Xoa(txtMaPhieu.Text) > -1 Then
                    MessageBox.Show("Xoa Thanh Cong")
                Else
                    MessageBox.Show("Vui long nhap Ma Doc Gia")
                End If
            End If
        End If
    End Sub
    Private Sub btnTim_Click(sender As Object, e As EventArgs) Handles btnTim.Click
        Dim tab As DataTable
        tab = MSBus.TableTim(cbxTim.SelectedIndex, txtKey.Text)
        If tab.Rows.Count() = 0 Then
            MessageBox.Show("Khong tim thay du lieu")
        Else
            dgvTable.DataSource = tab
        End If
    End Sub
    Private Sub btnXemCT_Click(sender As Object, e As EventArgs) Handles btnXemCT.Click
        frmCTMuon.Value = Convert.ToInt32(txtMaPhieu.Text)
        frmCTMuon.Show()
    End Sub
    Private Sub btnInCT_Click(sender As Object, e As EventArgs) Handles btnInCT.Click
        frmCTMuon.Value = Convert.ToInt32(txtMaPhieu.Text)
        frmCTMuon.Show()
    End Sub
#End Region
End Class