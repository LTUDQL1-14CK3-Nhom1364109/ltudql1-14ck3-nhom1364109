﻿Imports DTO
Imports BUS

Public Class frmMNDocGia
#Region "This"
    Private star As Integer
    Private max As Integer
    Private DGBus As New DocGiaBUS()

    Private Sub frmMNDocGia_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cbxTim.SelectedIndex = 0
        cbxChucVu.DataSource = DGBus.ListLQ()
        star = 0
        max = 10
    End Sub
    Private Function SetDTO() As DocGiaDTO
        Dim dgDTO As New DocGiaDTO()
        If txtID.Text = "" Then
            dgDTO.ID = 0
        Else
            dgDTO.ID = Convert.ToInt32(txtID.Text)
        End If
        dgDTO.ID_Loai = cbxChucVu.SelectedIndex + 1
        dgDTO.Xoa = 0
        dgDTO.NgayHetHan = dtpNHH.Value.ToShortDateString()
        dgDTO.NgaySinh = dtpNSinh.Value.ToShortDateString()
        dgDTO.GTinh = txtGTinh.Text
        dgDTO.Tel = txtFone.Text
        dgDTO.TenDG = txtTen.Text
        dgDTO.Email = txtEmail.Text
        dgDTO.DiaChi = txtDiaChi.Text
        dgDTO.CMND = txtCMND.Text
        Return dgDTO
    End Function
    Private Sub dgvTable_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTable.CellClick
        Dim row As Integer
        row = dgvTable.CurrentRow.Index
        txtID.Text = dgvTable.Rows(row).Cells(0).Value().ToString
        txtTen.Text = dgvTable.Rows(row).Cells(1).Value().ToString
        txtGTinh.Text = dgvTable.Rows(row).Cells(2).Value().ToString
        txtCMND.Text = dgvTable.Rows(row).Cells(3).Value().ToString
        txtEmail.Text = dgvTable.Rows(row).Cells(8).Value().ToString
        txtFone.Text = dgvTable.Rows(row).Cells(7).Value().ToString
        txtDiaChi.Text = dgvTable.Rows(row).Cells(6).Value().ToString
        cbxChucVu.Text = dgvTable.Rows(row).Cells(11).Value().ToString
        dtpNHH.Value = dgvTable.Rows(row).Cells(10).Value().ToString
        dtpNSinh.Value = dgvTable.Rows(row).Cells(4).Value().ToString
    End Sub
#End Region
#Region "Tool"
    Public Sub HienDS(ByVal i As Integer, ByVal s As Integer)
        max = s
        star = i
        If star = 0 Then
            btnFirst.Visible = False
            btnPre.Visible = False
        Else
            btnFirst.Visible = True
            btnPre.Visible = True
        End If
        If DGBus.Table(star + 10, max).Rows.Count = 0 Then
            btnNext.Visible = False
        Else
            btnNext.Visible = True
        End If
        dgvTable.DataSource = DGBus.Table(star, max)
    End Sub
    Public Sub Them()
        If txtTen.Text <> "" Then
            If MessageBox.Show("Ban Co Chac Chan Muon Them Thong Tin", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                If DGBus.Them(SetDTO()) > -1 Then
                    MessageBox.Show("Thành Công")
                    HienDS(star, max)
                Else
                    MessageBox.Show("Vui long nhap Day Du Thong Tin")
                End If
            End If
        End If
    End Sub
    Public Sub Sua()
        If Me.Validate = False Then
            Me.Show()
        ElseIf txtTen.Text <> "" Then
            If MessageBox.Show("Ban Co Chac Chan Muon Sua Thong Tin", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                If DGBus.Sua(SetDTO()) > -1 Then
                    MessageBox.Show("Thành Công")
                    HienDS(star, max)
                Else
                    MessageBox.Show("Vui long nhap Day Du Thong Tin")
                End If
            End If
        End If
    End Sub
    Public Sub Xoa()
        If txtTen.Text <> "" Then
            If MessageBox.Show("Ban Co Chac Chan Muon Xoa Thong Tin ", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                If DGBus.Xoa(txtID.Text) > -1 Then
                    MessageBox.Show("Xoa Thanh Cong")
                    HienDS(star, max)
                Else
                    MessageBox.Show("Vui long nhap Ma Doc Gia")
                End If
            End If
        End If
    End Sub
    Private Sub btnTim_Click(sender As Object, e As EventArgs) Handles btnTim.Click
        Dim tab As DataTable
        tab = DGBus.TableTim(cbxTim.SelectedIndex, txtKey.Text)
        If tab.Rows.Count() = 0 Then
            MessageBox.Show("Khong tim thay du lieu")
        Else
            dgvTable.DataSource = tab
        End If
    End Sub
#End Region
#Region "Phan Trang"
    Private Sub btnFirst_Click(sender As Object, e As EventArgs) Handles btnFirst.Click
        star = 0
        HienDS(star, max)
    End Sub

    Private Sub btnNext_Click(sender As Object, e As EventArgs) Handles btnNext.Click
        star += max
        HienDS(star, max)
    End Sub

    Private Sub btnPre_Click(sender As Object, e As EventArgs) Handles btnPre.Click
        star -= max
        If star < 0 Then star = 0
        HienDS(star, max)
    End Sub

    Private Sub btnPrice_Click(sender As Object, e As EventArgs) Handles btnPrice.Click
        If txtID.Text = "" Then
            MessageBox.Show("Vui Long Nhap Ma Doc Gia Can Gia Han")
        ElseIf MessageBox.Show("Phi Gia Han La " + DGBus.Phi(txtID.Text).ToString() + " Ban Muon Tiep Tuc", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
            If DGBus.Ghan(txtID.Text) = -1 Then
                MessageBox.Show("That Bai")
            Else
                MessageBox.Show("Thanh Cong")
            End If
        End If
    End Sub

    Private Sub btnBor_Click(sender As Object, e As EventArgs) Handles btnBor.Click
        frmParent.ShowFrm(frmMNMuon)
        frmMNMuon.load(txtID.Text, 1)
    End Sub

    Private Sub btnPay_Click(sender As Object, e As EventArgs) Handles btnPay.Click
        frmParent.ShowFrm(frmMNPay)
        frmMNPay.load(txtID.Text, 1)
    End Sub

    Private Sub btnXemCT_Click(sender As Object, e As EventArgs) Handles btnXemCT.Click
        frmCTDG.Value = Convert.ToInt32(txtID.Text)
        frmCTDG.Show()
    End Sub
#End Region
End Class