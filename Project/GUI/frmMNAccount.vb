﻿Imports BUS
Imports DTO

Public Class frmMNAccount
    Private Sub frmMNAccount_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cbxTim.SelectedIndex = 0
    End Sub
    Private AccBUS As New AccountBUS()
    Private Function SetDTO() As AccountDTO
        Dim AcDTO As New AccountDTO()
        AcDTO.ID = Convert.ToInt32(txtID.Text)
        AcDTO.Name = txtTen.Text
        AcDTO.User = txtUser.Text
        AcDTO.Pass = txtPass.Text
        Return AcDTO
    End Function
    Private Sub dgvTable_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvTable.CellClick
        Dim row As Integer
        row = dgvTable.CurrentRow.Index
        txtID.Text = dgvTable.Rows(row).Cells(0).Value().ToString
        txtTen.Text = dgvTable.Rows(row).Cells(3).Value().ToString
        txtUser.Text = dgvTable.Rows(row).Cells(1).Value().ToString
        txtPass.Text = dgvTable.Rows(row).Cells(2).Value().ToString
    End Sub
#Region "Tool"
    Public Sub HienDS()
        dgvTable.DataSource = AccBUS.Table()
    End Sub
    Public Sub Them()
        If txtTen.Text <> "" Then
            If MessageBox.Show("Ban Co Chac Chan Muon Them Thong Tin", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                If AccBUS.Them(SetDTO()) > -1 Then
                    MessageBox.Show("Thành Công")
                    HienDS()
                Else
                    MessageBox.Show("Vui long nhap Day Du Thong Tin")
                End If
            End If
        End If
    End Sub
    Public Sub Sua()
        If Me.Validate = False Then
            Me.Show()
        ElseIf txtTen.Text <> "" Then
            If MessageBox.Show("Ban Co Chac Chan Muon Sua Thong Tin", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                If AccBUS.Sua(SetDTO()) > -1 Then
                    MessageBox.Show("Thành Công")
                    HienDS()
                Else
                    MessageBox.Show("Vui long nhap Day Du Thong Tin")
                End If
            End If
        End If
    End Sub
    Public Sub Xoa()
        If txtTen.Text <> "" Then
            If MessageBox.Show("Ban Co Chac Chan Muon Xoa Thong Tin ", "Cảnh Báo", MessageBoxButtons.YesNo) = DialogResult.Yes Then
                If AccBUS.Xoa(txtID.Text) > -1 Then
                    MessageBox.Show("Xoa Thanh Cong")
                    HienDS()
                Else
                    MessageBox.Show("Vui long nhap Ma Doc Gia")
                End If
            End If
        End If
    End Sub
    Private Sub btnTim_Click(sender As Object, e As EventArgs) Handles btnTim.Click
        Dim tab As DataTable
        tab = AccBUS.TableTim(cbxTim.SelectedIndex, txtKey.Text)
        If tab.Rows.Count() = 0 Then
            MessageBox.Show("Khong tim thay du lieu")
        Else
            dgvTable.DataSource = tab
        End If
    End Sub
#End Region
End Class