﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmParent
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmParent))
        Me.MenuStrip = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.FileMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.SaveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveAsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.PrintToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SáchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.QuảnLýSáchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ThêmToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.XoáSáchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SửaSáchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ĐộcGiảToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ThểLoạiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrảMượnSáchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MượnSáchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrảSáchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ThốngKêToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ThốngKêToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.XemXoáToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.lblUser = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.SideNav1 = New DevComponents.DotNetBar.Controls.SideNav()
        Me.SideNavPanel5 = New DevComponents.DotNetBar.Controls.SideNavPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.LinkLabel1 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.LinkLabel2 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.LinkLabel4 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.LinkLabel3 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.SideNavPanel1 = New DevComponents.DotNetBar.Controls.SideNavPanel()
        Me.pnl24 = New System.Windows.Forms.Panel()
        Me.llbl24 = New System.Windows.Forms.LinkLabel()
        Me.pb24 = New System.Windows.Forms.PictureBox()
        Me.pnl21 = New System.Windows.Forms.Panel()
        Me.llbl21 = New System.Windows.Forms.LinkLabel()
        Me.pbx11 = New System.Windows.Forms.PictureBox()
        Me.pnl23 = New System.Windows.Forms.Panel()
        Me.lbl23 = New System.Windows.Forms.LinkLabel()
        Me.pb23 = New System.Windows.Forms.PictureBox()
        Me.pnl22 = New System.Windows.Forms.Panel()
        Me.llbl22 = New System.Windows.Forms.LinkLabel()
        Me.pb22 = New System.Windows.Forms.PictureBox()
        Me.SideNavPanel2 = New DevComponents.DotNetBar.Controls.SideNavPanel()
        Me.pnl44 = New System.Windows.Forms.Panel()
        Me.LinkLabel5 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.pnl41 = New System.Windows.Forms.Panel()
        Me.LinkLabel6 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.pnl42 = New System.Windows.Forms.Panel()
        Me.LinkLabel8 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.pnl43 = New System.Windows.Forms.Panel()
        Me.LinkLabel7 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.SideNavPanel4 = New DevComponents.DotNetBar.Controls.SideNavPanel()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.LinkLabel9 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.LinkLabel10 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.Panel12 = New System.Windows.Forms.Panel()
        Me.LinkLabel12 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox12 = New System.Windows.Forms.PictureBox()
        Me.Panel11 = New System.Windows.Forms.Panel()
        Me.LinkLabel11 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.SideNavPanel3 = New DevComponents.DotNetBar.Controls.SideNavPanel()
        Me.Panel13 = New System.Windows.Forms.Panel()
        Me.LinkLabel13 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox13 = New System.Windows.Forms.PictureBox()
        Me.Panel15 = New System.Windows.Forms.Panel()
        Me.LinkLabel15 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox15 = New System.Windows.Forms.PictureBox()
        Me.Panel14 = New System.Windows.Forms.Panel()
        Me.LinkLabel14 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox14 = New System.Windows.Forms.PictureBox()
        Me.Panel16 = New System.Windows.Forms.Panel()
        Me.LinkLabel16 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox16 = New System.Windows.Forms.PictureBox()
        Me.SideNavPanel7 = New DevComponents.DotNetBar.Controls.SideNavPanel()
        Me.Panel17 = New System.Windows.Forms.Panel()
        Me.LinkLabel17 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox17 = New System.Windows.Forms.PictureBox()
        Me.Panel20 = New System.Windows.Forms.Panel()
        Me.LinkLabel20 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox20 = New System.Windows.Forms.PictureBox()
        Me.Panel18 = New System.Windows.Forms.Panel()
        Me.LinkLabel18 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox18 = New System.Windows.Forms.PictureBox()
        Me.Panel19 = New System.Windows.Forms.Panel()
        Me.LinkLabel19 = New System.Windows.Forms.LinkLabel()
        Me.PictureBox19 = New System.Windows.Forms.PictureBox()
        Me.SideNavPanel6 = New DevComponents.DotNetBar.Controls.SideNavPanel()
        Me.SideNavItem1 = New DevComponents.DotNetBar.Controls.SideNavItem()
        Me.Separator1 = New DevComponents.DotNetBar.Separator()
        Me.SideNavItem3 = New DevComponents.DotNetBar.Controls.SideNavItem()
        Me.Separator6 = New DevComponents.DotNetBar.Separator()
        Me.SideNavItem8 = New DevComponents.DotNetBar.Controls.SideNavItem()
        Me.SideNavItem2 = New DevComponents.DotNetBar.Controls.SideNavItem()
        Me.Separator5 = New DevComponents.DotNetBar.Separator()
        Me.sniDG = New DevComponents.DotNetBar.Controls.SideNavItem()
        Me.sniUser = New DevComponents.DotNetBar.Controls.SideNavItem()
        Me.Separator3 = New DevComponents.DotNetBar.Separator()
        Me.SideNavItem4 = New DevComponents.DotNetBar.Controls.SideNavItem()
        Me.Separator2 = New DevComponents.DotNetBar.Separator()
        Me.SideNavItem9 = New DevComponents.DotNetBar.Controls.SideNavItem()
        Me.SideNavItem10 = New DevComponents.DotNetBar.Controls.SideNavItem()
        Me.Separator4 = New DevComponents.DotNetBar.Separator()
        Me.sniThoat = New DevComponents.DotNetBar.Controls.SideNavItem()
        Me.Separator7 = New DevComponents.DotNetBar.Separator()
        Me.MenuStrip.SuspendLayout()
        Me.StatusStrip.SuspendLayout()
        Me.SideNav1.SuspendLayout()
        Me.SideNavPanel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel4.SuspendLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel3.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SideNavPanel1.SuspendLayout()
        Me.pnl24.SuspendLayout()
        CType(Me.pb24, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl21.SuspendLayout()
        CType(Me.pbx11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl23.SuspendLayout()
        CType(Me.pb23, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl22.SuspendLayout()
        CType(Me.pb22, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SideNavPanel2.SuspendLayout()
        Me.pnl44.SuspendLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl41.SuspendLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl42.SuspendLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnl43.SuspendLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SideNavPanel4.SuspendLayout()
        Me.Panel9.SuspendLayout()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel10.SuspendLayout()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel12.SuspendLayout()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel11.SuspendLayout()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SideNavPanel3.SuspendLayout()
        Me.Panel13.SuspendLayout()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel15.SuspendLayout()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel14.SuspendLayout()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel16.SuspendLayout()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SideNavPanel7.SuspendLayout()
        Me.Panel17.SuspendLayout()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel20.SuspendLayout()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel18.SuspendLayout()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel19.SuspendLayout()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip
        '
        Me.MenuStrip.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.MenuStrip.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.MenuStrip.Font = New System.Drawing.Font("Tahoma", 9.75!)
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Size = New System.Drawing.Size(659, 24)
        Me.MenuStrip.TabIndex = 5
        Me.MenuStrip.Text = "MenuStrip"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem1})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(40, 20)
        Me.FileToolStripMenuItem.Text = "&File"
        '
        'ExitToolStripMenuItem1
        '
        Me.ExitToolStripMenuItem1.Name = "ExitToolStripMenuItem1"
        Me.ExitToolStripMenuItem1.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.F4), System.Windows.Forms.Keys)
        Me.ExitToolStripMenuItem1.Size = New System.Drawing.Size(141, 22)
        Me.ExitToolStripMenuItem1.Text = "&Exit"
        '
        'FileMenu
        '
        Me.FileMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.NewToolStripMenuItem, Me.OpenToolStripMenuItem, Me.ToolStripSeparator3, Me.SaveToolStripMenuItem, Me.SaveAsToolStripMenuItem, Me.ToolStripSeparator4, Me.PrintToolStripMenuItem, Me.ToolStripSeparator5, Me.ExitToolStripMenuItem})
        Me.FileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder
        Me.FileMenu.Name = "FileMenu"
        Me.FileMenu.Size = New System.Drawing.Size(40, 20)
        Me.FileMenu.Text = "&File"
        '
        'NewToolStripMenuItem
        '
        Me.NewToolStripMenuItem.Image = CType(resources.GetObject("NewToolStripMenuItem.Image"), System.Drawing.Image)
        Me.NewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.NewToolStripMenuItem.Name = "NewToolStripMenuItem"
        Me.NewToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.NewToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.NewToolStripMenuItem.Text = "&New"
        '
        'OpenToolStripMenuItem
        '
        Me.OpenToolStripMenuItem.Image = CType(resources.GetObject("OpenToolStripMenuItem.Image"), System.Drawing.Image)
        Me.OpenToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem"
        Me.OpenToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.OpenToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.OpenToolStripMenuItem.Text = "&Open"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(143, 6)
        '
        'SaveToolStripMenuItem
        '
        Me.SaveToolStripMenuItem.Image = CType(resources.GetObject("SaveToolStripMenuItem.Image"), System.Drawing.Image)
        Me.SaveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.SaveToolStripMenuItem.Name = "SaveToolStripMenuItem"
        Me.SaveToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.SaveToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.SaveToolStripMenuItem.Text = "&Save"
        '
        'SaveAsToolStripMenuItem
        '
        Me.SaveAsToolStripMenuItem.Name = "SaveAsToolStripMenuItem"
        Me.SaveAsToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.SaveAsToolStripMenuItem.Text = "Save &As"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(143, 6)
        '
        'PrintToolStripMenuItem
        '
        Me.PrintToolStripMenuItem.Image = CType(resources.GetObject("PrintToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PrintToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.PrintToolStripMenuItem.Name = "PrintToolStripMenuItem"
        Me.PrintToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.PrintToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.PrintToolStripMenuItem.Text = "&Print"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(143, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.F4), System.Windows.Forms.Keys)
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.ExitToolStripMenuItem.Text = "E&xit"
        '
        'SáchToolStripMenuItem
        '
        Me.SáchToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.QuảnLýSáchToolStripMenuItem, Me.ThêmToolStripMenuItem, Me.XoáSáchToolStripMenuItem, Me.SửaSáchToolStripMenuItem})
        Me.SáchToolStripMenuItem.Name = "SáchToolStripMenuItem"
        Me.SáchToolStripMenuItem.Size = New System.Drawing.Size(48, 20)
        Me.SáchToolStripMenuItem.Text = "&Sách"
        '
        'QuảnLýSáchToolStripMenuItem
        '
        Me.QuảnLýSáchToolStripMenuItem.Name = "QuảnLýSáchToolStripMenuItem"
        Me.QuảnLýSáchToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.QuảnLýSáchToolStripMenuItem.Text = "Quản Lý Sách"
        '
        'ThêmToolStripMenuItem
        '
        Me.ThêmToolStripMenuItem.Name = "ThêmToolStripMenuItem"
        Me.ThêmToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.ThêmToolStripMenuItem.Text = "Thêm Sách"
        '
        'XoáSáchToolStripMenuItem
        '
        Me.XoáSáchToolStripMenuItem.Name = "XoáSáchToolStripMenuItem"
        Me.XoáSáchToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.XoáSáchToolStripMenuItem.Text = "Xoá Sách"
        '
        'SửaSáchToolStripMenuItem
        '
        Me.SửaSáchToolStripMenuItem.Name = "SửaSáchToolStripMenuItem"
        Me.SửaSáchToolStripMenuItem.Size = New System.Drawing.Size(146, 22)
        Me.SửaSáchToolStripMenuItem.Text = "Sửa Sách"
        '
        'ĐộcGiảToolStripMenuItem
        '
        Me.ĐộcGiảToolStripMenuItem.Name = "ĐộcGiảToolStripMenuItem"
        Me.ĐộcGiảToolStripMenuItem.Size = New System.Drawing.Size(64, 20)
        Me.ĐộcGiảToolStripMenuItem.Text = "&Độc Giả"
        '
        'ThểLoạiToolStripMenuItem
        '
        Me.ThểLoạiToolStripMenuItem.Name = "ThểLoạiToolStripMenuItem"
        Me.ThểLoạiToolStripMenuItem.Size = New System.Drawing.Size(69, 20)
        Me.ThểLoạiToolStripMenuItem.Text = "Thể &Loại"
        '
        'TrảMượnSáchToolStripMenuItem
        '
        Me.TrảMượnSáchToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.MượnSáchToolStripMenuItem, Me.TrảSáchToolStripMenuItem})
        Me.TrảMượnSáchToolStripMenuItem.Name = "TrảMượnSáchToolStripMenuItem"
        Me.TrảMượnSáchToolStripMenuItem.Size = New System.Drawing.Size(108, 20)
        Me.TrảMượnSáchToolStripMenuItem.Text = "Trả Mượn Sách"
        '
        'MượnSáchToolStripMenuItem
        '
        Me.MượnSáchToolStripMenuItem.Name = "MượnSáchToolStripMenuItem"
        Me.MượnSáchToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.MượnSáchToolStripMenuItem.Text = "Mượn Sách"
        '
        'TrảSáchToolStripMenuItem
        '
        Me.TrảSáchToolStripMenuItem.Name = "TrảSáchToolStripMenuItem"
        Me.TrảSáchToolStripMenuItem.Size = New System.Drawing.Size(134, 22)
        Me.TrảSáchToolStripMenuItem.Text = "Trả Sách"
        '
        'ThốngKêToolStripMenuItem
        '
        Me.ThốngKêToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ThốngKêToolStripMenuItem1, Me.XemXoáToolStripMenuItem})
        Me.ThốngKêToolStripMenuItem.Name = "ThốngKêToolStripMenuItem"
        Me.ThốngKêToolStripMenuItem.Size = New System.Drawing.Size(68, 20)
        Me.ThốngKêToolStripMenuItem.Text = "Công Cụ"
        '
        'ThốngKêToolStripMenuItem1
        '
        Me.ThốngKêToolStripMenuItem1.Name = "ThốngKêToolStripMenuItem1"
        Me.ThốngKêToolStripMenuItem1.Size = New System.Drawing.Size(153, 22)
        Me.ThốngKêToolStripMenuItem1.Text = "Thống Kê"
        '
        'XemXoáToolStripMenuItem
        '
        Me.XemXoáToolStripMenuItem.Name = "XemXoáToolStripMenuItem"
        Me.XemXoáToolStripMenuItem.Size = New System.Drawing.Size(153, 22)
        Me.XemXoáToolStripMenuItem.Text = "Danh Sách Xoá"
        '
        'HelpMenu
        '
        Me.HelpMenu.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem})
        Me.HelpMenu.Name = "HelpMenu"
        Me.HelpMenu.Size = New System.Drawing.Size(77, 20)
        Me.HelpMenu.Text = "&Giới Thiệu"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.AboutToolStripMenuItem.Text = "&About ..."
        '
        'StatusStrip
        '
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblUser})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 750)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(659, 22)
        Me.StatusStrip.TabIndex = 7
        Me.StatusStrip.Text = "StatusStrip"
        '
        'lblUser
        '
        Me.lblUser.Name = "lblUser"
        Me.lblUser.Size = New System.Drawing.Size(0, 17)
        '
        'ToolStripStatusLabel
        '
        Me.ToolStripStatusLabel.Name = "ToolStripStatusLabel"
        Me.ToolStripStatusLabel.Size = New System.Drawing.Size(39, 17)
        Me.ToolStripStatusLabel.Text = "Status"
        '
        'SideNav1
        '
        Me.SideNav1.Controls.Add(Me.SideNavPanel5)
        Me.SideNav1.Controls.Add(Me.SideNavPanel1)
        Me.SideNav1.Controls.Add(Me.SideNavPanel2)
        Me.SideNav1.Controls.Add(Me.SideNavPanel4)
        Me.SideNav1.Controls.Add(Me.SideNavPanel3)
        Me.SideNav1.Controls.Add(Me.SideNavPanel7)
        Me.SideNav1.Controls.Add(Me.SideNavPanel6)
        Me.SideNav1.Dock = System.Windows.Forms.DockStyle.Left
        Me.SideNav1.Items.AddRange(New DevComponents.DotNetBar.BaseItem() {Me.SideNavItem1, Me.Separator1, Me.SideNavItem3, Me.Separator6, Me.SideNavItem8, Me.SideNavItem2, Me.Separator5, Me.sniDG, Me.sniUser, Me.Separator3, Me.SideNavItem4, Me.Separator2, Me.SideNavItem9, Me.SideNavItem10, Me.Separator4, Me.sniThoat})
        Me.SideNav1.Location = New System.Drawing.Point(0, 24)
        Me.SideNav1.Name = "SideNav1"
        Me.SideNav1.Padding = New System.Windows.Forms.Padding(1)
        Me.SideNav1.Size = New System.Drawing.Size(260, 726)
        Me.SideNav1.TabIndex = 9
        Me.SideNav1.Text = "SideNav1"
        '
        'SideNavPanel5
        '
        Me.SideNavPanel5.Controls.Add(Me.Panel1)
        Me.SideNavPanel5.Controls.Add(Me.Panel2)
        Me.SideNavPanel5.Controls.Add(Me.Panel4)
        Me.SideNavPanel5.Controls.Add(Me.Panel3)
        Me.SideNavPanel5.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SideNavPanel5.Location = New System.Drawing.Point(104, 36)
        Me.SideNavPanel5.Name = "SideNavPanel5"
        Me.SideNavPanel5.Size = New System.Drawing.Size(151, 689)
        Me.SideNavPanel5.TabIndex = 23
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel1.Controls.Add(Me.LinkLabel1)
        Me.Panel1.Controls.Add(Me.PictureBox1)
        Me.Panel1.Location = New System.Drawing.Point(18, 468)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(120, 130)
        Me.Panel1.TabIndex = 22
        '
        'LinkLabel1
        '
        Me.LinkLabel1.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel1.AutoSize = True
        Me.LinkLabel1.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel1.Location = New System.Drawing.Point(23, 106)
        Me.LinkLabel1.Name = "LinkLabel1"
        Me.LinkLabel1.Size = New System.Drawing.Size(78, 13)
        Me.LinkLabel1.TabIndex = 1
        Me.LinkLabel1.TabStop = True
        Me.LinkLabel1.Text = "Xoá Thông Tin"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel2.Controls.Add(Me.LinkLabel2)
        Me.Panel2.Controls.Add(Me.PictureBox2)
        Me.Panel2.Location = New System.Drawing.Point(18, 15)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(120, 130)
        Me.Panel2.TabIndex = 20
        '
        'LinkLabel2
        '
        Me.LinkLabel2.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel2.AutoSize = True
        Me.LinkLabel2.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel2.Location = New System.Drawing.Point(18, 106)
        Me.LinkLabel2.Name = "LinkLabel2"
        Me.LinkLabel2.Size = New System.Drawing.Size(85, 13)
        Me.LinkLabel2.TabIndex = 1
        Me.LinkLabel2.TabStop = True
        Me.LinkLabel2.Text = "Xem Danh Sách"
        '
        'PictureBox2
        '
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 0
        Me.PictureBox2.TabStop = False
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel4.Controls.Add(Me.LinkLabel4)
        Me.Panel4.Controls.Add(Me.PictureBox4)
        Me.Panel4.Location = New System.Drawing.Point(18, 166)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(120, 130)
        Me.Panel4.TabIndex = 19
        '
        'LinkLabel4
        '
        Me.LinkLabel4.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel4.AutoSize = True
        Me.LinkLabel4.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel4.Location = New System.Drawing.Point(11, 106)
        Me.LinkLabel4.Name = "LinkLabel4"
        Me.LinkLabel4.Size = New System.Drawing.Size(98, 13)
        Me.LinkLabel4.TabIndex = 1
        Me.LinkLabel4.TabStop = True
        Me.LinkLabel4.Text = "Quản Lý Loại Sách"
        '
        'PictureBox4
        '
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 0
        Me.PictureBox4.TabStop = False
        '
        'Panel3
        '
        Me.Panel3.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel3.Controls.Add(Me.LinkLabel3)
        Me.Panel3.Controls.Add(Me.PictureBox3)
        Me.Panel3.Location = New System.Drawing.Point(18, 317)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(120, 130)
        Me.Panel3.TabIndex = 21
        '
        'LinkLabel3
        '
        Me.LinkLabel3.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel3.AutoSize = True
        Me.LinkLabel3.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel3.Location = New System.Drawing.Point(23, 106)
        Me.LinkLabel3.Name = "LinkLabel3"
        Me.LinkLabel3.Size = New System.Drawing.Size(74, 13)
        Me.LinkLabel3.TabIndex = 1
        Me.LinkLabel3.TabStop = True
        Me.LinkLabel3.Text = "Sửa thông Tin"
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 0
        Me.PictureBox3.TabStop = False
        '
        'SideNavPanel1
        '
        Me.SideNavPanel1.Controls.Add(Me.pnl24)
        Me.SideNavPanel1.Controls.Add(Me.pnl21)
        Me.SideNavPanel1.Controls.Add(Me.pnl23)
        Me.SideNavPanel1.Controls.Add(Me.pnl22)
        Me.SideNavPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SideNavPanel1.Location = New System.Drawing.Point(112, 36)
        Me.SideNavPanel1.Name = "SideNavPanel1"
        Me.SideNavPanel1.Size = New System.Drawing.Size(143, 689)
        Me.SideNavPanel1.TabIndex = 35
        Me.SideNavPanel1.Visible = False
        '
        'pnl24
        '
        Me.pnl24.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.pnl24.Controls.Add(Me.llbl24)
        Me.pnl24.Controls.Add(Me.pb24)
        Me.pnl24.Location = New System.Drawing.Point(15, 484)
        Me.pnl24.Name = "pnl24"
        Me.pnl24.Size = New System.Drawing.Size(120, 130)
        Me.pnl24.TabIndex = 18
        '
        'llbl24
        '
        Me.llbl24.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.llbl24.AutoSize = True
        Me.llbl24.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.llbl24.Location = New System.Drawing.Point(33, 106)
        Me.llbl24.Name = "llbl24"
        Me.llbl24.Size = New System.Drawing.Size(54, 13)
        Me.llbl24.TabIndex = 1
        Me.llbl24.TabStop = True
        Me.llbl24.Text = "Xoá Sách"
        '
        'pb24
        '
        Me.pb24.Image = CType(resources.GetObject("pb24.Image"), System.Drawing.Image)
        Me.pb24.Location = New System.Drawing.Point(10, 3)
        Me.pb24.Name = "pb24"
        Me.pb24.Size = New System.Drawing.Size(100, 100)
        Me.pb24.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pb24.TabIndex = 0
        Me.pb24.TabStop = False
        '
        'pnl21
        '
        Me.pnl21.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.pnl21.Controls.Add(Me.llbl21)
        Me.pnl21.Controls.Add(Me.pbx11)
        Me.pnl21.Location = New System.Drawing.Point(15, 3)
        Me.pnl21.Name = "pnl21"
        Me.pnl21.Size = New System.Drawing.Size(120, 130)
        Me.pnl21.TabIndex = 16
        '
        'llbl21
        '
        Me.llbl21.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.llbl21.AutoSize = True
        Me.llbl21.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.llbl21.Location = New System.Drawing.Point(18, 106)
        Me.llbl21.Name = "llbl21"
        Me.llbl21.Size = New System.Drawing.Size(85, 13)
        Me.llbl21.TabIndex = 1
        Me.llbl21.TabStop = True
        Me.llbl21.Text = "Xem Danh Sách"
        '
        'pbx11
        '
        Me.pbx11.Image = CType(resources.GetObject("pbx11.Image"), System.Drawing.Image)
        Me.pbx11.Location = New System.Drawing.Point(10, 3)
        Me.pbx11.Name = "pbx11"
        Me.pbx11.Size = New System.Drawing.Size(100, 100)
        Me.pbx11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pbx11.TabIndex = 0
        Me.pbx11.TabStop = False
        '
        'pnl23
        '
        Me.pnl23.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.pnl23.Controls.Add(Me.lbl23)
        Me.pnl23.Controls.Add(Me.pb23)
        Me.pnl23.Location = New System.Drawing.Point(18, 320)
        Me.pnl23.Name = "pnl23"
        Me.pnl23.Size = New System.Drawing.Size(120, 130)
        Me.pnl23.TabIndex = 17
        '
        'lbl23
        '
        Me.lbl23.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.lbl23.AutoSize = True
        Me.lbl23.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.lbl23.Location = New System.Drawing.Point(33, 106)
        Me.lbl23.Name = "lbl23"
        Me.lbl23.Size = New System.Drawing.Size(54, 13)
        Me.lbl23.TabIndex = 1
        Me.lbl23.TabStop = True
        Me.lbl23.Text = "Sửa Sách"
        '
        'pb23
        '
        Me.pb23.Image = CType(resources.GetObject("pb23.Image"), System.Drawing.Image)
        Me.pb23.Location = New System.Drawing.Point(10, 3)
        Me.pb23.Name = "pb23"
        Me.pb23.Size = New System.Drawing.Size(100, 100)
        Me.pb23.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pb23.TabIndex = 0
        Me.pb23.TabStop = False
        '
        'pnl22
        '
        Me.pnl22.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.pnl22.Controls.Add(Me.llbl22)
        Me.pnl22.Controls.Add(Me.pb22)
        Me.pnl22.Location = New System.Drawing.Point(15, 163)
        Me.pnl22.Name = "pnl22"
        Me.pnl22.Size = New System.Drawing.Size(120, 130)
        Me.pnl22.TabIndex = 15
        '
        'llbl22
        '
        Me.llbl22.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.llbl22.AutoSize = True
        Me.llbl22.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.llbl22.Location = New System.Drawing.Point(29, 106)
        Me.llbl22.Name = "llbl22"
        Me.llbl22.Size = New System.Drawing.Size(62, 13)
        Me.llbl22.TabIndex = 1
        Me.llbl22.TabStop = True
        Me.llbl22.Text = "Thêm Sách"
        '
        'pb22
        '
        Me.pb22.Image = CType(resources.GetObject("pb22.Image"), System.Drawing.Image)
        Me.pb22.Location = New System.Drawing.Point(10, 3)
        Me.pb22.Name = "pb22"
        Me.pb22.Size = New System.Drawing.Size(100, 100)
        Me.pb22.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.pb22.TabIndex = 0
        Me.pb22.TabStop = False
        '
        'SideNavPanel2
        '
        Me.SideNavPanel2.Controls.Add(Me.pnl44)
        Me.SideNavPanel2.Controls.Add(Me.pnl41)
        Me.SideNavPanel2.Controls.Add(Me.pnl42)
        Me.SideNavPanel2.Controls.Add(Me.pnl43)
        Me.SideNavPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SideNavPanel2.Location = New System.Drawing.Point(112, 36)
        Me.SideNavPanel2.Name = "SideNavPanel2"
        Me.SideNavPanel2.Size = New System.Drawing.Size(143, 689)
        Me.SideNavPanel2.TabIndex = 6
        Me.SideNavPanel2.Visible = False
        '
        'pnl44
        '
        Me.pnl44.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.pnl44.Controls.Add(Me.LinkLabel5)
        Me.pnl44.Controls.Add(Me.PictureBox5)
        Me.pnl44.Location = New System.Drawing.Point(18, 532)
        Me.pnl44.Name = "pnl44"
        Me.pnl44.Size = New System.Drawing.Size(120, 130)
        Me.pnl44.TabIndex = 22
        '
        'LinkLabel5
        '
        Me.LinkLabel5.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel5.AutoSize = True
        Me.LinkLabel5.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel5.Location = New System.Drawing.Point(23, 106)
        Me.LinkLabel5.Name = "LinkLabel5"
        Me.LinkLabel5.Size = New System.Drawing.Size(84, 13)
        Me.LinkLabel5.TabIndex = 1
        Me.LinkLabel5.TabStop = True
        Me.LinkLabel5.Text = "Xoá Thành Viên"
        '
        'PictureBox5
        '
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 0
        Me.PictureBox5.TabStop = False
        '
        'pnl41
        '
        Me.pnl41.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.pnl41.Controls.Add(Me.LinkLabel6)
        Me.pnl41.Controls.Add(Me.PictureBox6)
        Me.pnl41.Location = New System.Drawing.Point(18, 25)
        Me.pnl41.Name = "pnl41"
        Me.pnl41.Size = New System.Drawing.Size(120, 130)
        Me.pnl41.TabIndex = 20
        '
        'LinkLabel6
        '
        Me.LinkLabel6.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel6.AutoSize = True
        Me.LinkLabel6.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel6.Location = New System.Drawing.Point(18, 106)
        Me.LinkLabel6.Name = "LinkLabel6"
        Me.LinkLabel6.Size = New System.Drawing.Size(85, 13)
        Me.LinkLabel6.TabIndex = 1
        Me.LinkLabel6.TabStop = True
        Me.LinkLabel6.Text = "Xem Danh Sách"
        '
        'PictureBox6
        '
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox6.TabIndex = 0
        Me.PictureBox6.TabStop = False
        '
        'pnl42
        '
        Me.pnl42.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.pnl42.Controls.Add(Me.LinkLabel8)
        Me.pnl42.Controls.Add(Me.PictureBox8)
        Me.pnl42.Location = New System.Drawing.Point(18, 197)
        Me.pnl42.Name = "pnl42"
        Me.pnl42.Size = New System.Drawing.Size(120, 130)
        Me.pnl42.TabIndex = 19
        '
        'LinkLabel8
        '
        Me.LinkLabel8.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel8.AutoSize = True
        Me.LinkLabel8.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel8.Location = New System.Drawing.Point(14, 106)
        Me.LinkLabel8.Name = "LinkLabel8"
        Me.LinkLabel8.Size = New System.Drawing.Size(92, 13)
        Me.LinkLabel8.TabIndex = 1
        Me.LinkLabel8.TabStop = True
        Me.LinkLabel8.Text = "Thêm Thành Viên"
        '
        'PictureBox8
        '
        Me.PictureBox8.Image = CType(resources.GetObject("PictureBox8.Image"), System.Drawing.Image)
        Me.PictureBox8.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox8.TabIndex = 0
        Me.PictureBox8.TabStop = False
        '
        'pnl43
        '
        Me.pnl43.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.pnl43.Controls.Add(Me.LinkLabel7)
        Me.pnl43.Controls.Add(Me.PictureBox7)
        Me.pnl43.Location = New System.Drawing.Point(18, 367)
        Me.pnl43.Name = "pnl43"
        Me.pnl43.Size = New System.Drawing.Size(120, 130)
        Me.pnl43.TabIndex = 21
        '
        'LinkLabel7
        '
        Me.LinkLabel7.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel7.AutoSize = True
        Me.LinkLabel7.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel7.Location = New System.Drawing.Point(23, 106)
        Me.LinkLabel7.Name = "LinkLabel7"
        Me.LinkLabel7.Size = New System.Drawing.Size(78, 13)
        Me.LinkLabel7.TabIndex = 1
        Me.LinkLabel7.TabStop = True
        Me.LinkLabel7.Text = "Sửa Thông Tin"
        '
        'PictureBox7
        '
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox7.TabIndex = 0
        Me.PictureBox7.TabStop = False
        '
        'SideNavPanel4
        '
        Me.SideNavPanel4.Controls.Add(Me.Panel9)
        Me.SideNavPanel4.Controls.Add(Me.Panel10)
        Me.SideNavPanel4.Controls.Add(Me.Panel12)
        Me.SideNavPanel4.Controls.Add(Me.Panel11)
        Me.SideNavPanel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SideNavPanel4.Location = New System.Drawing.Point(112, 36)
        Me.SideNavPanel4.Name = "SideNavPanel4"
        Me.SideNavPanel4.Size = New System.Drawing.Size(143, 689)
        Me.SideNavPanel4.TabIndex = 14
        Me.SideNavPanel4.Visible = False
        '
        'Panel9
        '
        Me.Panel9.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel9.Controls.Add(Me.LinkLabel9)
        Me.Panel9.Controls.Add(Me.PictureBox9)
        Me.Panel9.Location = New System.Drawing.Point(18, 463)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(120, 130)
        Me.Panel9.TabIndex = 22
        '
        'LinkLabel9
        '
        Me.LinkLabel9.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel9.AutoSize = True
        Me.LinkLabel9.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel9.Location = New System.Drawing.Point(23, 106)
        Me.LinkLabel9.Name = "LinkLabel9"
        Me.LinkLabel9.Size = New System.Drawing.Size(66, 13)
        Me.LinkLabel9.TabIndex = 1
        Me.LinkLabel9.TabStop = True
        Me.LinkLabel9.Text = "Xoá Dữ Liệu"
        '
        'PictureBox9
        '
        Me.PictureBox9.Image = CType(resources.GetObject("PictureBox9.Image"), System.Drawing.Image)
        Me.PictureBox9.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox9.TabIndex = 0
        Me.PictureBox9.TabStop = False
        '
        'Panel10
        '
        Me.Panel10.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel10.Controls.Add(Me.LinkLabel10)
        Me.Panel10.Controls.Add(Me.PictureBox10)
        Me.Panel10.Location = New System.Drawing.Point(18, 10)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(120, 130)
        Me.Panel10.TabIndex = 20
        '
        'LinkLabel10
        '
        Me.LinkLabel10.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel10.AutoSize = True
        Me.LinkLabel10.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel10.Location = New System.Drawing.Point(16, 106)
        Me.LinkLabel10.Name = "LinkLabel10"
        Me.LinkLabel10.Size = New System.Drawing.Size(88, 13)
        Me.LinkLabel10.TabIndex = 1
        Me.LinkLabel10.TabStop = True
        Me.LinkLabel10.Text = "Xem Danh Sách "
        '
        'PictureBox10
        '
        Me.PictureBox10.Image = CType(resources.GetObject("PictureBox10.Image"), System.Drawing.Image)
        Me.PictureBox10.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox10.TabIndex = 0
        Me.PictureBox10.TabStop = False
        '
        'Panel12
        '
        Me.Panel12.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel12.Controls.Add(Me.LinkLabel12)
        Me.Panel12.Controls.Add(Me.PictureBox12)
        Me.Panel12.Location = New System.Drawing.Point(18, 161)
        Me.Panel12.Name = "Panel12"
        Me.Panel12.Size = New System.Drawing.Size(120, 130)
        Me.Panel12.TabIndex = 19
        '
        'LinkLabel12
        '
        Me.LinkLabel12.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel12.AutoSize = True
        Me.LinkLabel12.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel12.Location = New System.Drawing.Point(23, 106)
        Me.LinkLabel12.Name = "LinkLabel12"
        Me.LinkLabel12.Size = New System.Drawing.Size(86, 13)
        Me.LinkLabel12.TabIndex = 1
        Me.LinkLabel12.TabStop = True
        Me.LinkLabel12.Text = "Thêm Thông Tin"
        '
        'PictureBox12
        '
        Me.PictureBox12.Image = CType(resources.GetObject("PictureBox12.Image"), System.Drawing.Image)
        Me.PictureBox12.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox12.Name = "PictureBox12"
        Me.PictureBox12.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox12.TabIndex = 0
        Me.PictureBox12.TabStop = False
        '
        'Panel11
        '
        Me.Panel11.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel11.Controls.Add(Me.LinkLabel11)
        Me.Panel11.Controls.Add(Me.PictureBox11)
        Me.Panel11.Location = New System.Drawing.Point(18, 312)
        Me.Panel11.Name = "Panel11"
        Me.Panel11.Size = New System.Drawing.Size(120, 130)
        Me.Panel11.TabIndex = 21
        '
        'LinkLabel11
        '
        Me.LinkLabel11.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel11.AutoSize = True
        Me.LinkLabel11.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel11.Location = New System.Drawing.Point(23, 106)
        Me.LinkLabel11.Name = "LinkLabel11"
        Me.LinkLabel11.Size = New System.Drawing.Size(78, 13)
        Me.LinkLabel11.TabIndex = 1
        Me.LinkLabel11.TabStop = True
        Me.LinkLabel11.Text = "Sửa Thông Tin"
        '
        'PictureBox11
        '
        Me.PictureBox11.Image = CType(resources.GetObject("PictureBox11.Image"), System.Drawing.Image)
        Me.PictureBox11.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox11.TabIndex = 0
        Me.PictureBox11.TabStop = False
        '
        'SideNavPanel3
        '
        Me.SideNavPanel3.Controls.Add(Me.Panel13)
        Me.SideNavPanel3.Controls.Add(Me.Panel15)
        Me.SideNavPanel3.Controls.Add(Me.Panel14)
        Me.SideNavPanel3.Controls.Add(Me.Panel16)
        Me.SideNavPanel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SideNavPanel3.Location = New System.Drawing.Point(112, 36)
        Me.SideNavPanel3.Name = "SideNavPanel3"
        Me.SideNavPanel3.Size = New System.Drawing.Size(143, 689)
        Me.SideNavPanel3.TabIndex = 51
        Me.SideNavPanel3.Visible = False
        '
        'Panel13
        '
        Me.Panel13.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel13.Controls.Add(Me.LinkLabel13)
        Me.Panel13.Controls.Add(Me.PictureBox13)
        Me.Panel13.Location = New System.Drawing.Point(12, 474)
        Me.Panel13.Name = "Panel13"
        Me.Panel13.Size = New System.Drawing.Size(120, 130)
        Me.Panel13.TabIndex = 26
        '
        'LinkLabel13
        '
        Me.LinkLabel13.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel13.AutoSize = True
        Me.LinkLabel13.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel13.Location = New System.Drawing.Point(23, 106)
        Me.LinkLabel13.Name = "LinkLabel13"
        Me.LinkLabel13.Size = New System.Drawing.Size(75, 13)
        Me.LinkLabel13.TabIndex = 1
        Me.LinkLabel13.TabStop = True
        Me.LinkLabel13.Text = "Xoá Phiếu Trả"
        '
        'PictureBox13
        '
        Me.PictureBox13.Image = CType(resources.GetObject("PictureBox13.Image"), System.Drawing.Image)
        Me.PictureBox13.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox13.Name = "PictureBox13"
        Me.PictureBox13.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox13.TabIndex = 0
        Me.PictureBox13.TabStop = False
        '
        'Panel15
        '
        Me.Panel15.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel15.Controls.Add(Me.LinkLabel15)
        Me.Panel15.Controls.Add(Me.PictureBox15)
        Me.Panel15.Location = New System.Drawing.Point(15, 179)
        Me.Panel15.Name = "Panel15"
        Me.Panel15.Size = New System.Drawing.Size(120, 130)
        Me.Panel15.TabIndex = 23
        '
        'LinkLabel15
        '
        Me.LinkLabel15.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel15.AutoSize = True
        Me.LinkLabel15.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel15.Location = New System.Drawing.Point(35, 106)
        Me.LinkLabel15.Name = "LinkLabel15"
        Me.LinkLabel15.Size = New System.Drawing.Size(51, 13)
        Me.LinkLabel15.TabIndex = 1
        Me.LinkLabel15.TabStop = True
        Me.LinkLabel15.Text = "Trả Sách"
        '
        'PictureBox15
        '
        Me.PictureBox15.Image = CType(resources.GetObject("PictureBox15.Image"), System.Drawing.Image)
        Me.PictureBox15.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox15.Name = "PictureBox15"
        Me.PictureBox15.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox15.TabIndex = 0
        Me.PictureBox15.TabStop = False
        '
        'Panel14
        '
        Me.Panel14.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel14.Controls.Add(Me.LinkLabel14)
        Me.Panel14.Controls.Add(Me.PictureBox14)
        Me.Panel14.Location = New System.Drawing.Point(15, 28)
        Me.Panel14.Name = "Panel14"
        Me.Panel14.Size = New System.Drawing.Size(120, 130)
        Me.Panel14.TabIndex = 24
        '
        'LinkLabel14
        '
        Me.LinkLabel14.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel14.AutoSize = True
        Me.LinkLabel14.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel14.Location = New System.Drawing.Point(18, 106)
        Me.LinkLabel14.Name = "LinkLabel14"
        Me.LinkLabel14.Size = New System.Drawing.Size(85, 13)
        Me.LinkLabel14.TabIndex = 1
        Me.LinkLabel14.TabStop = True
        Me.LinkLabel14.Text = "Xem Danh Sách"
        '
        'PictureBox14
        '
        Me.PictureBox14.Image = CType(resources.GetObject("PictureBox14.Image"), System.Drawing.Image)
        Me.PictureBox14.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox14.Name = "PictureBox14"
        Me.PictureBox14.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox14.TabIndex = 0
        Me.PictureBox14.TabStop = False
        '
        'Panel16
        '
        Me.Panel16.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel16.Controls.Add(Me.LinkLabel16)
        Me.Panel16.Controls.Add(Me.PictureBox16)
        Me.Panel16.Location = New System.Drawing.Point(15, 330)
        Me.Panel16.Name = "Panel16"
        Me.Panel16.Size = New System.Drawing.Size(120, 130)
        Me.Panel16.TabIndex = 25
        '
        'LinkLabel16
        '
        Me.LinkLabel16.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel16.AutoSize = True
        Me.LinkLabel16.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel16.Location = New System.Drawing.Point(23, 106)
        Me.LinkLabel16.Name = "LinkLabel16"
        Me.LinkLabel16.Size = New System.Drawing.Size(75, 13)
        Me.LinkLabel16.TabIndex = 1
        Me.LinkLabel16.TabStop = True
        Me.LinkLabel16.Text = "Sửa Phiếu Trả"
        '
        'PictureBox16
        '
        Me.PictureBox16.Image = CType(resources.GetObject("PictureBox16.Image"), System.Drawing.Image)
        Me.PictureBox16.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox16.Name = "PictureBox16"
        Me.PictureBox16.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox16.TabIndex = 0
        Me.PictureBox16.TabStop = False
        '
        'SideNavPanel7
        '
        Me.SideNavPanel7.Controls.Add(Me.Panel17)
        Me.SideNavPanel7.Controls.Add(Me.Panel20)
        Me.SideNavPanel7.Controls.Add(Me.Panel18)
        Me.SideNavPanel7.Controls.Add(Me.Panel19)
        Me.SideNavPanel7.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SideNavPanel7.Location = New System.Drawing.Point(112, 36)
        Me.SideNavPanel7.Name = "SideNavPanel7"
        Me.SideNavPanel7.Size = New System.Drawing.Size(143, 689)
        Me.SideNavPanel7.TabIndex = 55
        Me.SideNavPanel7.Visible = False
        '
        'Panel17
        '
        Me.Panel17.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel17.Controls.Add(Me.LinkLabel17)
        Me.Panel17.Controls.Add(Me.PictureBox17)
        Me.Panel17.Location = New System.Drawing.Point(11, 471)
        Me.Panel17.Name = "Panel17"
        Me.Panel17.Size = New System.Drawing.Size(120, 130)
        Me.Panel17.TabIndex = 26
        '
        'LinkLabel17
        '
        Me.LinkLabel17.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel17.AutoSize = True
        Me.LinkLabel17.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel17.Location = New System.Drawing.Point(23, 106)
        Me.LinkLabel17.Name = "LinkLabel17"
        Me.LinkLabel17.Size = New System.Drawing.Size(78, 13)
        Me.LinkLabel17.TabIndex = 1
        Me.LinkLabel17.TabStop = True
        Me.LinkLabel17.Text = "Xoá Thông Tin"
        '
        'PictureBox17
        '
        Me.PictureBox17.Image = CType(resources.GetObject("PictureBox17.Image"), System.Drawing.Image)
        Me.PictureBox17.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox17.Name = "PictureBox17"
        Me.PictureBox17.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox17.TabIndex = 0
        Me.PictureBox17.TabStop = False
        '
        'Panel20
        '
        Me.Panel20.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel20.Controls.Add(Me.LinkLabel20)
        Me.Panel20.Controls.Add(Me.PictureBox20)
        Me.Panel20.Location = New System.Drawing.Point(11, 320)
        Me.Panel20.Name = "Panel20"
        Me.Panel20.Size = New System.Drawing.Size(120, 130)
        Me.Panel20.TabIndex = 25
        '
        'LinkLabel20
        '
        Me.LinkLabel20.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel20.AutoSize = True
        Me.LinkLabel20.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel20.Location = New System.Drawing.Point(23, 106)
        Me.LinkLabel20.Name = "LinkLabel20"
        Me.LinkLabel20.Size = New System.Drawing.Size(86, 13)
        Me.LinkLabel20.TabIndex = 1
        Me.LinkLabel20.TabStop = True
        Me.LinkLabel20.Text = "Sửa Phiếu Mượn"
        '
        'PictureBox20
        '
        Me.PictureBox20.Image = CType(resources.GetObject("PictureBox20.Image"), System.Drawing.Image)
        Me.PictureBox20.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox20.Name = "PictureBox20"
        Me.PictureBox20.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox20.TabIndex = 0
        Me.PictureBox20.TabStop = False
        '
        'Panel18
        '
        Me.Panel18.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel18.Controls.Add(Me.LinkLabel18)
        Me.Panel18.Controls.Add(Me.PictureBox18)
        Me.Panel18.Location = New System.Drawing.Point(11, 18)
        Me.Panel18.Name = "Panel18"
        Me.Panel18.Size = New System.Drawing.Size(120, 130)
        Me.Panel18.TabIndex = 24
        '
        'LinkLabel18
        '
        Me.LinkLabel18.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel18.AutoSize = True
        Me.LinkLabel18.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel18.Location = New System.Drawing.Point(18, 106)
        Me.LinkLabel18.Name = "LinkLabel18"
        Me.LinkLabel18.Size = New System.Drawing.Size(85, 13)
        Me.LinkLabel18.TabIndex = 1
        Me.LinkLabel18.TabStop = True
        Me.LinkLabel18.Text = "Xem Danh Sách"
        '
        'PictureBox18
        '
        Me.PictureBox18.Image = CType(resources.GetObject("PictureBox18.Image"), System.Drawing.Image)
        Me.PictureBox18.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox18.Name = "PictureBox18"
        Me.PictureBox18.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox18.TabIndex = 0
        Me.PictureBox18.TabStop = False
        '
        'Panel19
        '
        Me.Panel19.BackColor = System.Drawing.Color.MediumSeaGreen
        Me.Panel19.Controls.Add(Me.LinkLabel19)
        Me.Panel19.Controls.Add(Me.PictureBox19)
        Me.Panel19.Location = New System.Drawing.Point(11, 169)
        Me.Panel19.Name = "Panel19"
        Me.Panel19.Size = New System.Drawing.Size(120, 130)
        Me.Panel19.TabIndex = 23
        '
        'LinkLabel19
        '
        Me.LinkLabel19.ActiveLinkColor = System.Drawing.Color.MediumBlue
        Me.LinkLabel19.AutoSize = True
        Me.LinkLabel19.LinkColor = System.Drawing.Color.FromArgb(CType(CType(128, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.LinkLabel19.Location = New System.Drawing.Point(29, 106)
        Me.LinkLabel19.Name = "LinkLabel19"
        Me.LinkLabel19.Size = New System.Drawing.Size(62, 13)
        Me.LinkLabel19.TabIndex = 1
        Me.LinkLabel19.TabStop = True
        Me.LinkLabel19.Text = "Mượn Sách"
        '
        'PictureBox19
        '
        Me.PictureBox19.Image = CType(resources.GetObject("PictureBox19.Image"), System.Drawing.Image)
        Me.PictureBox19.Location = New System.Drawing.Point(10, 3)
        Me.PictureBox19.Name = "PictureBox19"
        Me.PictureBox19.Size = New System.Drawing.Size(100, 100)
        Me.PictureBox19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox19.TabIndex = 0
        Me.PictureBox19.TabStop = False
        '
        'SideNavPanel6
        '
        Me.SideNavPanel6.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SideNavPanel6.Location = New System.Drawing.Point(112, 36)
        Me.SideNavPanel6.Name = "SideNavPanel6"
        Me.SideNavPanel6.Size = New System.Drawing.Size(143, 689)
        Me.SideNavPanel6.TabIndex = 27
        Me.SideNavPanel6.Visible = False
        '
        'SideNavItem1
        '
        Me.SideNavItem1.IsSystemMenu = True
        Me.SideNavItem1.Name = "SideNavItem1"
        Me.SideNavItem1.Symbol = ""
        Me.SideNavItem1.Text = "ABC"
        '
        'Separator1
        '
        Me.Separator1.FixedSize = New System.Drawing.Size(3, 1)
        Me.Separator1.Name = "Separator1"
        Me.Separator1.Padding.Bottom = 2
        Me.Separator1.Padding.Left = 6
        Me.Separator1.Padding.Right = 6
        Me.Separator1.Padding.Top = 2
        Me.Separator1.SeparatorOrientation = DevComponents.DotNetBar.eDesignMarkerOrientation.Vertical
        '
        'SideNavItem3
        '
        Me.SideNavItem3.Name = "SideNavItem3"
        Me.SideNavItem3.Panel = Me.SideNavPanel6
        Me.SideNavItem3.Symbol = ""
        Me.SideNavItem3.Text = "Công Cụ"
        '
        'Separator6
        '
        Me.Separator6.FixedSize = New System.Drawing.Size(3, 1)
        Me.Separator6.Name = "Separator6"
        Me.Separator6.Padding.Bottom = 2
        Me.Separator6.Padding.Left = 6
        Me.Separator6.Padding.Right = 6
        Me.Separator6.Padding.Top = 2
        Me.Separator6.SeparatorOrientation = DevComponents.DotNetBar.eDesignMarkerOrientation.Vertical
        '
        'SideNavItem8
        '
        Me.SideNavItem8.Name = "SideNavItem8"
        Me.SideNavItem8.Panel = Me.SideNavPanel1
        Me.SideNavItem8.Symbol = ""
        Me.SideNavItem8.Text = "Sách"
        '
        'SideNavItem2
        '
        Me.SideNavItem2.Checked = True
        Me.SideNavItem2.Name = "SideNavItem2"
        Me.SideNavItem2.Panel = Me.SideNavPanel5
        Me.SideNavItem2.Symbol = ""
        Me.SideNavItem2.Text = "Thể Loại"
        '
        'Separator5
        '
        Me.Separator5.FixedSize = New System.Drawing.Size(3, 1)
        Me.Separator5.Name = "Separator5"
        Me.Separator5.Padding.Bottom = 2
        Me.Separator5.Padding.Left = 6
        Me.Separator5.Padding.Right = 6
        Me.Separator5.Padding.Top = 2
        Me.Separator5.SeparatorOrientation = DevComponents.DotNetBar.eDesignMarkerOrientation.Vertical
        '
        'sniDG
        '
        Me.sniDG.Name = "sniDG"
        Me.sniDG.Panel = Me.SideNavPanel2
        Me.sniDG.Symbol = ""
        Me.sniDG.Text = "Độc Giả"
        '
        'sniUser
        '
        Me.sniUser.Name = "sniUser"
        Me.sniUser.Panel = Me.SideNavPanel4
        Me.sniUser.Symbol = ""
        Me.sniUser.Text = "Tài Khoản"
        '
        'Separator3
        '
        Me.Separator3.FixedSize = New System.Drawing.Size(3, 1)
        Me.Separator3.Name = "Separator3"
        Me.Separator3.Padding.Bottom = 2
        Me.Separator3.Padding.Left = 6
        Me.Separator3.Padding.Right = 6
        Me.Separator3.Padding.Top = 2
        Me.Separator3.SeparatorOrientation = DevComponents.DotNetBar.eDesignMarkerOrientation.Vertical
        '
        'SideNavItem4
        '
        Me.SideNavItem4.Name = "SideNavItem4"
        Me.SideNavItem4.Symbol = ""
        Me.SideNavItem4.Text = "Thống Kê"
        '
        'Separator2
        '
        Me.Separator2.FixedSize = New System.Drawing.Size(3, 1)
        Me.Separator2.Name = "Separator2"
        Me.Separator2.Padding.Bottom = 2
        Me.Separator2.Padding.Left = 6
        Me.Separator2.Padding.Right = 6
        Me.Separator2.Padding.Top = 2
        Me.Separator2.SeparatorOrientation = DevComponents.DotNetBar.eDesignMarkerOrientation.Vertical
        '
        'SideNavItem9
        '
        Me.SideNavItem9.Name = "SideNavItem9"
        Me.SideNavItem9.Panel = Me.SideNavPanel3
        Me.SideNavItem9.Symbol = ""
        Me.SideNavItem9.Text = "Trả Sách"
        '
        'SideNavItem10
        '
        Me.SideNavItem10.Name = "SideNavItem10"
        Me.SideNavItem10.Panel = Me.SideNavPanel7
        Me.SideNavItem10.Symbol = ""
        Me.SideNavItem10.Text = "Mượn Sách"
        '
        'Separator4
        '
        Me.Separator4.FixedSize = New System.Drawing.Size(3, 1)
        Me.Separator4.Name = "Separator4"
        Me.Separator4.Padding.Bottom = 2
        Me.Separator4.Padding.Left = 6
        Me.Separator4.Padding.Right = 6
        Me.Separator4.Padding.Top = 2
        Me.Separator4.SeparatorOrientation = DevComponents.DotNetBar.eDesignMarkerOrientation.Vertical
        '
        'sniThoat
        '
        Me.sniThoat.Name = "sniThoat"
        Me.sniThoat.Symbol = ""
        Me.sniThoat.Text = "Exit"
        '
        'Separator7
        '
        Me.Separator7.FixedSize = New System.Drawing.Size(3, 1)
        Me.Separator7.Name = "Separator7"
        Me.Separator7.Padding.Bottom = 2
        Me.Separator7.Padding.Left = 6
        Me.Separator7.Padding.Right = 6
        Me.Separator7.Padding.Top = 2
        Me.Separator7.SeparatorOrientation = DevComponents.DotNetBar.eDesignMarkerOrientation.Vertical
        '
        'frmParent
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(659, 772)
        Me.Controls.Add(Me.SideNav1)
        Me.Controls.Add(Me.MenuStrip)
        Me.Controls.Add(Me.StatusStrip)
        Me.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip
        Me.Name = "frmParent"
        Me.Text = "frmParent"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.SideNav1.ResumeLayout(False)
        Me.SideNav1.PerformLayout()
        Me.SideNavPanel5.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SideNavPanel1.ResumeLayout(False)
        Me.pnl24.ResumeLayout(False)
        Me.pnl24.PerformLayout()
        CType(Me.pb24, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl21.ResumeLayout(False)
        Me.pnl21.PerformLayout()
        CType(Me.pbx11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl23.ResumeLayout(False)
        Me.pnl23.PerformLayout()
        CType(Me.pb23, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl22.ResumeLayout(False)
        Me.pnl22.PerformLayout()
        CType(Me.pb22, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SideNavPanel2.ResumeLayout(False)
        Me.pnl44.ResumeLayout(False)
        Me.pnl44.PerformLayout()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl41.ResumeLayout(False)
        Me.pnl41.PerformLayout()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl42.ResumeLayout(False)
        Me.pnl42.PerformLayout()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnl43.ResumeLayout(False)
        Me.pnl43.PerformLayout()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SideNavPanel4.ResumeLayout(False)
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel12.ResumeLayout(False)
        Me.Panel12.PerformLayout()
        CType(Me.PictureBox12, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel11.ResumeLayout(False)
        Me.Panel11.PerformLayout()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SideNavPanel3.ResumeLayout(False)
        Me.Panel13.ResumeLayout(False)
        Me.Panel13.PerformLayout()
        CType(Me.PictureBox13, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel15.ResumeLayout(False)
        Me.Panel15.PerformLayout()
        CType(Me.PictureBox15, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel14.ResumeLayout(False)
        Me.Panel14.PerformLayout()
        CType(Me.PictureBox14, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel16.ResumeLayout(False)
        Me.Panel16.PerformLayout()
        CType(Me.PictureBox16, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SideNavPanel7.ResumeLayout(False)
        Me.Panel17.ResumeLayout(False)
        Me.Panel17.PerformLayout()
        CType(Me.PictureBox17, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel20.ResumeLayout(False)
        Me.Panel20.PerformLayout()
        CType(Me.PictureBox20, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel18.ResumeLayout(False)
        Me.Panel18.PerformLayout()
        CType(Me.PictureBox18, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel19.ResumeLayout(False)
        Me.Panel19.PerformLayout()
        CType(Me.PictureBox19, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents HelpMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents ToolStripStatusLabel As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents StatusStrip As System.Windows.Forms.StatusStrip
    Friend WithEvents PrintToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SaveAsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents NewToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FileMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents OpenToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents SaveToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents MenuStrip As System.Windows.Forms.MenuStrip
    Friend WithEvents SideNav1 As DevComponents.DotNetBar.Controls.SideNav
    Friend WithEvents SideNavPanel4 As DevComponents.DotNetBar.Controls.SideNavPanel
    Friend WithEvents SideNavPanel2 As DevComponents.DotNetBar.Controls.SideNavPanel
    Friend WithEvents SideNavItem1 As DevComponents.DotNetBar.Controls.SideNavItem
    Friend WithEvents Separator1 As DevComponents.DotNetBar.Separator
    Friend WithEvents sniDG As DevComponents.DotNetBar.Controls.SideNavItem
    Friend WithEvents sniUser As DevComponents.DotNetBar.Controls.SideNavItem
    Friend WithEvents sniThoat As DevComponents.DotNetBar.Controls.SideNavItem
    Friend WithEvents SáchToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents QuảnLýSáchToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ThêmToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents XoáSáchToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SửaSáchToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ĐộcGiảToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ThểLoạiToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TrảMượnSáchToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MượnSáchToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TrảSáchToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ThốngKêToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ThốngKêToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents XemXoáToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SideNavPanel6 As DevComponents.DotNetBar.Controls.SideNavPanel
    Friend WithEvents SideNavPanel5 As DevComponents.DotNetBar.Controls.SideNavPanel
    Friend WithEvents SideNavItem3 As DevComponents.DotNetBar.Controls.SideNavItem
    Friend WithEvents SideNavItem2 As DevComponents.DotNetBar.Controls.SideNavItem
    Friend WithEvents SideNavItem4 As DevComponents.DotNetBar.Controls.SideNavItem
    Friend WithEvents Separator2 As DevComponents.DotNetBar.Separator
    Friend WithEvents Separator3 As DevComponents.DotNetBar.Separator
    Friend WithEvents Separator4 As DevComponents.DotNetBar.Separator
    Friend WithEvents Separator6 As DevComponents.DotNetBar.Separator
    Friend WithEvents Separator5 As DevComponents.DotNetBar.Separator
    Friend WithEvents Separator7 As DevComponents.DotNetBar.Separator
    Friend WithEvents SideNavPanel1 As DevComponents.DotNetBar.Controls.SideNavPanel
    Friend WithEvents SideNavItem8 As DevComponents.DotNetBar.Controls.SideNavItem
    Friend WithEvents pnl24 As Panel
    Friend WithEvents llbl24 As LinkLabel
    Friend WithEvents pb24 As PictureBox
    Friend WithEvents pnl21 As Panel
    Friend WithEvents llbl21 As LinkLabel
    Friend WithEvents pbx11 As PictureBox
    Friend WithEvents pnl23 As Panel
    Friend WithEvents lbl23 As LinkLabel
    Friend WithEvents pb23 As PictureBox
    Friend WithEvents pnl22 As Panel
    Friend WithEvents llbl22 As LinkLabel
    Friend WithEvents pb22 As PictureBox
    Friend WithEvents pnl44 As Panel
    Friend WithEvents LinkLabel5 As LinkLabel
    Friend WithEvents PictureBox5 As PictureBox
    Friend WithEvents pnl41 As Panel
    Friend WithEvents LinkLabel6 As LinkLabel
    Friend WithEvents PictureBox6 As PictureBox
    Friend WithEvents pnl42 As Panel
    Friend WithEvents LinkLabel8 As LinkLabel
    Friend WithEvents PictureBox8 As PictureBox
    Friend WithEvents pnl43 As Panel
    Friend WithEvents LinkLabel7 As LinkLabel
    Friend WithEvents PictureBox7 As PictureBox
    Friend WithEvents Panel1 As Panel
    Friend WithEvents LinkLabel1 As LinkLabel
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents LinkLabel2 As LinkLabel
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents Panel4 As Panel
    Friend WithEvents LinkLabel4 As LinkLabel
    Friend WithEvents PictureBox4 As PictureBox
    Friend WithEvents Panel3 As Panel
    Friend WithEvents LinkLabel3 As LinkLabel
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents Panel9 As Panel
    Friend WithEvents LinkLabel9 As LinkLabel
    Friend WithEvents PictureBox9 As PictureBox
    Friend WithEvents Panel10 As Panel
    Friend WithEvents LinkLabel10 As LinkLabel
    Friend WithEvents PictureBox10 As PictureBox
    Friend WithEvents Panel12 As Panel
    Friend WithEvents LinkLabel12 As LinkLabel
    Friend WithEvents PictureBox12 As PictureBox
    Friend WithEvents Panel11 As Panel
    Friend WithEvents LinkLabel11 As LinkLabel
    Friend WithEvents PictureBox11 As PictureBox
    Friend WithEvents SideNavPanel7 As DevComponents.DotNetBar.Controls.SideNavPanel
    Friend WithEvents Panel17 As Panel
    Friend WithEvents LinkLabel17 As LinkLabel
    Friend WithEvents PictureBox17 As PictureBox
    Friend WithEvents Panel20 As Panel
    Friend WithEvents LinkLabel20 As LinkLabel
    Friend WithEvents PictureBox20 As PictureBox
    Friend WithEvents Panel18 As Panel
    Friend WithEvents LinkLabel18 As LinkLabel
    Friend WithEvents PictureBox18 As PictureBox
    Friend WithEvents Panel19 As Panel
    Friend WithEvents LinkLabel19 As LinkLabel
    Friend WithEvents PictureBox19 As PictureBox
    Friend WithEvents SideNavPanel3 As DevComponents.DotNetBar.Controls.SideNavPanel
    Friend WithEvents Panel13 As Panel
    Friend WithEvents LinkLabel13 As LinkLabel
    Friend WithEvents PictureBox13 As PictureBox
    Friend WithEvents Panel15 As Panel
    Friend WithEvents LinkLabel15 As LinkLabel
    Friend WithEvents PictureBox15 As PictureBox
    Friend WithEvents Panel14 As Panel
    Friend WithEvents LinkLabel14 As LinkLabel
    Friend WithEvents PictureBox14 As PictureBox
    Friend WithEvents Panel16 As Panel
    Friend WithEvents LinkLabel16 As LinkLabel
    Friend WithEvents PictureBox16 As PictureBox
    Friend WithEvents SideNavItem9 As DevComponents.DotNetBar.Controls.SideNavItem
    Friend WithEvents SideNavItem10 As DevComponents.DotNetBar.Controls.SideNavItem
    Friend WithEvents FileToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents lblUser As ToolStripStatusLabel
End Class
