﻿Imports System
Imports System.Collections.Generic
Imports System.Linq
Imports System.ComponentModel
Imports DevExpress.Mvvm
Imports DevExpress.Mvvm.DataAnnotations
Imports DevExpress.Mvvm.POCO
Imports DevExpress.Mvvm.ViewModel
Imports GUI.QLTVEntitiesDataModel
Namespace Global.GUI.ViewModels
  ''' <summary>
  ''' Represents the root POCO view model for the QLTVEntities data model.
  ''' </summary>
  Public Partial Class QLTVEntitiesViewModel
    Inherits DocumentsViewModel(Of QLTVEntitiesModuleDescription, IQLTVEntitiesUnitOfWork)
    Private Const _TablesGroup As String = "Tables"
    Private Const _ViewsGroup As String = "Views"    
    ''' <summary>
    ''' Creates a new instance of QLTVEntitiesViewModel as a POCO view model.
    ''' </summary>
    Public Shared Function Create() As QLTVEntitiesViewModel
      Return ViewModelSource.Create(Function() New QLTVEntitiesViewModel())
    End Function    
    ''' <summary>
    ''' Initializes a new instance of the QLTVEntitiesViewModel class.
    ''' This constructor is declared protected to avoid undesired instantiation of the QLTVEntitiesViewModel type without the POCO proxy factory.
    ''' </summary>
    Protected Sub New()
      MyBase.New(UnitOfWorkSource.GetUnitOfWorkFactory())
    End Sub
    Protected Overrides Function CreateModules() As QLTVEntitiesModuleDescription()
      Return New QLTVEntitiesModuleDescription() { New QLTVEntitiesModuleDescription("vv DG Het Han", "vv_DG_HetHanCollectionView", _ViewsGroup),New QLTVEntitiesModuleDescription("vv Muon", "vv_MuonCollectionView", _ViewsGroup),New QLTVEntitiesModuleDescription("vv Sach It", "vv_SachItCollectionView", _ViewsGroup) }
    End Function
  End Class
  Public Partial Class QLTVEntitiesModuleDescription
    Inherits ModuleDescription(Of QLTVEntitiesModuleDescription)
    Public Sub New(ByVal title As String, ByVal documentType As String, ByVal group As String, Optional ByVal peekCollectionViewModelFactory As Func(Of QLTVEntitiesModuleDescription, Object) = Nothing)
      MyBase.New(title, documentType, group, peekCollectionViewModelFactory)
    End Sub
  End Class
End Namespace
