﻿Imports System
Imports System.Linq
Imports DevExpress.Mvvm.POCO
Imports DevExpress.Mvvm.DataModel
Imports DevExpress.Mvvm.ViewModel
Imports GUI.QLTVEntitiesDataModel
Imports GUI.Common
Imports GUI
Namespace Global.GUI.ViewModels
  ''' <summary>
  ''' Represents the vv_SachIt collection view model.
  ''' </summary>
  Public Partial Class vv_SachItCollectionViewModel
    Inherits ReadOnlyCollectionViewModel(Of vv_SachIt, IQLTVEntitiesUnitOfWork)    
    ''' <summary>
    ''' Creates a new instance of vv_SachItCollectionViewModel as a POCO view model.
    ''' </summary>
    ''' <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
    Public Shared Function Create(Optional ByVal unitOfWorkFactory As IUnitOfWorkFactory(Of IQLTVEntitiesUnitOfWork) = Nothing) As vv_SachItCollectionViewModel
      Return ViewModelSource.Create(Function() New vv_SachItCollectionViewModel(unitOfWorkFactory))
    End Function    
    ''' <summary>
    ''' Initializes a new instance of the vv_SachItCollectionViewModel class.
    ''' This constructor is declared protected to avoid undesired instantiation of the vv_SachItCollectionViewModel type without the POCO proxy factory.
    ''' </summary>
    ''' <param name="unitOfWorkFactory">A factory used to create a unit of work instance.</param>
    Protected Sub New(Optional ByVal unitOfWorkFactory As IUnitOfWorkFactory(Of IQLTVEntitiesUnitOfWork) = Nothing)
      MyBase.New(If(unitOfWorkFactory, UnitOfWorkSource.GetUnitOfWorkFactory()), Function(ByVal x) x.vv_SachIt)
    End Sub
  End Class
End Namespace
