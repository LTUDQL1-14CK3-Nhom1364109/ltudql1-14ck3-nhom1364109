﻿Public Class frmCTDG
    Private _value As Integer
    Public Property Value As Integer
        Get
            Return _value
        End Get
        Set(value As Integer)
            _value = value
        End Set
    End Property
    Private Sub frmCTDG_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DataSetCT.ChiTietSach' table. You can move, or remove it, as needed.
        Me.ChiTietDGTableAdapter.Fill(Me.DataSetCT.ChiTietDG, Value)

        Me.ReportViewer1.RefreshReport()
    End Sub
End Class