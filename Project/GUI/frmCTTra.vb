﻿Public Class frmCTTra
    Private _value As Integer
    Public Property Value As Integer
        Get
            Return _value
        End Get
        Set(value As Integer)
            _value = value
        End Set
    End Property
    Private Sub frmCTTra_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'DataSetCT.PhieuTra' table. You can move, or remove it, as needed.
        Me.PhieuTraTableAdapter.Fill(Me.DataSetCT.PhieuTra, Value)
        Me.ReportViewer1.RefreshReport()
    End Sub
End Class